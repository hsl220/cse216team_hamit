package edu.lehigh.cse216.cse216team_hamit.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import com.sendgrid.*;
import java.io.IOException;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;


public class UsersService //implements DatabaseInteractor<User>
{
    /**
     * The field that will hold the single instance of the class
     */
    private static UsersService mInstance;

    /**
     * Returns the single instance of the class. If the instance has not yet been initiailized,
     * then it will initialize a new instance and then return it.
     *
     * @return the single instance of the class
     */
    public static UsersService getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new UsersService();
        }

        return mInstance;
    }

    private UsersService()
    {}

    /*
        MARK: DatabaseInteractor Methods
    */

    /**
     * Creates the users table in the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public void createTable(Connection con) throws SQLException
    {
        // sql query that will create the table
        String createTableString = "create table users ("
                                + "email varchar(255) not null primary key, "
                                + "signature varchar(255) not null, "
                                + "salt varchar(225) not null, "
                                + "name varchar(225) not null, "
                                + "sessionkey varchar(225) not null )";

        // creates the sql statement
        Statement createTableStatement = con.createStatement();
        // executes the statement
        createTableStatement.execute(createTableString);
        // releases the resource
        createTableStatement.close();
    }

    /**
     * Drops the user table from the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public void dropTable(Connection con) throws SQLException
    {
        // the sql query string to drop the table
        String dropTableString = "drop table users";

        // creates the sql statement
        Statement dropTableStatement = con.createStatement();
        // executes the statement
        dropTableStatement.execute(dropTableString);
        // releases resource
        dropTableStatement.close();
    }

    /**
     * Gets all of the rows in the users table. Will return null if there are no rows
     * in the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public ArrayList<User> getAllRows(Connection con) throws SQLException
    {
        ArrayList<User> users = null;

        String getAllRowsString = "select * from users";
        Statement getAllRowsStatement = con.createStatement();
        ResultSet rows = getAllRowsStatement.executeQuery(getAllRowsString);

        // checks if there is at least one user
        if (rows.next())
        {
            users = new ArrayList<User>();

            do
            {
                User user = User.parseUser(rows);
                users.add(user);
            }
            while (rows.next());
        }

        // releases resource
        getAllRowsStatement.close();

        return users;
    }

    /**
     * Gets a specific user by their email
     *
     * @param con the Connection to the database
     * @param string the email of the user, ie: id of the user
     * @throws SQLException
     */
    public User getRowWithId(Connection con, String email) throws SQLException
    {
        User user = null;

        String getRowWithIdString = "select * from users where email = ?";
        PreparedStatement getRowWithIdStatement = con.prepareStatement(getRowWithIdString);
        getRowWithIdStatement.setString(1, email);
        ResultSet row = getRowWithIdStatement.executeQuery();

        if (row.next())
        {
            user = User.parseUser(row);
        }

        getRowWithIdStatement.close();

        return user;
    }

    /**
     * Deletes a user by their email
     *
     * @param con the Connection to the database
     * @param email the user's email, ie: the user's id
     * @throws SQLException
     */
    public void deleteRowWithId(Connection con, String email) throws SQLException
    {
        String deleteRowWithIdString = "delete from users where email = ?";

        PreparedStatement deleteRowWithIdStatement = con.prepareStatement(deleteRowWithIdString);
        deleteRowWithIdStatement.setString(1, email);
        deleteRowWithIdStatement.execute();
    }
    /**
     * Gets a user that contains a specific string
     * @param con the Connection to the database
     * @param content the string containing the name that you are looking for in users
     * @return the user with the given first and last name
     * @throws SQLException
     */

    public ArrayList<User> getRowWithName(Connection con, String content) throws SQLException
    {
        ArrayList<User> users = null;

        String getRowWithNameString = "select * from users where name like '%' || ? || '%'";
        PreparedStatement getRowWithNameStatement = con.prepareStatement(getRowWithNameString);

        getRowWithNameStatement.setString(1, content);
        ResultSet rows = getRowWithNameStatement.executeQuery();

        if (rows.next())
        {
            users = new ArrayList<User>();
        }

        do
        {
            User user = User.parseUser(rows);
            users.add(user);
        }
        while (rows.next());
        return users;
    }

    public void insertRowWithEmail(Connection con, String email, String name) throws SQLException
    {

        String pass = Long.toHexString(Double.doubleToLongBits(Math.random()));
        String hashedPass = "";
        String salt = "";
        try {
        hashedPass = AuthHelper.generateHash(pass);
        }
        catch (NoSuchAlgorithmException ex)
        {
            System.out.println(ex);
        }
        catch (InvalidKeySpecException ex)
        {
            System.out.println(ex);
        }

        try {
        salt = AuthHelper.generateSalt().toString();
        }
        catch (NoSuchAlgorithmException ex)
        {
            System.out.println(ex);
        }
        String hashedPassAndSalt = hashedPass + salt;
        String insertRowWithIdString = "insert into users (email, signature, salt, name, sessionkey)"
                                        + " values (?, ?, ?, ?, ?)";
        PreparedStatement preparedStmt = con.prepareStatement(insertRowWithIdString);
        preparedStmt.setString(1, email);
        preparedStmt.setString(2, hashedPassAndSalt);
        preparedStmt.setString(3, salt);
        preparedStmt.setString(4, name);
        preparedStmt.setString(5, " ");

        preparedStmt.execute();

        Email from = new Email("hsl220@lehigh.edu");
        String subject = "Activate your account on \"The Buzz!";
        Email to = new Email(email);
        Content content = new Content("text/plain", "Hi " + name + "!\nSign in using your email and this password: " + pass + ". Be sure to change your password upon your first login!");
        Mail mail = new Mail(from, subject, to, content);
    
        SendGrid sg = new SendGrid(System.getenv("SENDGRID_API_KEY"));
        Request request = new Request();
        try {
          request.setMethod(Method.POST);
          request.setEndpoint("mail/send");
          request.setBody(mail.build());
          Response response = sg.api(request);
          System.out.println(response.getStatusCode());
          System.out.println(response.getBody());
          System.out.println(response.getHeaders());
        } catch (IOException ex) {
          System.out.println(ex);
        }
       
}
}