package edu.lehigh.cse216.cse216team_hamit.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MessagesService //implements DatabaseInteractor<Message>
{
    /**
     * The field that will hold the single instance of the class
     */
    private static MessagesService mInstance;

    /**
     * Returns the single instance of the class. If the instance has not yet been initiailized,
     * then it will initialize a new instance and then return it.
     *
     * @return the single instance of the class
     */
    public static MessagesService getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new MessagesService();
        }

        return mInstance;
    }

    private MessagesService()
    {}

    /*
        MARK: DatabaseInteractor Methods
    */

    /**
     * Creates the messages table in the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public void createTable(Connection con) throws SQLException
    {
        // sql query that will create the table
        String createTableString = "create table messages ("
                                + "id serial primary key, "
                                + "title varchar(255) not null, "
                                + "body varchar(255) not null, "
                                + "createdDate timestamp not null default current_timestamp, "
                                + "updatedDate timestamp, "
                                + "upvoteCount int default 0, "
                                + "downvoteCount int default 0, "
                                + "foreign key authorEmail references users email )";

        // creates the sql statement
        Statement createTableStatement = con.createStatement();
        // executes the statement
        createTableStatement.execute(createTableString);
        // releases the resource
        createTableStatement.close();
    }

    /**
     * Drops the message table from the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public void dropTable(Connection con) throws SQLException
    {
        // the sql query string to drop the table
        String dropTableString = "drop table messages";

        // creates the sql statement
        Statement dropTableStatement = con.createStatement();
        // executes the statement
        dropTableStatement.execute(dropTableString);
        // releases resource
        dropTableStatement.close();
    }

    /**
     * Gets all of the rows in the messages table. Will return null if there are no rows
     * in the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public ArrayList<Message> getAllRows(Connection con) throws SQLException
    {
        ArrayList<Message> messages = null;

        String getAllRowsString = "select * from messages";
        Statement getAllRowsStatement = con.createStatement();
        ResultSet rows = getAllRowsStatement.executeQuery(getAllRowsString);

        // checks if there is at least one message
        if (rows.next())
        {
            messages = new ArrayList<Message>();

            do
            {
                Message message = Message.parseMessage(rows);
                messages.add(message);
            }
            while (rows.next());
        }

        // releases resource
        getAllRowsStatement.close();

        return messages;
    }

    /**
     * Gets a specific message based on its id
     *
     * @param con the Connection to the database
     * @param id the id of the message
     * @throws SQLException
     */
    public Message getRowWithId(Connection con, int id) throws SQLException
    {
        Message message = null;

        String getRowWithIdString = "select * from messages where id = ?";
        PreparedStatement getRowWithIdStatement = con.prepareStatement(getRowWithIdString);
        getRowWithIdStatement.setInt(1, id);
        ResultSet row = getRowWithIdStatement.executeQuery();

        if (row.next())
        {
            message = Message.parseMessage(row);
        }

        getRowWithIdStatement.close();

        return message;
    }

    /**
     * Deletes a message with the specific id
     *
     * @param con the Connection to the database
     * @param id the id of the message
     * @throws SQLException
     */
    public void deleteRowWithId(Connection con, int id) throws SQLException
    {
        String deleteRowWithIdString = "delete from messages where id = ?";

        PreparedStatement deleteRowWithIdStatement = con.prepareStatement(deleteRowWithIdString);
        deleteRowWithIdStatement.setInt(1, id);
        deleteRowWithIdStatement.execute();
    }

    /*
        MARK: public member methods
    */

    /**
     * Gets a message that contains a specific string
     * @param con the Connection to the database
     * @param content the string that you are looking for in the message
     * @return the messages that contain the string
     * @throws SQLException
     */
    public ArrayList<Message> getRowWithContents(Connection con, String content) throws SQLException
    {
        ArrayList<Message> messages = null;

        String getRowWithContentString = "select * from messages where title like '%' || ? || '%' or body like '%' || ? || '%'";
        PreparedStatement getRowWithContentStatement = con.prepareStatement(getRowWithContentString);

        getRowWithContentStatement.setString(1, content);
        getRowWithContentStatement.setString(2, content);
        ResultSet rows = getRowWithContentStatement.executeQuery();

        if (rows.next())
        {
            messages = new ArrayList<Message>();
        }

        do
        {
            Message message = Message.parseMessage(rows);
            messages.add(message);
        }
        while (rows.next());

        return messages;
    }
}