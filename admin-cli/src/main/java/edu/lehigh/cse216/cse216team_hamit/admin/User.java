package edu.lehigh.cse216.cse216team_hamit.admin;

import java.sql.ResultSet;
import java.sql.SQLException;

public class User {
	private String mEmail;
    private String mSignature; //hashed password with salt, salt separate?
    private String mSalt;
    private String mName;
    private String mSessionKey;

    /**
	 *
	 * @param email
	 * @param signature
     * @param salt
     * @param name
     * @param sessionkey
	 */
	public User(String email, String signature, String salt, String name, String sessionkey)
	{
		mEmail = email;
        mSignature = signature;
        mSalt = salt;
        mName = name;
        mSessionKey = sessionkey;
    }
    
    public String getEmail()
    {
        return mEmail;
    }

    public String getSignature()
    {
        return mSignature;
    }

    public String getSalt()
    {
        return mSalt;
    }

    public String getName() {
        return mName;
    }

    public String getSessionKey()
    {
        return mSessionKey;
    }

    public String toString()
	{
		return String.format(
			"EMAIL: %3s SIGNATURE: %-25s SALT: %-40s NAME: %-15s SESSIONKEY: %-15s",
			mEmail,
			mSignature,
			mSalt,
			mName,
            mSessionKey
		);
	}

	public static User parseUser(ResultSet row) throws SQLException
	{
		String email = row.getString("email");
		String signature = row.getString("signature");
		String salt = row.getString("salt");
		String name = row.getString("name");
        String sessionKey = row.getString("sessionkey");

		return new User(email, signature, salt, name, sessionKey);
	}

}