package edu.lehigh.cse216.cse216team_hamit.admin;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Message {
	private int mId;
	private String mTitle;
	private String mBody;
	private Date mDateCreated;
	private Date mDateUpdated;
	private int mUpvoteCount;
	private int mDownvoteCount;
	private String mAuthorEmail;

	/**
	 *
	 * @param id
	 * @param title
	 * @param body
	 * @param dateCreated
	 * @param updatedDate
	 * @param authorEmail
	 */
	public Message(int id, String title, String body, Date dateCreated, Date updatedDate, int upvoteCount, int downvoteCount, String authorEmail)
	{
		mId = id;
		mTitle = title;
		mBody = body;
		mDateCreated = dateCreated;
		mDateUpdated = updatedDate;
		mUpvoteCount = upvoteCount;
		mDownvoteCount = downvoteCount;
		mAuthorEmail = authorEmail;
	}

	public int getId()
	{
		return mId;
	}

	public String getTitle()
	{
		return mTitle;
	}

	public String getBody()
	{
		return mBody;
	}

	public Date getDateCreated()
	{
		return mDateCreated;
	}

	public Date getDateUpdated()
	{
		return mDateUpdated;
	}

	public int getUpvoteCount()
	{
		return mUpvoteCount;
	}

	public int getDownvoteCount()
	{
		return mDownvoteCount;
	}

	public String getAuthorEmail()
	{
		return mAuthorEmail;
	} 

	public String toString()
	{
		return String.format(
			"ID: %3d TITLE: %-25s BODY: %-40s DATECREATED: %-15s UPVOTES: %3d DOWNVOTES: %3d AUTHOREMAIL: %25s ",
			mId,
			mTitle,
			mBody,
			mDateCreated.toString(),
			mUpvoteCount,
			mDownvoteCount,
			mAuthorEmail
		);
	}

	public static Message parseMessage(ResultSet row) throws SQLException
	{
		int id = row.getInt("id");
		String title = row.getString("title");
		String body = row.getString("body");
		Date dateCreated = row.getDate("createdDate");
		Date updatedDate = row.getDate("updatedDate");
		int upvoteCount = row.getInt("upvoteCount");
		int downvoteCount = row.getInt("downvoteCount");
		String authorEmail = row.getString("authorEmail");

		return new Message(id, title, body, dateCreated, updatedDate, upvoteCount, downvoteCount, authorEmail);
	}
}