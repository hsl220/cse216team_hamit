package edu.lehigh.cse216.cse216team_hamit.admin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.net.URI;
import java.net.URISyntaxException;
import com.sendgrid.*;
import java.io.IOException;

public class App
{
    public static void main( String[] args )
    {
        Email from = new Email("hannahl1409@gmail.com");
        String subject = "Sending with SendGrid is Fun";
        Email to = new Email("hsl220@lehigh.edu");
        Content content = new Content("text/plain", "and easy to do anywhere, even with Java");
        Mail mail = new Mail(from, subject, to, content);
    
        SendGrid sg = new SendGrid(System.getenv("SENDGRID_API_KEY"));
        Request request = new Request();
        try {
          request.setMethod(Method.POST);
          request.setEndpoint("mail/send");
          request.setBody(mail.build());
          Response response = sg.api(request);
          System.out.println(response.getStatusCode());
          System.out.println(response.getBody());
          System.out.println(response.getHeaders());
        } catch (IOException ex) {
          System.out.println(ex);
        }
        Connection con = getConnection();

        if (con == null)
        {
            System.out.println("Main: connection was null");
            return;
        }

        Admin adminApp = new Admin(con);
        adminApp.start();
    }

    /**
     * the URI to the database on the heroku instance
     */
    private static final String DB_URL = "postgres://ppezmnlwjujzsj:f4a9518045fecbaa55682b13d64439cd0cd74700449c1e32d2d332dbe66b9522@ec2-75-101-153-56.compute-1.amazonaws.com:5432/d9aevoh464ai9p";

    /**
     * Creates the connection to the database
     * @return the connection to the database
     */
    private static Connection getConnection()
    {
        Connection con = null;
        try
        {
            URI dbUri = new URI(DB_URL);

            String username = dbUri.getUserInfo().split(":")[0];
            String password = dbUri.getUserInfo().split(":")[1];
            String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath() + "?sslmode=require";

            con = DriverManager.getConnection(dbUrl, username, password);
        }
        catch (SQLException e)
        {
            // handleSQLErrorWithCode(e);
            e.printStackTrace();
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }

        return con;
    }
}
