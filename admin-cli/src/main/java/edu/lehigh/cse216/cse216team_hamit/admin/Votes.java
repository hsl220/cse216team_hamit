package edu.lehigh.cse216.cse216team_hamit.admin;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Votes {
	private String mEmail;
    private int mId;
    private int mType;


    /**
	 *
	 * @param email
	 * @param message mId
     * @param type (upvotes or downvotes) 1 for upvote, -1 for downvote
	 */
	public Votes(String email, int id, int type)
	{
		mEmail = email;
		mId = id;
		mType = type;
	}

	public String getEmail()
	{
		return mEmail;
	}

	public int getId()
	{
		return mId;
	}

	public int getType()
	{
		return mType;
	}

	public String toString()
	{
		return String.format(
			"EMAIL: %3s ID: %-25d TYPE: %-40d ",
			mEmail,
			mId,
			mType
		);
	}

	public static Votes parseVotes(ResultSet row) throws SQLException
	{
		String email = row.getString("email");
		int id = row.getInt("id");
		int type = row.getInt("type");

		return new Votes(email, id, type);
	}
}