package edu.lehigh.cse216.cse216team_hamit.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class VotesService
{
    /**
     * The field that will hold the single instance of the class
     */
    private static VotesService mInstance;

    /**
     * Returns the single instance of the class. If the instance has not yet been initiailized,
     * then it will initialize a new instance and then return it.
     *
     * @return the single instance of the class
     */
    public static VotesService getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new VotesService();
        }

        return mInstance;
    }

    private VotesService()
    {}

    /**
     * Creates the votes table in the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public void createTable(Connection con) throws SQLException
    {
        // sql query that will create the table
        String createTableString = "create table votes ("
                                + "email varchar(255) not null, "
                                + "id int default 0, "
                                + "type int default 0 )";

        // creates the sql statement
        Statement createTableStatement = con.createStatement();
        // executes the statement
        createTableStatement.execute(createTableString);
        // releases the resource
        createTableStatement.close();
    }

    /**
     * Drops the votes table from the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public void dropTable(Connection con) throws SQLException
    {
        // the sql query string to drop the table
        String dropTableString = "drop table votes";

        // creates the sql statement
        Statement dropTableStatement = con.createStatement();
        // executes the statement
        dropTableStatement.execute(dropTableString);
        // releases resource
        dropTableStatement.close();
    }

    /**
     * Gets all of the rows in the votes table. Will return null if there are no rows
     * in the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public ArrayList<Votes> getAllRows(Connection con) throws SQLException
    {
        ArrayList<Votes> votes = null;

        String getAllRowsString = "select * from votes";
        Statement getAllRowsStatement = con.createStatement();
        ResultSet rows = getAllRowsStatement.executeQuery(getAllRowsString);

        // checks if there is at least one user
        if (rows.next())
        {
            votes = new ArrayList<Votes>();

            do
            {
                Votes vote = Votes.parseVotes(rows);
                votes.add(vote);
            }
            while (rows.next());
        }

        // releases resource
        getAllRowsStatement.close();

        return votes;
    }
         /**
     * Gets all votes of a message given the message id
     *
     * @param con the Connection to the database
     * @param id the id of the message
     * @throws SQLException
     */
    public ArrayList<Votes> getRowsWithId(Connection con, int id) throws SQLException
    {
        ArrayList<Votes> votes = null;

        String getRowWithIdString = "select * from votes where id = ?";
        PreparedStatement getRowWithIdStatement = con.prepareStatement(getRowWithIdString);
        getRowWithIdStatement.setInt(1, id);
        ResultSet rows = getRowWithIdStatement.executeQuery();

         // checks if there is at least one message
        if (rows.next())
                {
                    votes = new ArrayList<Votes>();
        
                    do
                    {
                        Votes vote = Votes.parseVotes(rows);
                        votes.add(vote);
                    }
                    while (rows.next());
                }
        
                // releases resource
                getRowWithIdStatement.close();

        return votes;
    }
    /**
     * Deletes a vote with the specific message id & user email
     *
     * @param con the Connection to the database
     * @param id the id of the message
     * @param email the email of the user
     * @throws SQLException
     */
    public void deleteRowWithId(Connection con, int id, String email) throws SQLException
    {
        String deleteRowWithIdString = "delete from messages where id = ? && email = ? ";

        PreparedStatement deleteRowWithIdStatement = con.prepareStatement(deleteRowWithIdString);
        deleteRowWithIdStatement.setInt(1, id);
        deleteRowWithIdStatement.setString(2, email);
        deleteRowWithIdStatement.execute();
    }
}