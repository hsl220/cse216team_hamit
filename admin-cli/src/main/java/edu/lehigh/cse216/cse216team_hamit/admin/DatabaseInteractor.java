package edu.lehigh.cse216.cse216team_hamit.admin;

import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseInteractor<T>
{
    public void createTable(Connection con) throws SQLException;
    public void dropTable(Connection con) throws SQLException;

    public T getRowWithId(Connection con, int id) throws SQLException;
    public ArrayList<T> getAllRows(Connection con) throws SQLException;

    public void deleteRowWithId(Connection con, int id) throws SQLException;
}