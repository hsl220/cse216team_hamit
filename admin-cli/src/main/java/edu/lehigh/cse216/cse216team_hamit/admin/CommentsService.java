package edu.lehigh.cse216.cse216team_hamit.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CommentsService 
{
    /**
     * The field that will hold the single instance of the class
     */
    private static CommentsService mInstance;

    /**
     * Returns the single instance of the class. If the instance has not yet been initiailized,
     * then it will initialize a new instance and then return it.
     *
     * @return the single instance of the class
     */
    public static CommentsService getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new CommentsService();
        }

        return mInstance;
    }

    private CommentsService()
    {}

    /**
     * Creates the comments table in the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public void createTable(Connection con) throws SQLException
    {
        // sql query that will create the table
        String createTableString = "create table comments ("
                                + "id int default 0, "
                                + "body varchar(255) not null, "
                                + "createdDate timestamp not null default current_timestamp, "
                                + "foreign key authorEmail references users email )";

        // creates the sql statement
        Statement createTableStatement = con.createStatement();
        // executes the statement
        createTableStatement.execute(createTableString);
        // releases the resource
        createTableStatement.close();
    }

    /**
     * Drops the comments table from the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public void dropTable(Connection con) throws SQLException
    {
        // the sql query string to drop the table
        String dropTableString = "drop table comments";

        // creates the sql statement
        Statement dropTableStatement = con.createStatement();
        // executes the statement
        dropTableStatement.execute(dropTableString);
        // releases resource
        dropTableStatement.close();
    }

    /**
     * Gets all of the rows in the comments table. Will return null if there are no rows
     * in the database
     *
     * @param con the Connection to the database
     * @throws SQLException
     */
    public ArrayList<Comment> getAllRows(Connection con) throws SQLException
    {
        ArrayList<Comment> comments = null;

        String getAllRowsString = "select * from comments";
        Statement getAllRowsStatement = con.createStatement();
        ResultSet rows = getAllRowsStatement.executeQuery(getAllRowsString);

        // checks if there is at least one comment
        if (rows.next())
        {
            comments = new ArrayList<Comment>();

            do
            {
                Comment comment = Comment.parseComment(rows);
                comments.add(comment);
            }
            while (rows.next());
        }

        // releases resource
        getAllRowsStatement.close();

        return comments;
    }

    /*
        MARK: public member methods
    */

    /**
     * Deletes a comment with the given content
     * @param con the Connection to the database
     * @param content the string that you are looking for in the comment
     * @throws SQLException
     */
    public void deleteRowWithContents(Connection con, String content) throws SQLException
    {

        String getRowWithContentString = "select * from comments where body like '%' || ? || '%'";
     //   PreparedStatement getRowWithContentStatement = con.prepareStatement(getRowWithContentString);

       // getRowWithContentStatement.setString(1, content);
       // ResultSet rows = getRowWithContentStatement.executeQuery();


        PreparedStatement deleteRowWithIdStatement = con.prepareStatement(getRowWithContentString);
        deleteRowWithIdStatement.setString(1, content);
        deleteRowWithIdStatement.execute();

        
    }
     /**
     * Gets all comments of a message given the message id
     *
     * @param con the Connection to the database
     * @param id the id of the message
     * @throws SQLException
     */
    public ArrayList<Comment> getRowsWithId(Connection con, int id) throws SQLException
    {
        ArrayList<Comment> comments = null;

        String getRowWithIdString = "select * from comments where id = ?";
        PreparedStatement getRowWithIdStatement = con.prepareStatement(getRowWithIdString);
        getRowWithIdStatement.setInt(1, id);
        ResultSet rows = getRowWithIdStatement.executeQuery();

         // checks if there is at least one message
        if (rows.next())
                {
                    comments = new ArrayList<Comment>();
        
                    do
                    {
                        Comment comment = Comment.parseComment(rows);
                        comments.add(comment);
                    }
                    while (rows.next());
                }
        
                // releases resource
                getRowWithIdStatement.close();

        return comments;
    }
    /**
     * Gets all comments written by a user
     *
     * @param con the Connection to the database
     * @param id the email of the user
     * @throws SQLException
     */
    public ArrayList<Comment> getRowsWithEmail(Connection con, String email) throws SQLException
    {
        ArrayList<Comment> comments = null;

        String getRowWithEmailString = "select * from comments where authorEmail like '%' || ? || '%'";
        PreparedStatement getRowWithEmailStatement = con.prepareStatement(getRowWithEmailString);
        getRowWithEmailStatement.setString(1, email);
        ResultSet rows = getRowWithEmailStatement.executeQuery();

         // checks if there is at least one message
        if (rows.next())
                {
                    comments = new ArrayList<Comment>();
        
                    do
                    {
                        Comment comment = Comment.parseComment(rows);
                        comments.add(comment);
                    }
                    while (rows.next());
                }
        
                // releases resource
                getRowWithEmailStatement.close();

        return comments;
    }
}