package edu.lehigh.cse216.cse216team_hamit.admin;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Comment {
	private int mId;
	private String mBody;
	private Date mDateCreated;
	private String mAuthorEmail;

	/**
	 *
	 * @param id
	 * @param body
	 * @param dateCreated
	 * @param author 
	 */
	public Comment(int id, String body, Date dateCreated, String authorEmail)
	{
		mId = id;
		mBody = body;
		mDateCreated = dateCreated;
		mAuthorEmail = authorEmail;
	}

	public int getId()
	{
		return mId;
	}


	public String getBody()
	{
		return mBody;
	}

	public Date getDateCreated()
	{
		return mDateCreated;
	}

	
	public String getAuthorEmail()
	{
		return mAuthorEmail;
	} 

	public String toString()
	{
		return String.format(
			"ID: %3d BODY: %-40s DATECREATED: %-15s AUTHOREMAIL: %-25s ",
			mId,
			mBody,
			mDateCreated.toString(),
			mAuthorEmail
		);
	}

	public static Comment parseComment(ResultSet row) throws SQLException
	{
		int id = row.getInt("id");
		String body = row.getString("body");
		Date dateCreated = row.getDate("createdDate");
		String authorEmail = row.getString("authorEmail");

		return new Comment(id, body, dateCreated, authorEmail);
	}
}