package edu.lehigh.cse216.cse216team_hamit.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class Admin
{
    // connection to database on heroku instance
    private Connection mConnection;

    // used to get input through the command line from user
    private BufferedReader mIn;

    /**
     * Constructs an instance of the admin app with a connection
     * @param con a connection to the database
     */
    public Admin(Connection con)
    {
        // set the fields in this class
        mConnection = con;
        mIn = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * Starts the admin app
     */
    public void start()
    {
        while (true)
        {
            printMenu();
            char action = prompt("TD1*-+q");

            if (action == 'q')
            {
                try
                {
                    mConnection.close();
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
                break;
            }

            switch (action)
            {
                case 'T':
                    createTable();
                    break;
                case 'D':
                    dropTable();
                    break;
                case '1':
                    queryRows();
                    break;
                case '*':
                    printAllRows();
                    break;
                case '-':
                    promptDeleteRow();
                    break;
                case '+':
                    promptInsertRow();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Starts the prompts for creating a table
     */
    private void createTable()
    {
        // prompt which table to create
        System.out.println("Create a table");
        System.out.println("  [m] messages");
        System.out.println("  [u] users");
        System.out.println("  [v] votes");
        System.out.println("  [x] comments");
        System.out.println("  [c] cancel");

        // get the selected action from the user
        char action = prompt("muvxc");
        try
        {
            // do something based on the different acceptable inputs
            switch (action)
            {
                case 'm':
                    MessagesService.getInstance().createTable(mConnection);
                    break;
                case 'u':
                    UsersService.getInstance().createTable(mConnection);
                    break;
                case 'v':
                    VotesService.getInstance().createTable(mConnection);
                    break;
                case 'x':
                    CommentsService.getInstance().createTable(mConnection);
                    break;
                case 'c':
                    System.out.println("canceled");
                    break;
                default:
                    break;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            // handleErrorWithCode(e);
        }
    }

    /**
     * Starts the prompts for dropping a table
     */
    private void dropTable()
    {
        ArrayList<String> tableNames = getTableNames();

        if (tableNames != null)
        {
            System.out.println("Drop a table");

            String tableName = selectTable();
            try
            {
                if (tableName == null)
                {
                    System.out.println("canceled");
                }
                else if (tableName.equals("messages"))
                {
                    MessagesService.getInstance().dropTable(mConnection);
                    System.out.println("Successfully dropped table messages");
                }
                else if (tableName.equals("users"))
                {
                    UsersService.getInstance().dropTable(mConnection);
                    System.out.println("Successfully dropped table users");
                }
                else if (tableName.equals("votes"))
                {
                    VotesService.getInstance().dropTable(mConnection);
                    System.out.println("Successfully dropped table votes");
                }
                else if (tableName.equals("comments"))
                {
                    CommentsService.getInstance().dropTable(mConnection);
                    System.out.println("Successfully dropped table comments");
                }
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                // handleErrorWithCode(e);
            }
        }
        else
        {
            System.out.println("No tables in database. Try creating one first");
        }
    }

    /**
     * Prints the table names in a formatted manner and creates the actions string
     * @param tableNames an ArrayList<String> that contains the names of each table
     */
    private String printTablesNames(ArrayList<String> tableNames)
    {
        int index = 1;
        String actions = "";
        for (String name : tableNames)
        {
            actions += "" + index;
            System.out.println("  [" + (index++) + "] " + name);
        }
        actions += "c";
        System.out.println("  [c] cancel");
        return actions;
    }

    /**
     * Starts the prompts to print all the rows in a table from the database
     */
    private void printAllRows()
    {
        System.out.println("Pick a table");
        String tableName = selectTable();

        try
        {
            if (tableName == null)
            {
                System.out.println("canceled");
            }
            else if (tableName.equals("messages"))
            {
                ArrayList<Message> messages = MessagesService.getInstance().getAllRows(mConnection);

                if (messages != null)
                {
                    for (Message message : messages)
                    {
                        System.out.println(message);
                    }
                }
                else
                {
                    System.out.println("No rows found in table messages");
                }
            }
            else if (tableName.equals("users"))
            {
                ArrayList<User> users = UsersService.getInstance().getAllRows(mConnection);

                if (users != null)
                {
                    for (User user : users)
                    {
                        System.out.println(user);
                    }
                }
                else
                {
                    System.out.println("No rows found in table users");
                }
            }
            else if (tableName.equals("votes"))
            {
                ArrayList<Votes> votes = VotesService.getInstance().getAllRows(mConnection);

                if (votes != null)
                {
                    for (Votes vote : votes)
                    {
                        System.out.println(vote);
                    }
                }
                else
                {
                    System.out.println("No rows found in table votes");
                }
            }
            else if (tableName.equals("comments"))
            {
                ArrayList<Comment> comments = CommentsService.getInstance().getAllRows(mConnection);

                if (comments != null)
                {
                    for (Comment comment: comments)
                    {
                        System.out.println(comment);
                    }
                }
                else
                {
                    System.out.println("No rows found in table comments");
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            // handleErrorWithCode(e);
        }
    }

    /**
     * Prints the menu options
     */
    private void printMenu()
    {
        System.out.println("Main Menu");
        System.out.println("  [T] Create table");
        System.out.println("  [D] Drop table");
        System.out.println("  [1] Query rows");
        System.out.println("  [*] Get all rows");
        System.out.println("  [-] Delete a row");
        System.out.println("  [+] Insert a row");
        System.out.println("  [q] Quit Program");
    }

    /**
     * Starts the prompts to select a table from the database
     * @return
     */
    private String selectTable()
    {
        ArrayList<String> tableNames = getTableNames();
        String actions = printTablesNames(tableNames);
        char action = prompt(actions);

        if (action == 'c')
        {
            return null;
        }

        int tableNameIndex = Integer.parseInt("" + action);
        return tableNames.get(tableNameIndex - 1);
    }

    /**
     * Prompts the user to choose a char from the actions string. Will loop until a valid selection is made, and it is case sensitive.
     * @param actions a string containing the case sensitive chars that are the valid choices
     * @return the valid action that the user selected
     */
    private char prompt(String actions)
    {
        while (true) {
            String action;
            try {
                action = mIn.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
            if (action.length() != 1)
                continue;
            if (actions.contains(action)) {
                return action.charAt(0);
            }
            System.out.println("Invalid Command");
        }
    }

    /**
     * Starts the prompts to query the rows of a table in the database
     */
    private void queryRows()
    {
        System.out.println("Query a table");

        String tableName = selectTable();

        if (tableName == null)
        {
            System.out.println("canceled");
        }
        else if (tableName.equals("messages"))
        {
            queryMessages();
        }
        else if (tableName.equals("users"))
        {
            queryUsers();
        }
        else if (tableName.equals("comments"))
        {
            queryComments();
        }
        else if (tableName.equals("votes"))
        {
            queryVotes();
        }
    }

    /**
     * Starts the prompts to query the rows of the messages table in the database
     */
    private void queryMessages()
    {
        System.out.println("Select column to query");
        System.out.println("  [d] date");
        System.out.println("  [t] title");
        System.out.println("  [b] body");
        System.out.println("  [c] cancel");

        char action = prompt("idtbc");

        switch(action)
        {
            case 'd':
                System.out.println("can't query by date yet");
                break;
            case 't':
            case 'b':
                System.out.println("Enter text: ");
                String content;
                try {
                    content = mIn.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                ArrayList<Message> messages = null;

                try
                {
                    messages = MessagesService.getInstance().getRowWithContents(mConnection, content);
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                    // handleErrorWithCode(e);
                }

                if (messages != null)
                {
                    for (Message message : messages)
                    {
                        System.out.println(message);
                    }
                }
                else
                {
                    System.out.println("No messages contain: " + content);
                }
                break;
            case 'c':
                System.out.println("canceled");
                break;
            default:
                break;
        }
    }

        /**
     * Starts the prompts to query the rows of the users table in the database
     */
    private void queryUsers()
    {
        System.out.println("Select column to query");
        System.out.println("  [e] email");
        System.out.println("  [n] name");
        System.out.println("  [c] cancel");

        char action = prompt("ienc");

        switch(action)
        {
            case 'e':
            System.out.println("Enter email: ");
            String content;
            try {
                content = mIn.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            User user = null;
            try
            {
                user = UsersService.getInstance().getRowWithId(mConnection, content);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                // handleErrorWithCode(e);
            }

            if (user != null)
            {
                    System.out.println(user);
            }
            else
            {
                System.out.println("No users with email: " + content);
            }
            break;
            case 'n':
                System.out.println("Enter the user's name: ");
                String name;
                try {
                    name = mIn.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                ArrayList<User> u = null;
                try
                {
                    u = UsersService.getInstance().getRowWithName(mConnection, name);
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                    // handleErrorWithCode(e);
                }
    
                if (u != null)
                {
                    for (User us: u)
                    System.out.println(us);
                }
                else
                {
                    System.out.println("No users with name: " + name);
                }
                break;
            case 'c':
                System.out.println("canceled");
                break;
            default:
                break;
        }
    }
    /**
     * Starts the prompts to query the rows of the comments table in the database
     */
    private void queryComments()
    {
        System.out.println("Select column to query");
        System.out.println("  [m] message id");
        System.out.println("  [b] body");
        System.out.println("  [u] user");
        System.out.println("  [c] cancel");

        char action = prompt("imbuc");

        switch(action)
        {
            case 'm':
                System.out.println("Enter the message id: ");
                int id;
                try {
                    id = Integer.parseInt(mIn.readLine());
                } catch (IOException e)
                {
                    e.printStackTrace();
                    return;
                }
                ArrayList<Comment> comments = null;
                try {
                    comments = CommentsService.getInstance().getRowsWithId(mConnection, id);
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
                if (comments != null)
                {
                    for (Comment comment : comments)
                    {
                        System.out.println(comment);
                    }
                }
                else
                {
                    System.out.println("No comments for message with message id: " + id);
                }
                break;
            case 'b':
                System.out.println("Can't query by body yet");
                break;
            case 'u':
            System.out.println("Enter email: ");
                String email;
                try {
                    email = mIn.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                ArrayList<Comment> c = null;

                try
                {
                    c = CommentsService.getInstance().getRowsWithEmail(mConnection, email);
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                    // handleErrorWithCode(e);
                }

                if (c != null)
                {
                    for (Comment co : c)
                    {
                        System.out.println(co);
                    }
                }
                else
                {
                    System.out.println("No comments from user with email: " + email);
                }
                break;
            case 'c':
                System.out.println("canceled");
                break;
            default:
                break;
        }
    }
     /**
     * Starts the prompts to query the rows of the votes table in the database
     */
    private void queryVotes()
    {
        System.out.println("Select column to query");
        System.out.println("  [m] message id");
        System.out.println("  [c] cancel");

        char action = prompt("imuc");

        switch(action)
        {
            case 'm':
                System.out.println("Enter the message id: ");
                int id;
                try {
                    id = Integer.parseInt(mIn.readLine());
                } catch (IOException e)
                {
                    e.printStackTrace();
                    return;
                }
                ArrayList<Votes> votes = null;
                try {
                    votes = VotesService.getInstance().getRowsWithId(mConnection, id);
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
                if (votes != null)
                {
                    for (Votes vote : votes)
                    {
                        System.out.println(vote);
                    }
                }
                else
                {
                    System.out.println("No votes for message with message id: " + id);
                }
                break;
            case 'c':
                System.out.println("canceled");
                break;
            default:
                break;
        }
    }
    /**
     * Starts the prompts to delete a row in a table in the database
     */
    private void promptDeleteRow()
    {
        String tableName = selectTable();
        if (tableName == null)
        {
            System.out.println("canceled");
            return;
        }


        if (tableName.equals("messages"))
        {
            int id;
            while (true)
            {
                System.out.println("Enter id: ");
                try
                {
                    id = Integer.parseInt(mIn.readLine());
                    break;
                }
                catch (IOException e)
                {
                    System.out.println("please enter an integer");
                }
                catch (InputMismatchException e)
                {
                    System.out.println("please enter an integer");
                }
            }
            try
            {
                MessagesService.getInstance().deleteRowWithId(mConnection, id);
                System.out.println("successfully deleted message with id (" + id + ")");
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                // handleErrorWithCode(e);
            }
        }
        if (tableName.equals("users"))
        {
            String email;
            while (true)
            {
                System.out.println("Enter email: ");
                try
                {
                    email = mIn.readLine();
                    break;
                }
                catch (IOException e) {
                    e.printStackTrace();
                    continue;
                }
            }
            try
            {
                UsersService.getInstance().deleteRowWithId(mConnection, email);
                System.out.println("successfully deleted user with email (" + email + ")");
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                // handleErrorWithCode(e);
            }
        }
        if (tableName.equals("comments"))
        {
            String body;
            while (true)
            {
                System.out.println("Enter body of comment: ");
                try
                {
                    body = mIn.readLine();
                    break;
                }
                catch (IOException e) {
                    e.printStackTrace();
                    continue;
                }
            }
            try
            {
                CommentsService.getInstance().deleteRowWithContents(mConnection, body);
                System.out.println("successfully deleted comment with body (" + body + ")");
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                // handleErrorWithCode(e);
            }
        }
        if (tableName.equals("votes"))
        {
            int id;
            String email;
            while (true)
            {
                System.out.println("Enter id of message: ");
                try
                {
                    id = Integer.parseInt(mIn.readLine());
                    email = mIn.readLine();
                    break;
                }
                catch (IOException e) {
                    e.printStackTrace();
                    continue;
                }
            }
            try
            {
                VotesService.getInstance().deleteRowWithId(mConnection, id, email);
                System.out.println("successfully deleted vote with message id (" + id + ") and user email (" + email + ")");
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                // handleErrorWithCode(e);
            }
        }
    }
        /**
     * Starts the prompts to delete a row in a table in the database
     */
    private void promptInsertRow()
    {
        String tableName = selectTable();
        if (tableName == null)
        {
            System.out.println("canceled");
            return;
        }
        else if (tableName.equals("votes"))
        {
            System.out.println("Can't insert row for votes table");
            return;
        }
        else if (tableName.equals("comments"))
        {
            System.out.println("Can't insert row for comments table");
            return;
        }
        else if (tableName.equals("messages"))
        {
            System.out.println("Can't insert row for messages table");
            return;
        }
        else if (tableName.equals("users"))
        {
            String email;
            String name;
            while (true)
            {
                System.out.println("Enter email: ");
                try
                {
                    email = mIn.readLine();
                    System.out.println("Enter name: ");
                    name = mIn.readLine();
                    break;
                }
                catch (IOException e) {
                    e.printStackTrace();
                    continue;
                }
            }
            try
            {
                UsersService.getInstance().insertRowWithEmail(mConnection, email, name);
                System.out.println("successfully inserted user with email (" + email + ")");
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                // handleErrorWithCode(e);
            }
        }
    }
    
    /**
     * Get the names of the tables currently in the database
     * @return an ArrayList<String> containing the names of the tables
     */
    private ArrayList<String> getTableNames()
    {
        ArrayList<String> tableNames = null;

        String query = "SELECT table_name FROM information_schema.tables WHERE table_schema='public'";

        try
        {
            Statement statement = mConnection.createStatement();
            ResultSet names = statement.executeQuery(query);

            if (names.next())
            {
                tableNames = new ArrayList<String>();

                do
                {
                    tableNames.add(names.getString("table_name"));
                }
                while (names.next());
            }
        }
        catch (SQLException e)
        {
            // handleSQLErrorWithCode(e);
            e.printStackTrace();
        }

        return tableNames;
    }

    // private void handleErrorWithCode(SQLException e)
    // {
    //     int errorCode = e.getErrorCode();

    //     switch (errorCode)
    //     {
    //         case 0:
    //             System.out.println("some error");
    //             break;
    //         default:
    //             break;
    //     }
    // }
}