TARGETFOLDER=./local;

rm -rf $TARGETFOLDER;
mkdir $TARGETFOLDER;

node_modules/typescript/bin/tsc app.ts --removeComments --strict --target es2015 --outFile $TARGETFOLDER/$WEBFOLDER/app.js;
node_modules/typescript/bin/tsc ts/**/**.ts --removeComments --strict --target es2015 --outFile $TARGETFOLDER/$WEBFOLDER/app.js;
# node_modules/typescript/bin/tsc ts/screens/**.ts --removeComments --strict --target es2015 --outFile $TARGETFOLDER/$WEBFOLDER/app.js;
# node_modules/typescript/bin/tsc ts/models/**.ts --removeComments --strict --target es2015 --outFile $TARGETFOLDER/$WEBFOLDER/app.js;
# node_modules/typescript/bin/tsc ts/services/**.ts --removeComments --strict --target es2015 --outFile $TARGETFOLDER/$WEBFOLDER/app.js;

cp index.html $TARGETFOLDER;
cat app.css css/**.css >> $TARGETFOLDER/app.css;

cp node_modules/jquery/dist/jquery.min.js $TARGETFOLDER;
cp node_modules/bootstrap/dist/css/bootstrap.min.css $TARGETFOLDER;
cp node_modules/bootstrap/dist/css/bootstrap.min.css.map $TARGETFOLDER;
cp node_modules/bootstrap/dist/js/bootstrap.min.js $TARGETFOLDER;
cp node_modules/font-awesome/css/font-awesome.min.css $TARGETFOLDER;
cp -R node_modules/font-awesome/fonts $TARGETFOLDER;