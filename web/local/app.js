"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class Message {
    constructor(id, senderEmail, title, body, dateCreated, upvoteCount, downvoteCount) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.senderEmail = senderEmail;
        this.dateCreated = dateCreated;
        this.upvoteCount = upvoteCount;
        this.downvoteCount = downvoteCount;
    }
    static parseMessage(data) {
        const { mId, mTitle, mBody, mSenderEmail, mDateCreated, mUpvoteCount, mDownvoteCount } = data;
        return new Message(mId, mSenderEmail, mTitle, mBody, mDateCreated, mUpvoteCount, mDownvoteCount);
    }
}
class MyComment {
    constructor(id, messageId, senderEmail, body) {
        this.id = id;
        this.senderEmail = senderEmail;
        this.body = body;
        this.messageId = messageId;
    }
    static parseComment(data) {
        const { mId, mMessageId, mSenderEmail, mBody } = data;
        return new MyComment(mId, mMessageId, mSenderEmail, mBody);
    }
}
class User {
    constructor(email, firstName, lastName) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    static parseUser(data) {
        const { email, firstName, lastName } = data;
        return new User(email, firstName, lastName);
    }
}
class Vote {
    constructor(email, id, type) {
        this.email = email;
        this.id = id;
        this.type = type;
    }
    static parseMessage(obj) {
        const { email, id, type } = obj;
        return new Vote(email, id, type);
    }
}
class MyNavigation {
    static showingNewPage() {
        MyNavigation.prevScrollPos = window.scrollY;
        window.scrollTo(0, 0);
    }
    static closingPage() {
        window.scrollTo(0, MyNavigation.prevScrollPos);
    }
}
MyNavigation.prevScrollPos = 0;
class ProfileScreen {
    static init() {
        if (!ProfileScreen.isInit) {
            var user = new User('adg219@lehigh.edu', 'alexander', 'greene');
            let html = `<div id="${ProfileScreen.NAME}">` +
                `<header id="${ProfileScreen.NAME}-header">` +
                `<button id="${ProfileScreen.NAME}-btn-close" class="btn"><i class="fa fa-times"></i></button>` +
                `<h1>Profile</h1>` +
                `</header>` +
                `</div>`;
            $('#app').append(html);
            ProfileScreen.isInit = true;
        }
    }
    static refresh() {
        ProfileScreen.init();
        let user = new User('adg219@lehigh.edu', 'alexander', 'greene');
        $(`#${ProfileScreen.NAME}`).append(HTMLStringCreator.createProfileHTML(user));
    }
    static show() {
        ProfileScreen.init();
        $(`#${ProfileScreen.NAME}`).show();
        ProfileScreen.refresh();
    }
    static hide() {
        ProfileScreen.init();
        $(`#${ProfileScreen.NAME}`).hide();
        $(`#profile-card`).remove();
    }
    static setCloseBtnListener(handler) {
        $(`#${ProfileScreen.NAME}-btn-close`).click(() => {
            ProfileScreen.hide();
            handler();
        });
    }
}
ProfileScreen.NAME = "profile-screen";
ProfileScreen.isInit = false;
class HTMLStringCreator {
    static createMessageCard(componentName) {
        return `<div id="${componentName}-message-card" class="card">` +
            `<div class="card-body">` +
            `<h5 id="${componentName}-message-title" class="card-title"></h5>` +
            `<h6 id="${componentName}-message-subtitle" class="card-subtitle mb-2 text-muted"></h6>` +
            `<p id="${componentName}-message-text" class="card-text"></p>` +
            `</div>` +
            `</div>`;
    }
    static createCommentCard(componentName, subtitle, text) {
        return `<div class="card ${componentName}-comment-card">` +
            `<div class="card-body">` +
            `<h6 id="${componentName}-message-subtitle" class="card-subtitle mb-2 text-muted">${subtitle}</h6>` +
            `<p id="${componentName}-message-text" class="card-text">${text}</p>` +
            `</div>` +
            `</div>`;
    }
    static createFormGroupWithTextInput(className, label, placeholder, s) {
        return `<div class="form-group">` +
            `<label for="${s}">${label}</label>` +
            `<input type="text" class="form-control" id="${className}-field" aria-describedby="${s}Help" placeholder="${placeholder}">` +
            `</div>`;
    }
    static createFormGroupWithTextArea(className, label, s) {
        return `<div class="form-group">` +
            `<label for="${s}">${label}</label>` +
            `<textarea class="form-control" id="${className}-textarea" rows="3"></textarea>` +
            `</div>`;
    }
    static createProfileHTML(user) {
        const { email, firstName, lastName } = user;
        return `<div class="card" id="profile-card">` +
            `<div class="card-body">` +
            `<p id="profile-name" class="card-text">${firstName + ' ' + lastName}</p>` +
            `<p id="profile-email" class="card-text">${email}</p>` +
            `</div>` +
            `</div>`;
    }
}
class Constants {
}
Constants.API_URL = 'https://glacial-fjord-88850.herokuapp.com';
class CommentService {
    constructor() { }
    getCommentsForUserWithAuthorEmail(email) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                url: Constants.API_URL + `/comments/${email}`,
                method: 'GET'
            });
            const { mStatus, mData, mMessage } = result;
            let comments = [];
            if (mStatus === 'ok') {
                mData.forEach(cmtData => {
                    let comment = MyComment.parseComment(cmtData);
                    comments.push(comment);
                });
                resolve(comments);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    addComment(comment) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                url: Constants.API_URL + `/comments`,
                method: 'POST',
                data: JSON.stringify(comment),
                contentType: 'application/json'
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                resolve(true);
            }
            else {
                reject(mMessage);
            }
        }));
    }
}
CommentService.instance = new CommentService();
class MessageDetailScreen {
    static setMessage(message) {
        MessageDetailScreen.selectedMessage = message;
    }
    static init() {
        if (!MessageDetailScreen.isInit) {
            let div = $(`<div id="${MessageDetailScreen.NAME}"><div id="${MessageDetailScreen.NAME}-header"><button id="${MessageDetailScreen.NAME}-btn-back" class="btn"><i class="fa fa-times"></i></button><h1 class="text-center">Message Detail</h1></div></div>`);
            let card = HTMLStringCreator.createMessageCard(MessageDetailScreen.NAME);
            let header = `<h5 id="${MessageDetailScreen.NAME}-comments-count">Comments</h5>`;
            let commentForm = $(`<form id="${MessageDetailScreen.NAME}-comment-form">${MessageDetailScreen.createFormGroupWithTextArea('Add comment', 'add-comment', 2, `${MessageDetailScreen.NAME}-add-comment-form`)}</form>`);
            commentForm.submit(e => e.preventDefault());
            let addCommentBtn = $(`<button id="${MessageDetailScreen.NAME}-add-comment-btn" class="btn btn-warning">Submit Comment</button>`);
            addCommentBtn.click(MessageDetailScreen.submitComment);
            div.append(card);
            commentForm.append(addCommentBtn);
            div.append(commentForm);
            div.append(header);
            $('#app').append(div);
            MessageDetailScreen.isInit = true;
        }
    }
    static submitComment() {
        console.log('submitting comment');
        $(`#${MessageDetailScreen.NAME}-add-comment-textarea`).val('');
    }
    static addEditBtnClickListener(handler) {
        MessageDetailScreen.init();
        $(`#${MessageDetailScreen.NAME}-btn-edit`).click(handler);
    }
    static addBackBtnClickListener(handler) {
        MessageDetailScreen.init();
        $(`#${MessageDetailScreen.NAME}-btn-back`).click(MessageDetailScreen.hide);
    }
    static submit() {
        MessageDetailScreen.init();
        let title = $(`#${MessageDetailScreen.NAME}-title-field`).val();
        let body = $(`#${MessageDetailScreen.NAME}-body-textarea`).val();
        MessageDetailScreen.hide();
    }
    static resetForm() {
        $(`#${MessageDetailScreen.NAME}-title-field`).val('');
        $(`#${MessageDetailScreen.NAME}-body-textarea`).val('');
        $(`#${MessageDetailScreen.NAME}-add-comment-textarea`).val('');
    }
    static createFormGroupWithTextArea(label, s, rows, className) {
        if (rows === undefined) {
            rows = 3;
        }
        return `
			<div class="form-group ${className}">
				<label>${label}</label>
				<textarea class="form-control" id="${MessageDetailScreen.NAME}-${s}-textarea" rows="${rows}"></textarea>
			</div>
		`;
    }
    static refreshComments() {
        return __awaiter(this, void 0, void 0, function* () {
            $(`#${MessageDetailScreen.NAME}-comments`).remove();
            let email = 'adg219@lehigh.edu';
            let comments = yield CommentService.instance.getCommentsForUserWithAuthorEmail(email);
            $(`#${MessageDetailScreen.NAME}-comments-count`).text(`Comments (${comments.length})`);
            let table = $(`<table id="${MessageDetailScreen.NAME}-comments"></table>`);
            comments.forEach(comment => {
                let row = HTMLStringCreator.createCommentCard(MessageDetailScreen.NAME, comment.senderEmail, comment.body);
                table.append(row);
            });
            $(`#${MessageDetailScreen.NAME}`).append(table);
        });
    }
    static hide() {
        MessageDetailScreen.init();
        MessageDetailScreen.resetForm();
        $(`#${MessageDetailScreen.NAME}-edit-link-container`).remove();
        $(`#${MessageDetailScreen.NAME}`).hide();
    }
    static setCloseBtnListener(handler) {
        MessageDetailScreen.init();
        MessageDetailScreen.hide();
        $(`#${MessageDetailScreen.NAME}-btn-back`).click(() => {
            MessageDetailScreen.hide();
            handler();
        });
    }
    static show() {
        MessageDetailScreen.init();
        const { title, senderEmail, body } = MessageDetailScreen.selectedMessage;
        $(`#${MessageDetailScreen.NAME}-message-title`).text(title);
        $(`#${MessageDetailScreen.NAME}-message-subtitle`).text(senderEmail);
        $(`#${MessageDetailScreen.NAME}-message-text`).text(body);
        if (MessageDetailScreen.selectedMessage.senderEmail === 'adg2191@lehigh.edu') {
            let ul = $(`<ul id="${MessageDetailScreen.NAME}-edit-link-container" class="list-group list-group-flush"></ul>`);
            let li = $(`<li class="list-group-item"></li>`);
            let editLink = $(`<a href="#" class="card-link float-right">Edit</a>`);
            editLink.click(e => {
                console.log('edit message');
            });
            li.append(editLink);
            ul.append(li);
            $(`#${MessageDetailScreen.NAME}-message-card`).append(ul);
        }
        MessageDetailScreen.refreshComments();
        $(`#${MessageDetailScreen.NAME}`).show();
    }
}
MessageDetailScreen.NAME = 'message-detail-screen';
MessageDetailScreen.isInit = false;
class MessagesService {
    MessagesService() { }
    upvote(id) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                type: 'PUT',
                url: Constants.API_URL + `/message/${id}/upvote`,
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                resolve(true);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    downvote(id) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                type: 'PUT',
                url: Constants.API_URL + `/message/${id}/downvote`,
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === "ok") {
                resolve(true);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    postMessage(email, title, body) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                type: 'POST',
                url: Constants.API_URL + "/messages",
                contentType: 'application/json',
                data: JSON.stringify({ email, title, body })
            })
                .catch(error => console.log(error));
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                resolve(true);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    getMessages() {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                type: "GET",
                url: Constants.API_URL + "/messages",
            });
            const { mStatus, mData, mMessage } = result;
            let messages = [];
            if (mStatus === "ok") {
                let objs = mData;
                objs.forEach(obj => {
                    messages.push(Message.parseMessage(obj));
                });
                resolve(messages);
            }
            else {
                reject(mMessage);
            }
        }));
    }
}
MessagesService.instance = new MessagesService();
class ChatScreen {
    static init() {
        if (!ChatScreen.isInit) {
            let html = `<div id="${ChatScreen.NAME}">` +
                `<div id="${ChatScreen.NAME}-header">` +
                `<button class="btn btn-light" id="${ChatScreen.NAME}-btn-profile">Profile</button>` +
                `<h1 class="text-center">Messages</h1>` +
                `<button class="btn" id="${ChatScreen.NAME}-btn-add"><i class="fa fa-pencil-square-o"></i></button>` +
                `</div>` +
                `</div>`;
            $('#app').append(html);
            $(`#${ChatScreen.NAME}-btn-add`).click(ChatScreen.showAddMessageScreen);
            $(`#${ChatScreen.NAME}-btn-profile`).click(ChatScreen.showProfileScreen);
            ChatScreen.isInit = true;
        }
    }
    static showMessageDetail(message) {
        MessageDetailScreen.setMessage(message);
        MessageDetailScreen.setCloseBtnListener(ChatScreen.show);
        MessageDetailScreen.show();
        ChatScreen.hide();
    }
    static showProfileScreen() {
        ProfileScreen.show();
        ProfileScreen.setCloseBtnListener(ChatScreen.show);
        ChatScreen.hide();
    }
    static showAddMessageScreen() {
        AddMessageScreen.show();
        AddMessageScreen.setCloseBtnListener(ChatScreen.show);
        ChatScreen.hide();
    }
    static hide() {
        ChatScreen.init();
        $(`#${ChatScreen.NAME}`).hide();
    }
    static show() {
        ChatScreen.init();
        $(`#${ChatScreen.NAME}`).show();
    }
    static refresh() {
        return __awaiter(this, void 0, void 0, function* () {
            ChatScreen.init();
            $(`#${ChatScreen.NAME}-table`).remove();
            $(`#${ChatScreen.NAME}`).append(`<table id="${ChatScreen.NAME}-table"></table>`);
            let messages = yield MessagesService.instance.getMessages();
            messages.forEach(message => {
                let row = $(`<tr class="${ChatScreen.NAME}-row"></tr>`);
                let rowTds = ChatScreen.createMessageRow(message);
                rowTds.forEach((td, i) => {
                    let element = $(td);
                    if (i === 0) {
                        element.click(ChatScreen.messageRowClickListener);
                    }
                    row.append(element);
                });
                $(`#${ChatScreen.NAME}-table`).append(row);
            });
            ChatScreen.setupVoteClickListeners();
            ChatScreen.setupEmailClickListener();
        });
    }
    static setupEmailClickListener() {
        $(`.${ChatScreen.NAME}-user-email`).each((i, obj) => {
            let p = $(obj);
            p.click(e => {
                let p = $(e.target);
                let email = p.text();
                console.log(email);
            });
        });
    }
    static setupVoteClickListeners() {
        $(`.btn-upvote`).click((e) => __awaiter(this, void 0, void 0, function* () {
            let btn = $(e.target);
            let id = parseInt(btn.data('id'));
            let voteContainer = btn.parent();
            console.log('upvoting message with id: ' + id);
            let success = yield MessagesService.instance.upvote(id)
                .catch(errorMessage => console.log(errorMessage));
            console.log(success);
            if (success) {
                console.log('upvote was successful');
            }
            else {
                console.log('upvote was unsuccessful');
            }
        }));
        $(`.btn-downvote`).click((e) => __awaiter(this, void 0, void 0, function* () {
            let btn = $(e.target);
            let id = parseInt(btn.data('id'));
            console.log('downvoting message with id: ' + id);
            let success = yield MessagesService.instance.downvote(id)
                .catch(errorMessage => console.log(errorMessage));
            console.log(success);
            if (success) {
                console.log('downvote was successful');
            }
            else {
                console.log('downvote was unsuccessful');
            }
        }));
    }
    static messageRowClickListener(e) {
        let textContainer = $(e.target);
        let title = textContainer.data('title');
        let id = parseInt(textContainer.data('id'));
        let body = textContainer.data('body');
        let senderEmail = textContainer.data('senderEmail');
        let dateCreated = textContainer.data('dateCreated');
        let upvoteCount = textContainer.data('upvoteCount');
        let downvoteCount = textContainer.data('downvoteCount');
        let message = new Message(id, senderEmail, title, body, dateCreated, upvoteCount, downvoteCount);
        ChatScreen.showMessageDetail(message);
    }
    static createMessageRow(message) {
        let { id, senderEmail, title, body, dateCreated, upvoteCount, downvoteCount } = message;
        let voteSum = upvoteCount - downvoteCount;
        let className = '';
        if (voteSum > 0) {
            className = 'positive';
        }
        else if (voteSum < 0) {
            className = 'negative';
        }
        return [
            `<td data-id="${id}"
			data-date-created="${dateCreated}"
			data-sender-email="${senderEmail}"
			data-title="${title}"
			data-body="${body}"
			data-upvote-count="${upvoteCount}"
			data-downvote-count="${downvoteCount}">` +
                `<h5>${title}</h5>` +
                `<p>${senderEmail}</p>` +
                `<h6>${body}</h6>` +
                `</td>`,
            `<td>` +
                `<button data-id="${id}" class="btn btn-upvote"><i class="fa fa-chevron-up"></i></button>` +
                `<p class="${className}-user-email">${voteSum}</p>` +
                `<button data-id="${id}" class="btn btn-downvote"><i class="fa fa-chevron-down"></i></button>` +
                `</td>`
        ];
    }
    static logout() {
        window.localStorage.setItem('the-buzz-authorization-token', '');
        window.localStorage.setItem('the-buzz-current-user-email', '');
        ChatScreen.hide();
    }
}
ChatScreen.NAME = "chat-screen";
ChatScreen.isInit = false;
class AddMessageScreen {
    static init() {
        if (!AddMessageScreen.isInit) {
            let div = $(`<div id="${AddMessageScreen.NAME}"><div id="${AddMessageScreen.NAME}-header"><h1 class="text-center">Add Message</h1></div></div>`);
            let form = $(`<form></form>`);
            form.submit((e) => {
                e.preventDefault();
            });
            form.append(HTMLStringCreator.createFormGroupWithTextInput(AddMessageScreen.NAME, 'Title', 'Enter title', 'title'));
            form.append(HTMLStringCreator.createFormGroupWithTextArea(AddMessageScreen.NAME, 'Body', 'body'));
            let submitBtn = $(`<button class="btn btn-warning" id="${AddMessageScreen.NAME}-btn-submit">Add Message</button>`);
            submitBtn.click(AddMessageScreen.submit);
            let cancelBtn = $(`<button id="${AddMessageScreen.NAME}-btn-cancel" class="btn btn-light" style="margin-left: 10px;">Cancel</button>`);
            form.append(submitBtn);
            form.append(cancelBtn);
            div.append(form);
            $('#app').append(div);
            AddMessageScreen.isInit = true;
        }
    }
    static submit() {
        return __awaiter(this, void 0, void 0, function* () {
            AddMessageScreen.init();
            console.log('trying to add message');
            let title = $(`#${AddMessageScreen.NAME}-field`).val().toString();
            let body = $(`#${AddMessageScreen.NAME}-textarea`).val().toString();
            let email = "adg219@lehigh.edu";
            let success = yield MessagesService.instance.postMessage(email, title, body)
                .catch(errorMessage => window.alert(errorMessage));
            if (success) {
                console.log('message added successfully');
                AddMessageScreen.hide();
                ChatScreen.refresh();
                ChatScreen.show();
            }
            else {
                console.log('message could not be added');
            }
        });
    }
    static resetForm() {
        $(`#${AddMessageScreen.NAME}-field`).val('');
        $(`#${AddMessageScreen.NAME}-textarea`).val('');
    }
    static hide() {
        AddMessageScreen.init();
        AddMessageScreen.resetForm();
        $(`#${AddMessageScreen.NAME}`).hide();
    }
    static show() {
        AddMessageScreen.init();
        $(`#${AddMessageScreen.NAME}`).show();
    }
    static setCloseBtnListener(handler) {
        $(`#${AddMessageScreen.NAME}-btn-cancel`).click(() => {
            AddMessageScreen.hide();
            handler();
        });
    }
}
AddMessageScreen.NAME = 'add-message-form';
AddMessageScreen.isInit = false;
class AuthScreen {
    static init() {
        if (!AuthScreen.isInit) {
            AuthScreen.isInit = true;
        }
    }
}
AuthScreen.NAME = 'auth-screen';
AuthScreen.isInit = false;
class UserService {
    constructor() { }
    getUserInfoWithId(email) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                url: Constants.API_URL + `/users/${email}`,
                method: 'GET'
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                resolve(User.parseUser(mData));
            }
            else {
                reject(mMessage);
            }
        }));
    }
}
UserService.instance = new UserService();
