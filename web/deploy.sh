TARGETFOLDER=../backend/src/main/resources;

WEBFOLDER=web;

rm -rf $TARGETFOLDER;
mkdir $TARGETFOLDER;
mkdir $TARGETFOLDER/$WEBFOLDER;

node_modules/typescript/bin/tsc app.ts ts/**.ts --removeComments --strict --target es2015 --outFile $TARGETFOLDER/$WEBFOLDER/app.js;

cp index.html $TARGETFOLDER/$WEBFOLDER;
cat app.css css/**.css >> $TARGETFOLDER/$WEBFOLDER/app.css;

cp node_modules/jquery/dist/jquery.min.js $TARGETFOLDER/$WEBFOLDER;
cp node_modules/bootstrap/dist/css/bootstrap.min.css $TARGETFOLDER/$WEBFOLDER;
cp node_modules/bootstrap/dist/css/bootstrap.min.css.map $TARGETFOLDER/$WEBFOLDER;
cp node_modules/bootstrap/dist/js/bootstrap.min.js $TARGETFOLDER/$WEBFOLDER;
cp node_modules/font-awesome/css/font-awesome.min.css $TARGETFOLDER/$WEBFOLDER;
cp -R node_modules/font-awesome/fonts $TARGETFOLDER/$WEBFOLDER;