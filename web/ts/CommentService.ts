/// <reference path="../Constants.ts" />
/// <reference path="MyComment.ts" />

class CommentService {
    static instance = new CommentService()

    private constructor() {}

    getCommentsWithMessageId(id: number): Promise<MyComment[]> {
        return new Promise(async (resolve, reject) => {
            let result = await $.ajax({
                url: Constants.API_URL + `/comments/${id}`,
                method: 'GET',
                dataType: 'json'
            })

            const { mStatus, mData, mMessage } = result

            if (mStatus === 'ok') {
                let comments: MyComment[] = []
                if (mData !== undefined) {
                    mData.forEach((cmtData: any) => {
                        let comment = MyComment.parseComment(cmtData)
                        comments.push(comment)
                    })
                }
                resolve(comments)
            } else {
                reject(mMessage)
            }
        })
    }

    addComment(id: number, email: string, body: string): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            let result = await $.ajax({
                url: Constants.API_URL + `/comments/${id}`,
                method: 'POST',
                data: JSON.stringify({ email, body }),
                contentType: 'application/json',
                dataType: 'json'
            })

            const { mStatus, mData, mMessage } = result

            if (mStatus === 'ok') {
                resolve(true)
            } else {
                reject(mMessage)
            }
        })
    }
}