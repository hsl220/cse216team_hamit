/// <reference path="HTMLStringCreator.ts" />
/// <reference path="CommentService.ts" />
/// <reference path="Message.ts" />
/// <reference path="MessagesService.ts" />

class MessageDetailScreen {
	// has to contain the message title, body, sender, datecreated
	// also has to get all the comments and show those in a list
	private static NAME = 'message-detail-screen'
	private static isInit = false

	private static selectedMessage: Message
	static setMessage(message: Message) {
		MessageDetailScreen.selectedMessage = message
	}

	private static init() {
		if (!MessageDetailScreen.isInit) {
			let div = $(`<div id="${MessageDetailScreen.NAME}"><div id="${MessageDetailScreen.NAME}-header"><button id="${MessageDetailScreen.NAME}-btn-back" class="btn"><i class="fa fa-times"></i></button><h1 class="text-center">Message Detail</h1></div></div>`)
			let card = HTMLStringCreator.createMessageCard(MessageDetailScreen.NAME)

			let header = `<h5 id="${MessageDetailScreen.NAME}-comments-count">Comments</h5>`

			let commentForm = $(`<form id="${MessageDetailScreen.NAME}-comment-form">${MessageDetailScreen.createFormGroupWithTextArea('Add comment', 'add-comment', 2, `${MessageDetailScreen.NAME}-add-comment-form`)}</form>`)
			commentForm.submit(e => e.preventDefault())

			let addCommentBtn = $(`<button id="${MessageDetailScreen.NAME}-add-comment-btn" class="btn btn-warning">Submit Comment</button>`)
			addCommentBtn.click(MessageDetailScreen.submitComment)

			// // form.append(editBtn)
			// form.append(closeBtn)
			div.append(card)
			commentForm.append(addCommentBtn)
			div.append(commentForm)
			div.append(header)
			$('#app').append(div)
			MessageDetailScreen.isInit = true
		}
	}

	private static async submitComment() {
		console.log('submitting comment')
		let email = 'adg219@lehigh.edu'
		let body = $(`#${MessageDetailScreen.NAME}-add-comment-textarea`).val().toString()

		console.log(body)

		try {
			let success = await CommentService.instance.addComment(MessageDetailScreen.selectedMessage.id, email, body)

			if (success) {
				console.log('added comment successfully')
				MessageDetailScreen.refreshComments()
				$(`#${MessageDetailScreen.NAME}-add-comment-textarea`).val('')
			}
		} catch (error) {
			window.alert(error)
		}
	}

	static addEditBtnClickListener(handler: JQuery.EventHandler<HTMLElement>) {
		MessageDetailScreen.init()
		$(`#${MessageDetailScreen.NAME}-btn-edit`).click(handler)
	}

	static addBackBtnClickListener(handler: JQuery.EventHandler<HTMLElement>) {
		MessageDetailScreen.init()
		$(`#${MessageDetailScreen.NAME}-btn-back`).click(MessageDetailScreen.hide)
	}

	static submit() {
		MessageDetailScreen.init()
		// get the text from the fields
		let title = $(`#${MessageDetailScreen.NAME}-title-field`).val()
		let body = $(`#${MessageDetailScreen.NAME}-body-textarea`).val()

		MessageDetailScreen.hide()
	}

	private static resetForm() {
		$(`#${MessageDetailScreen.NAME}-title-field`).val('')
		$(`#${MessageDetailScreen.NAME}-body-textarea`).val('')
		$(`#${MessageDetailScreen.NAME}-add-comment-textarea`).val('')
	}

	private static createFormGroupWithTextArea(label: string, s: string, rows?: number, className?:string): string {
		if (rows === undefined) {
			rows = 3
		}

		return `
			<div class="form-group ${className}">
				<label>${label}</label>
				<textarea class="form-control" id="${MessageDetailScreen.NAME}-${s}-textarea" rows="${rows}"></textarea>
			</div>
		`
	}

	private static async refreshComments() {
		$(`#${MessageDetailScreen.NAME}-comments`).remove()

		try {
			// get comments
			let comments = await CommentService.instance.getCommentsWithMessageId(MessageDetailScreen.selectedMessage.id)

			$(`#${MessageDetailScreen.NAME}-comments-count`).text(`Comments (${comments.length})`)

			let table = $(`<table id="${MessageDetailScreen.NAME}-comments"></table>`)

			// render comment list
			comments.forEach(comment => {
				let row = HTMLStringCreator.createCommentCard(MessageDetailScreen.NAME, comment)
				table.append(row)
			})

			$(`#${MessageDetailScreen.NAME}`).append(table)

			$(`.${MessageDetailScreen.NAME}-message-author-email`).each((i, obj) => {
				let p = $(obj)
				p.click(e => {
					let email = $(e.currentTarget).text().split(' ')[0]
					ProfileScreen.setSelectedUserWithEmail(email, MessageDetailScreen.show)
					MessageDetailScreen.hide()
				})
			})
		} catch (error) {
			window.alert(error)
		}
	}

	static hide() {
		MessageDetailScreen.init()
		MessageDetailScreen.resetForm()
		$(`#${MessageDetailScreen.NAME}-edit-link-container`).remove()
		$(`#${MessageDetailScreen.NAME}`).hide()
	}

	static setCloseBtnListener(handler: () => void) {
		MessageDetailScreen.init()
		MessageDetailScreen.hide()
		$(`#${MessageDetailScreen.NAME}-btn-back`).click(() => {
			MessageDetailScreen.hide()
			handler()
		})
	}

	private static createEditForm(): string {
		return `<form>` +
					`<div class="form-group">` +
						`<label for="title">Title</label>` +
						`<input type="text" class="form-control" id="${MessageDetailScreen.NAME}-edit-field" aria-describedby="titleHelp">` +
					`</div>` +
					`<div class="form-group">` +
						`<label for="body">Body</label>` +
						`<textarea class="form-control" id="${MessageDetailScreen.NAME}-edit-textarea" rows="3"></textarea>` +
					`</div>` +
					`<button id="${MessageDetailScreen.NAME}-edit-save-btn" class="btn btn-warning">Save</button>` +
					`<button id="${MessageDetailScreen.NAME}-edit-cancel-btn" class="btn">Cancel</button>` +
				`</form>`
	}

	private static refreshMessage() {
		const { title, senderEmail, body } = MessageDetailScreen.selectedMessage
		$(`#${MessageDetailScreen.NAME}-message-title`).text(title)
		$(`#${MessageDetailScreen.NAME}-message-subtitle`).text(senderEmail)
		$(`#${MessageDetailScreen.NAME}-message-text`).text(body)
	}

	static show() {
		MessageDetailScreen.init()

		MessageDetailScreen.refreshMessage()
		$(`#${MessageDetailScreen.NAME}-edit-link-container`).remove()

		if (MessageDetailScreen.selectedMessage.senderEmail === 'adg219@lehigh.edu') {
			let ul = $(`<ul id="${MessageDetailScreen.NAME}-edit-link-container" class="list-group list-group-flush"></ul>`)
			let li = $(`<li class="list-group-item"></li>`)
			let editLink = $(`<a href="#" class="card-link float-right">Edit</a>`)
			editLink.click(e => {
				let editForm = $(MessageDetailScreen.createEditForm())
				editForm.submit(e => {
					e.preventDefault()
				})
				let div = $(`<div id="${MessageDetailScreen.NAME}-edit-form"></div>`)
				div.append(editForm)
				$(`body`).append(div)

				$(`#${MessageDetailScreen.NAME}-edit-field`).val(MessageDetailScreen.selectedMessage.title)
				$(`#${MessageDetailScreen.NAME}-edit-textarea`).val(MessageDetailScreen.selectedMessage.body)

				$(`#${MessageDetailScreen.NAME}-edit-cancel-btn`).click(e => $(`#${MessageDetailScreen.NAME}-edit-form`).remove())
				$(`#${MessageDetailScreen.NAME}-edit-save-btn`).click(async e => {
					let title = $(`#${MessageDetailScreen.NAME}-edit-field`).val().toString()
					let body = $(`#${MessageDetailScreen.NAME}-edit-textarea`).val().toString()

					try {
						let success = await MessagesService.instance.updateMessage(MessageDetailScreen.selectedMessage.id, title, body)

						if (success) {
							$(`#${MessageDetailScreen.NAME}-edit-form`).remove()
							MessageDetailScreen.selectedMessage.update(title, body)
							MessageDetailScreen.refreshMessage()
						}
					} catch (error) {
						window.alert(error)
					}
				})

			})
			li.append(editLink)
			ul.append(li)
			$(`#${MessageDetailScreen.NAME}-message-card`).append(ul)
		}

		MessageDetailScreen.refreshComments()
		$(`#${MessageDetailScreen.NAME}`).show()
	}
}