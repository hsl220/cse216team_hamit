class MyNavigation {
	private static prevScrollPos = 0

	static showingNewPage() {
		MyNavigation.prevScrollPos = window.scrollY
		window.scrollTo(0,0)
	}

	static closingPage() {
		window.scrollTo(0, MyNavigation.prevScrollPos)
	}
}