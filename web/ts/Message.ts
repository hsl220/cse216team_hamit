class Message {
	id: number
	title: string
	body: string
	senderEmail: string
	dateCreated: string
	upvoteCount: number
	downvoteCount: number

	constructor(id: number, senderEmail: string, title: string, body: string, dateCreated: string, upvoteCount: number, downvoteCount: number) {
		this.id = id
		this.title = title
		this.body = body
		this.senderEmail = senderEmail
		this.dateCreated = dateCreated
		this.upvoteCount = upvoteCount
		this.downvoteCount = downvoteCount
	}

	static parseMessage(data: any): Message {
		const {
			mId,
			mTitle,
			mBody,
			mSenderEmail,
			mDateCreated,
			mUpvoteCount,
			mDownvoteCount } = data


		return new Message(mId, mSenderEmail, mTitle, mBody, mDateCreated, mUpvoteCount, mDownvoteCount)
	}

	update(title: string, body: string) {
		this.title = title
		this.body = body
	}
}