class MyComment {
	id: number
	authorEmail: string
	body: string
	createdDate: string

	constructor(id: number, authorEmail: string, body: string, createdDate: string) {
		this.id = id
		this.authorEmail = authorEmail
		this.body = body
		this.createdDate = createdDate
	}

	static parseComment(data: any): MyComment {
		const { mId, mAuthorEmail, mBody, mCreatedDate } = data
		return new MyComment(mId, mAuthorEmail, mBody, mCreatedDate)
	}
}