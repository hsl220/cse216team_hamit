class Vote{

    email: string;
    id: number;
    type: number;


    constructor(email: string, id: number, type: number) {
        this.email = email;
        this.id = id;
        this.type = type;
    }

    static parseMessage(obj: any): Vote {
        const { email, id, type } = obj
        return new Vote(email,id, type);
    }
}