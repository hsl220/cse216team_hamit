/// <reference path="User.ts" />
class HTMLStringCreator {

    static createMessageCard(componentName: string): string {
        return `<div id="${componentName}-message-card" class="card">` +
                    `<div class="card-body">` +
                        `<h5 id="${componentName}-message-title" class="card-title"></h5>` +
                        `<h6 id="${componentName}-message-subtitle" class="card-subtitle mb-2 text-muted"></h6>` +
                        `<p id="${componentName}-message-text" class="card-text"></p>` +
                    `</div>` +
                `</div>`
    }

    static createMessageCardWithMessage(componentName: string, message: Message): string {
        const { title, body, senderEmail, dateCreated } = message
        return `<div id="${componentName}-message-card" class="card">` +
                    `<div class="card-body">` +
                        `<h5 class="${componentName}-message-title" class="card-title">${title}</h5>` +
                        `<h6 class="${componentName}-message-subtitle" class="card-subtitle mb-2 text-muted">${senderEmail} - ${dateCreated}</h6>` +
                        `<p class="${componentName}-message-text" class="card-text">${body}</p>` +
                    `</div>` +
                `</div>`
    }

    static createCommentCard(componentName: string, comment: MyComment): string {
        return `<div class="card ${componentName}-comment-card">` +
                    `<div class="card-body">` +
                        `<h6 class="${componentName}-message-author-email" class="card-subtitle mb-2 text-muted">${comment.authorEmail}</h6>` +
                        `<p class="${componentName}-message-text" class="card-text">${comment.body}</p>` +
                    `</div>` +
                `</div>`
    }

    static createFormGroupWithTextInput(className: string, label: string, placeholder: string, s: string): string {
		return `<div class="form-group">` +
					`<label for="${s}">${label}</label>` +
					`<input type="text" class="form-control" id="${className}-field" aria-describedby="${s}Help" placeholder="${placeholder}">` +
				`</div>`
	}

	static createFormGroupWithTextArea(className: string, label: string, s: string): string {

		return `<div class="form-group">` +
					`<label for="${s}">${label}</label>` +
					`<textarea class="form-control" id="${className}-textarea" rows="3"></textarea>` +
				`</div>`
    }

    static createProfileHTML(user: User): string {
        const { email, name } = user
        return `<div class="card" id="profile-card">` +
                    `<div class="card-body">` +
                        `<p id="profile-name" class="card-text">${name}</p>` +
                        `<p id="profile-email" class="card-text">${email}</p>` +
                    `</div>` +
                `</div>`
    }
}
