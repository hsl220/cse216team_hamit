/// <reference path="./MyNavigation.ts" />
/// <reference path="./ChatScreen.ts" />
/// <reference path="./HTMLStringCreator.ts" />

class AddMessageScreen {
	private static NAME = 'add-message-form'
	private static isInit = false

	private static init() {
		if (!AddMessageScreen.isInit) {
			let div = $(`<div id="${AddMessageScreen.NAME}"><div id="${AddMessageScreen.NAME}-header"><h1 class="text-center">Add Message</h1></div></div>`)
			let form = $(`<form></form>`)
			form.submit((e) => {
				e.preventDefault();
			})
			form.append(HTMLStringCreator.createFormGroupWithTextInput(AddMessageScreen.NAME, 'Title', 'Enter title', 'title'))
			form.append(HTMLStringCreator.createFormGroupWithTextArea(AddMessageScreen.NAME, 'Body', 'body'))

			let submitBtn = $(`<button class="btn btn-warning" id="${AddMessageScreen.NAME}-btn-submit">Add Message</button>`)
			submitBtn.click(AddMessageScreen.submit)

			let cancelBtn = $(`<button id="${AddMessageScreen.NAME}-btn-cancel" class="btn btn-light" style="margin-left: 10px;">Cancel</button>`)				

			let chooseFileBtn = $(`<button id="${AddMessageScreen.NAME}-btn" class="btn btn-light" style="margin-left: 10px;">Choose File</button>`)

			form.append(submitBtn)
			form.append(cancelBtn)
			form.append(chooseFileBtn)
			div.append(form)
			$('#app').append(div)
			AddMessageScreen.isInit = true
		}
	}

	static async submit() {
		AddMessageScreen.init()

		console.log('trying to add message')

		// get the text from the fields
		let title = $(`#${AddMessageScreen.NAME}-field`).val().toString()
		let body = $(`#${AddMessageScreen.NAME}-textarea`).val().toString()
		let email = "adg219@lehigh.edu"

		let success = await MessagesService.instance.postMessage(email, title, body)
								.catch(errorMessage => window.alert(errorMessage))

		if (success) {
			console.log('message added successfully')
			AddMessageScreen.hide() // call AddMessageScreen only when the request was successful
			ChatScreen.refresh()
			ChatScreen.show()
		} else {
			console.log('message could not be added')
		}
	}

	private static resetForm() {
		$(`#${AddMessageScreen.NAME}-field`).val('')
		$(`#${AddMessageScreen.NAME}-textarea`).val('')
	}

	static hide() {
		AddMessageScreen.init()
		AddMessageScreen.resetForm()
		$(`#${AddMessageScreen.NAME}`).hide()
	}

	static show() {
		AddMessageScreen.init()
		$(`#${AddMessageScreen.NAME}`).show()
	}

	static setCloseBtnListener(handler: () => void) {
		$(`#${AddMessageScreen.NAME}-btn-cancel`).click(() => {
			AddMessageScreen.hide()
			handler()
		})
	}
}