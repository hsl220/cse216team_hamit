/// <reference path="User.ts" />
/// <reference path="UserService.ts" />
/// <reference path="MessagesService.ts" />

class ProfileScreen {

    private static NAME = "profile-screen"
    private static isInit = false

    private static selectedUser: User
    static async setSelectedUserWithEmail(email: string, callback: () => void) {
        try {
            let user = await UserService.instance.getUserInfoWithEmail(email)
            ProfileScreen.selectedUser = user
            ProfileScreen.show()
            ProfileScreen.setCloseBtnHandler(callback)
        } catch (error) {
            window.alert(error)
        }
    }

    private static setCloseBtnHandler(afterCloseHandler: () => void) {
        $(`#${ProfileScreen.NAME}-btn-close`).click(() => {
            ProfileScreen.hide()
            afterCloseHandler()
        })
    }

    private static init() {
        if (!ProfileScreen.isInit) {
            var user = new User('adg219@lehigh.edu', 'alexander greene')

            let html = `<div id="${ProfileScreen.NAME}">` +
                            `<header id="${ProfileScreen.NAME}-header">` +
                                `<button id="${ProfileScreen.NAME}-btn-close" class="btn"><i class="fa fa-times"></i></button>` +
                                `<h1>Profile</h1>` +
                            `</header>` +
                        `</div>`

            $('#app').append(html)

            ProfileScreen.isInit = true
        }
    }

    static async refresh() {
        ProfileScreen.init()

        $('#profile-card').remove()
        $(`#${ProfileScreen.NAME}-messages-table`).remove()
        $(`#${ProfileScreen.NAME}-messages-header`).remove()

        $(`#${ProfileScreen.NAME}`).append(HTMLStringCreator.createProfileHTML(ProfileScreen.selectedUser))

        try {
            let messages = await MessagesService.instance.getMessagesWithAuthorEmail(ProfileScreen.selectedUser.email)
            // add messages table
            let table = $(`<table id="${ProfileScreen.NAME}-messages-table"></table>`)
            messages.forEach(message => {
                table.append(HTMLStringCreator.createMessageCardWithMessage(ProfileScreen.NAME, message))
            })
            let messagesHeader = $(`<h4 id="${ProfileScreen.NAME}-messages-header">Messages (${messages.length})</h4>`)

            $(`#${ProfileScreen.NAME}`).append(messagesHeader)
            $(`#${ProfileScreen.NAME}`).append(table)
        } catch (error) {
            window.alert(error)
        }
    }

    static show() {
        ProfileScreen.init()
        $(`#${ProfileScreen.NAME}`).show()
        ProfileScreen.refresh()
    }

    static hide() {
        ProfileScreen.init()
        $(`#${ProfileScreen.NAME}`).hide()
        $(`#profile-card`).remove()
    }
}