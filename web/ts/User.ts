class User {
    email: string
    name: string

    constructor(email: string, name: string) {
        this.email = email
        this.name = name
    }

    static parseUser(data: any): User {
        const { mEmail, mName } = data
        return new User(mEmail, mName)
    }
}