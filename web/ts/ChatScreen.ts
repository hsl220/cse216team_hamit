/// <reference path='ProfileScreen.ts' />
/// <reference path='AddMessageScreen.ts' />
/// <reference path='MessageDetailScreen.ts' />
/// <reference path='MessagesService.ts' />

class ChatScreen {
	private static NAME = "chat-screen"
	private static isInit = false

	private static init() {
		if (!ChatScreen.isInit) {
			let html = `<div id="${ChatScreen.NAME}">` +
							`<div id="${ChatScreen.NAME}-header">` +
								`<button class="btn btn-light" id="${ChatScreen.NAME}-btn-profile">Profile</button>` +
								`<h1 class="text-center">Messages</h1>` +
								`<button class="btn" id="${ChatScreen.NAME}-btn-add"><i class="fa fa-pencil-square-o"></i></button>` +
							`</div>` +
						`</div>`
			$('#app').append(html)

			$(`#${ChatScreen.NAME}-btn-add`).click(ChatScreen.showAddMessageScreen)

			$(`#${ChatScreen.NAME}-btn-profile`).click(e => {
				ChatScreen.showProfileScreen('adg219@lehigh.edu')
			})

			ChatScreen.isInit = true
		}
	}

	private static showMessageDetail(message: Message) {
		MessageDetailScreen.setMessage(message)
		MessageDetailScreen.setCloseBtnListener(ChatScreen.show)
		MessageDetailScreen.show()
		ChatScreen.hide()
	}

	private static showProfileScreen(email: string) {
		ProfileScreen.setSelectedUserWithEmail(email, ChatScreen.show)
		ChatScreen.hide()
	}

	private static showAddMessageScreen() {
		AddMessageScreen.show()
		AddMessageScreen.setCloseBtnListener(ChatScreen.show)
		ChatScreen.hide()
	}

	static hide() {
		ChatScreen.init()
		$(`#${ChatScreen.NAME}`).hide()
	}

	static show() {
		ChatScreen.init()
		$(`#${ChatScreen.NAME}`).show()
	}

	static async refresh() {
		ChatScreen.init()

		$(`#${ChatScreen.NAME}-table`).remove()

		$(`#${ChatScreen.NAME}`).append(`<table id="${ChatScreen.NAME}-table"></table>`)

		// TODO: perform ajax request to get the messages
		let messages = await MessagesService.instance.getMessages('adg219@lehigh.edu')

		messages.forEach(message => {
			// TODO:
			// - get the voteType for userEmail and messageId
			// - if there is one, then apply positive class to upvote btn
			// - 	or apply negative class to downvote btn (indicates to the user that they have already voted)

			let row = $(`<tr class="${ChatScreen.NAME}-row"></tr>`)

			// TODO: pass the voteType into createMessageRow and add the voteType to the data prop
			let rowTds = ChatScreen.createMessageRow(message)

			rowTds.forEach((td, i) => {
				let element = $(td)

				if (i === 0) {
					element.click(ChatScreen.messageRowClickListener)
				}

				row.append(element)
			})

			$(`#${ChatScreen.NAME}-table`).append(row)
		})

		ChatScreen.setupVoteClickListeners()
		ChatScreen.setupEmailClickListener()
	}

	private static setupEmailClickListener() {
		$(`.${ChatScreen.NAME}-user-email`).each((i, obj) => {
			let p = $(obj)
			p.click(e => {
				e.stopPropagation();
				e.preventDefault();
				let p = $(e.currentTarget)
				let email = p.text().split(' ')[0]

				console.log(email)

				// show profile page for user with email
				ChatScreen.showProfileScreen(email)
			})
		})
	}

	static updateMessageRow() {
		// TODO:
	}

	private static setupVoteClickListeners() {
		$(`.btn-upvote`).click(async e => {
			let btn = $(e.target)
			let id = parseInt(btn.data('id'))
			let email = 'adg219@lehigh.edu'

			// upvote the message with the id

			// TODO:
			// - check if the voteType for userEmail and messageId exists and is the same as 1
			// - if the voteType does not exist (=0) then upvote like normal
			// - if it is, then delete the vote
			// - else update vote type
			try {
				let success = await MessagesService.instance.upvote(email, id)

				if (success) {
					ChatScreen.updateVoteSum(id, 1)
				} else {
					console.log('upvote was unsuccessful')
				}
			} catch (error) {
				window.alert(error)
			}
		})

		$(`.btn-downvote`).click(async e => {
			let btn = $(e.target)
			let id = parseInt(btn.data('id'))
			let email = 'adg219@lehigh.edu'

			// upvote the message with the id
			// TODO:
			// - check if the voteType for userEmail and messageId exists and is the same as -1
			// - if the voteType does not exist (=0) then downvote like normal
			// - if it is, then delete the vote
			// - else update vote type
			try {
				let success = await MessagesService.instance.downvote(email, id)

				if (success) {
					ChatScreen.updateVoteSum(id, -1)
				} else {
					console.log('downvote was unsuccessful')
				}
			} catch (error) {
				window.alert(error)
			}
		})
	}

	private static updateVoteSum(id: number, value: number) {
		$(`.${ChatScreen.NAME}-message-vote-sum`).each((i, obj) => {
			let p = $(obj)
			let messageId = parseInt(p.data('id'))
			if (messageId === id) {

				let sum = parseInt(p.text())
				let newSum = sum + value
				if (newSum > 0) {
					p.addClass('positive')
					p.removeClass('negative')
				} else if (newSum < 0) {
					p.addClass('negative')
					p.removeClass('positive')
				} else {
					p.removeClass('negative')
					p.removeClass('positive')
				}

				p.text(newSum)
			}
		})
	}

	static messageRowClickListener(e: any) {
		let textContainer = $(e.currentTarget)
		let title = textContainer.data('title')
		let id = parseInt(textContainer.data('id'))
		let body = textContainer.data('body')
		let senderEmail = textContainer.data('senderEmail')
		let dateCreated = textContainer.data('dateCreated')
		let upvoteCount = textContainer.data('upvoteCount')
		let downvoteCount = textContainer.data('downvoteCount')

		let message = new Message(id, senderEmail, title, body, dateCreated, upvoteCount, downvoteCount)

		ChatScreen.showMessageDetail(message)
	}

	private static createMessageRow(message: Message): string[] {
		let { id, senderEmail, title, body, dateCreated, upvoteCount, downvoteCount } = message

		//TODO: get user name using the senderEmail

		let voteSum = upvoteCount - downvoteCount
		let className = ''

		if (voteSum > 0) {
			className = 'positive'
		} else if (voteSum < 0) {
			className = 'negative'
		}

		return [
			`<td data-id="${id}"
			data-date-created="${dateCreated}"
			data-sender-email="${senderEmail}"
			data-title="${title}"
			data-body="${body}"
			data-upvote-count="${upvoteCount}"
			data-downvote-count="${downvoteCount}">` +
				`<h5>${title}</h5>` +
				`<p class="${ChatScreen.NAME}-user-email">${senderEmail} - ${dateCreated}</p>` +
				`<h6>${body}</h6>` +
			`</td>`,

			`<td>` +
				`<button data-id="${id}" class="btn btn-upvote"><i class="fa fa-chevron-up" data-id="${id}"></i></button>` +
				`<p class="${ChatScreen.NAME}-message-vote-sum ${className}" data-id="${id}">${voteSum}</p>` +
				`<button data-id="${id}" class="btn btn-downvote"><i class="fa fa-chevron-down" data-id="${id}"></i></button>` +
			`</td>`
		]
	}

	private static logout() {
		window.localStorage.setItem('the-buzz-authorization-token', '')
		window.localStorage.setItem('the-buzz-current-user-email', '')
		ChatScreen.hide()

		// show the google sign in button
	}
}