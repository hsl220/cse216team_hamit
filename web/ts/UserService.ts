/// <reference path="User.ts" />

class UserService {
    static instance = new UserService()

    private constructor() {}

    getUserInfoWithEmail(email: string): Promise<User> {
        return new Promise(async (resolve, reject) => {
            let result = await $.ajax({
                url: Constants.API_URL + `/users/${email}`,
                method: 'GET',
                dataType: 'json'
            })

            const { mStatus, mData, mMessage } = result

            if (mStatus === 'ok') {
                resolve(User.parseUser(mData))
            } else {
                reject(mMessage)
            }
        })
    }
}