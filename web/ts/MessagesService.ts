/// <reference path="Message.ts" />
/// <reference path="../Constants.ts" />
class MessagesService {
    static instance: MessagesService = new MessagesService()

    private MessagesService() {}

    upvote(email: string, messageId: number): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            let result = await $.ajax({
                type: 'POST',
                url: Constants.API_URL + `/votes`,
                data: JSON.stringify({ email, messageId, type: 1 }),
                contentType: 'application/json',
                dataType: 'json'
            })

            const { mStatus, mData, mMessage } = result

            if (mStatus === 'ok') {
                resolve(true)
            } else {
                reject(mMessage)
            }
        })
    }

    // when a vote is made, make sure to also update the vote table to
    // prevent the same user from voting multiple times on a message

    downvote(email: string, messageId: number): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            let result = await $.ajax({
                type: 'POST',
                url: Constants.API_URL + `/votes`,
                data: JSON.stringify({ email, messageId, type: -1 }),
                contentType: 'application/json',
                dataType: 'json'
            })

            const { mStatus, mData, mMessage } = result

            if (mStatus === "ok"){
                resolve(true)
            }else {
                reject(mMessage)
            }
        })
    }

    postMessage(email: string, title: string, body: string): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            let result: any = await $.ajax({
                type: 'POST',
                url: Constants.API_URL + "/messages",
                contentType: 'application/json',
                data: JSON.stringify({ email, title, body }),
                dataType: 'json'
            })
            .catch(error => console.log(error))

            const { mStatus, mData, mMessage } = result

            if (mStatus === 'ok') {
                resolve(true)
            } else {
                reject(mMessage)
            }
        })
    }

    getMessages(email: string): Promise<Message[]> {
        return new Promise(async (resolve, reject) => {
            let result = await $.ajax({
                type: "GET",
                url: Constants.API_URL + `/messages-with-blocker-email/${email}`,
                dataType: 'json'
            })

            const { mStatus, mData, mMessage } = result
            let messages: Message[] = []

            if (mStatus === "ok"){
                let objs: [any] = mData;
                objs.forEach(obj => {
                    messages.push(Message.parseMessage(obj));
                });
                resolve(messages)
            } else {
                reject(mMessage)
            }
        });
    }

    getMessagesWithAuthorEmail(email: string): Promise<Message[]> {
        return new Promise(async (resolve, reject) => {
            let result = await $.ajax({
                url: Constants.API_URL + `/messages-with-email/${email}`,
                method: 'GET',
                dataType: 'json'
            })

            const { mStatus, mData, mMessage } = result

            let messages: Message[] = []

            if (mStatus === 'ok') {
                let objects: [any] = mData
                objects.forEach(obj => {
                    let message = Message.parseMessage(obj)
                    messages.push(message)
                })
                resolve(messages)
            } else {
                reject(mMessage)
            }
        })
    }

    updateMessage(id: number, title: string, body: string): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            let result = await $.ajax({
                url: Constants.API_URL + `messages/${id}`,
                method: 'PUT',
                data: JSON.stringify({ title, body }),
                dataType: 'json'
            })

            const { mStatus, mData, mMessage } = result

            if (mStatus === 'ok') {
                resolve(true)
            } else {
                reject(mMessage)
            }
        })
    }
}