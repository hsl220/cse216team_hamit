/// <reference path="./ts/ChatScreen.ts" />

const API_URL = 'https://glacial-fjord-88850.herokuapp.com'

$(() => {

    ChatScreen.refresh()

    // if (checkIfLoggedIn) {
    //     ChatScreen.refresh()
    // } else {
    //     $('body').append('<button id="signinButton">Sign in with Google</button>')
    //     gapi.load('auth2', function() {
    //         auth2 = gapi.auth2.init({
    //           client_id: '760530349142-p66fpijr1lrohcrbsonrio5ef717c4d2.apps.googleusercontent.com',
    //           // Scopes to request in addition to 'profile' and 'email'
    //           //scope: 'additional_scope'
    //         });
    //       });

    //     $('#signinButton').click(function() {
    //         // signInCallback defined in step 6.
    //         auth2.grantOfflineAccess().then(signInCallback);
    //       });
    // }
})

function readText() {
  document.getElementById("img1").src = "data:image/png;base64," + document.getBase64(file)
}

function uploadImage() {
  function getBase64(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function() {
      document.getElementById("img2").src = reader.result;
    };
  }
}
/** 
<button onclick="readText();">Display</button>
<input type="file" id="uploaded"/>
<button onclick="uploadImage();">Upload</button>  */

function checkIfLoggedIn(): boolean {
    let token = window.localStorage.getItem('the-buzz-authorization-token')
    return (token === undefined || token === null || token.length < 1) ? false : true
}

function signInCallback(authResult) {
    if (authResult['code']) {

      // Hide the sign-in button now that the user is authorized, for example:
      $('#signinButton').attr('style', 'display: none');

      // Send the code to the server
      $.ajax({
        type: 'POST',
        url: 'https://glacial-fjord-88850.herokuapp.com/auth',
        // Always include an `X-Requested-With` header in every AJAX request,
        // to protect against CSRF attacks.
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        contentType: 'application/octet-stream; charset=utf-8',
        success: function(result) {
          console.log('result from google: ', result)

          // get the user information: email, profile, etc

          // store that information in local storage
          // let currentUserEmail = email
          // window.localStorage.setItem('the-buzz-current-user-email', currentUserEmail)
        },
        processData: false,
        data: authResult['code']
      });
    } else {
      // There was an error.
    }
  }