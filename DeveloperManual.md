# The Buzz - Developer Manual
#### CSE216Team_HAMIT
##### Team members:
- Hannah Lee
- Travis Barnes
- Isaiah Morales
- Maxime Martin
- Alexander Greene

<br>
## Download
Download the source code from BitBucket repo.
Link: https://bitbucket.org/hsl220/cse216team_hamit/src

<br>
## What is The Buzz?
It's an app that was developed to help our organization communicate effectively with all of its members. It is a chat application that facilitates the congregation of innovative thoughts and ideas, and keeps everyone up to date with what's happening throughout the company.

#### Functionality:
###### General Users
- Google Sign In
- Send Messages
- Edit/Update Messages (*own)
- Share Comments For Messages
- Delete Messages (*own)
- Share files with each other
- Flag Inappropriate Messages
- Block Obnoxious Users

###### Admin
- All of the above
- Remove Users
- Delete Messages (*any)

own - operations only on messages they posted
any - operations on any messages

<br>
#### Activity Diagram
![alt text](CSE216_Activity_Diagram.png)

<br>
## Compile

##### Backend
After downloading the source code, run mvn package to compile the code.
```
mvn package
```
##### Web
After downloading the source code make sure to npm install in the terminal in the root directory for the web app to install all necessary dependencies.
```
npm install
```
That should start a local http server and you can visit the site by visiting localhost:3000.
<b>Heads up:</b> Starting the application this way will not allow you to access the backend app

##### Admin CLI
After downloading the source code, run mvn package.
```
mvn package
```
<br>
## Setup

#### Web
After running npm install to compile, you want to run npm run start. You can see what this command does in the package.json file under scripts.
```
npm run start
```

#### Backend
Before running the backend, you need to install docker on your machine and set up an instance to run the backend app on. You'll also want to setup the docker instance with PostgreSQL database. And provide the environment variables.
```
docker run -p5432:<port> --name <name-of-instance> \
 -e POSTGRES_PASSWORD=<some-password> \
 -e POSTGRES_USER=<some-username> \
 -d postgres
```
The above code should all be on one line.

Then run mvn exec:java to run the backend app and specify the variables from above
```
POSTGRES_IP=127.0.0.1 POSTGRES_PORT=<port> \
POSTGRES_USER=<some-username> \
POSTGRES_PASS=<some-password> \
mvn exec:java
```

#### Admin CLI
After running mvn package, run mvn exec:java.
```
mvn exec:java
```

#### Android
Open up the android project in Android Studio. Click on File -> Sync Gradle. After that, Click Build. Then click Clean Project, and after that, click Rebuild Project.
After doing all of that, you should be able to build and run the app using one of the stock Android Emulators.
