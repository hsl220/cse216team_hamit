The Buzz

The Buzz is a message board that allows users to post messages anonymously. Users are 
allowed to upvote and downvote messages. Messages will be displayed in order of 
having the most recent posts at the top. Our project can be run on our web front end:
glacial-fjord-88850.herokuapp.com 
or by launching our android app.

Installation:
Install Maven through Maven's website.
Install Android Studio
npm install -S http-server : allows us to run server locally

Running Tests: 

Android:
Tests the Main Activity, Second Activity (Messsage Activity), and Message object.
	Tests that the buttons on each page work properly, and message object methods 
do what they're supposed to.
Run UITests, MessageTest through AndroidStudio 


Built with:
Maven 
Android Studio
Heroku 

Contributors:
Hannah Lee
Alex Greene
Travis Barnes
Isaiah Morales
Maxime Martin


