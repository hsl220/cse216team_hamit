package hamit.cse216.lehigh.edu.thebuzz;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MessagesUnitTest {
    Messages test;


    @Before
    public void setUp() throws Exception {
        test = new Messages("Test","This is a test",0,15,5,"Today");
    }
    @Test
    public void testGetTitle() {
        assertEquals("Test", test.getTitle());
    }

    @Test
    public void testSetTitle(){
        test.setTitle("New Title");
        assertEquals("New Title", test.getTitle());
    }
    @Test
    public void testGetBody() {
        assertEquals("This is a test", test.getBody());
    }

    @Test
    public void testSetBody(){
        test.setBody("New Body");
        assertEquals("New Body", test.getBody());
    }
    @Test
    public void testGetmID() {
        assertEquals(0, test.getmID());
    }

    @Test
    public void testSetmID(){
        test.setmID(10);
        assertEquals(10, test.getmID());
    }

    @Test
    public void testGetUpvotes(){
        assertEquals(15, test.getUpvotes());
    }

    @Test
    public void testSetUpvotes(){
        test.setUpvotes(10);
        assertEquals(10, test.getUpvotes());
    }
    @Test
    public void testGetDownvotes(){
        assertEquals(5, test.getDownvotes());
    }

    @Test
    public void testSetDownvotes(){
        test.setDownvotes(20);
        assertEquals(20, test.getDownvotes());
    }
    @Test
    public void testGetDate() {
        assertEquals("Today", test.getDate());
    }

    @Test
    public void testSetDate(){
        test.setDate("Tomorrow");
        assertEquals("Tomorrow", test.getDate());
    }
}
