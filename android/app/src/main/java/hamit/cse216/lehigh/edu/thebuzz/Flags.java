package hamit.cse216.lehigh.edu.thebuzz;

public class Flags {

        private int messageId;
        private String flaggerEmail;

        public Flags(int messageId, String flaggerEmail){
            this.messageId = messageId;
            this.flaggerEmail = flaggerEmail;
        }

        public int getMessageId() { return messageId; }

        public void setMessageId(int messageId) { this.messageId = messageId; }

        public String getFlaggerEmail() { return flaggerEmail; }

        public void setFlaggerEmail() { this.flaggerEmail = flaggerEmail; }

}
