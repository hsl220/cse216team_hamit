package hamit.cse216.lehigh.edu.thebuzz;
import org.json.JSONObject;

public class Blocks {
    private String blockerEmail;
    private String blockeeEmail;

    public Blocks(String blockerEmail, String blockeeEmail){
        this.blockerEmail = blockerEmail;
        this.blockeeEmail = blockeeEmail;
    }

    public String getBlockerEmail() {
        return blockerEmail;
    }

    public void setBlockerEmail(String blockerEmail) {
        this.blockerEmail = blockerEmail;
    }

    public String getBlockeeEmail() {
        return blockeeEmail;
    }

    public void setBlockeeEmail(String blockeeEmail) {
        this.blockeeEmail = blockeeEmail;
    }

    public static Blocks parseJson(JSONObject jsonObject) {
        Blocks block = null;
        String blockerEmail;
        String blockeeEmail;

        try {

            blockerEmail = jsonObject.getString("mBlockerEmail");
            blockeeEmail = jsonObject.getString("mBlockeeEmail");

            block = new Blocks(blockerEmail, blockeeEmail);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return block;
    }
}
