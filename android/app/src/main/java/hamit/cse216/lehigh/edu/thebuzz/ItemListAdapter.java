package hamit.cse216.lehigh.edu.thebuzz;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Travis Barnes
 * 9/24/18
 * Item Adapter used to add messages to the Recycle View
 */
public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder>{
    private ArrayList<Messages> mMessages;
    private ShowsActivity activity;

    /**
     * Constructor for ItemListAdapter
     * @param data
     *      List of messages formed form GET request
     */
    ItemListAdapter(ArrayList<Messages> data, ShowsActivity activity) {
        mMessages = data;
        this.activity = activity;
    }

    /**
     * Sets layout for individual messages checkout the list_item
     * files in the layout folder. To switch layouts simply change
     * the first inflate parameter 'R.layout.your_layout_choice'
     * @param viewGroup
     * @param i
     * @return
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item2, viewGroup, false);
        return new ViewHolder(view, activity);
    }

    public void refresh(ArrayList<Messages> newMessages) {
        mMessages = newMessages;
        notifyDataSetChanged();
    }

    /**
     * You can use this method to set text and data for each specific
     * message such as setting the Title textview with the
     * title text from the message. Use the data fields of the
     * view holder below.
     * @param viewHolder
     *      the collection of view items for a specific message
     * @param position
     *      position in the message array list
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Messages d = mMessages.get(position);
        viewHolder.mTitle.setText(d.getTitle());
        viewHolder.mBody.setText(d.getBody());
        viewHolder.mDateCreated.setText("Created: " + d.getDate());
        viewHolder.mUpVotes.setText("Upvotes: " + d.getUpvotes());
        viewHolder.mDownVotes.setText("Downvotes: " + d.getDownvotes());
        viewHolder.upVoteBtn.setTag(d.getmID());
        viewHolder.downVoteBtn.setTag(d.getmID());
        viewHolder.mId = d.getmID();
    }

    /**
     * Shows how many messages there are
     * @return size of array list
     */
    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    /**
     * The view holder is used to bunch together the view items in the layout used
     * for the individual messages. Each intended item must be initialized as a data
     * field and then later assigned in the constructor.
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mTitle;
        TextView mBody;
        TextView mDateCreated;
        TextView mUpVotes;
        TextView mDownVotes;
        Button upVoteBtn;
        Button downVoteBtn;
        int mId = 0;
        private ShowsActivity activity;

        /**
         * The constructor for the ViewHolder and each data field
         * is assigned here using items in the layout
         * @param itemView
         */
        public ViewHolder(@NonNull View itemView, ShowsActivity activity) {
            super(itemView);
            this.mTitle = (TextView) itemView.findViewById(R.id.listItemTitle);
            this.mBody = (TextView) itemView.findViewById(R.id.listItemBody);
            this.mDateCreated = (TextView) itemView.findViewById(R.id.listItemDate);
            this.mUpVotes = (TextView) itemView.findViewById(R.id.listItemUpvotes);
            this.mDownVotes = (TextView) itemView.findViewById(R.id.listItemDownvotes);
            this.downVoteBtn = (Button) itemView.findViewById(R.id.DownVoteBtn);
            this.upVoteBtn = (Button) itemView.findViewById(R.id.UpVoteBtn);
            this.activity = activity;
            this.itemView.setOnClickListener(this);

            View view = itemView.findViewById(R.id.view2);
            view.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onClick(View v) {
            String title = mTitle.getText().toString().trim();
            String body = mBody.getText().toString().trim();
            String dateCreated = mDateCreated.getText().toString().trim();
            String upVotes = mUpVotes.getText().toString().trim();
            String downVotes = mDownVotes.getText().toString().trim();

            Log.d("itemListItem", String.format("title: %s, body: %s, date: %s, upvotes: %s, downvotes: %s", title, body, dateCreated, upVotes, downVotes));

            Intent intent = new Intent(activity.getActivityIntent(), MessageDetailActivity.class);
            intent.putExtra(MainActivity.MAIN_ACTIVITY_TO_MESSAGE_DETAIL_TITLE, title);
            intent.putExtra(MainActivity.MAIN_ACTIVITY_TO_MESSAGE_DETAIL_DATE, dateCreated);
            intent.putExtra(MainActivity.MAIN_ACTIVITY_TO_MESSAGE_DETAIL_BODY, body);
            intent.putExtra(MainActivity.MAIN_ACTIVITY_TO_MESSAGE_DETAIL_ID, mId);
            activity.showActivity(intent);
        }
    }

}
