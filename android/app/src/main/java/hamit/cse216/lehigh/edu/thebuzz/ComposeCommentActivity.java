package hamit.cse216.lehigh.edu.thebuzz;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.drive.Drive;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class ComposeCommentActivity extends AppCompatActivity implements VolleyCallback {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AttachedFileAdapter adapter;

    private EditText bodyText;

    private ArrayList<AttachedFile> attachedImages;
    private String mCurrentPhotoPath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.compose_comment);

        attachedImages = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView4);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AttachedFileAdapter(attachedImages, this);
        recyclerView.setAdapter(adapter);

        bodyText = findViewById(R.id.editText);
    }

    private final int GALLERY_REQUEST_CODE = 1;
    private final int READ_EXTERNAL_STORAGE_REQUEST_CODE = 2;
    private final int TAKE_PHOTO_REQUEST_CODE = 3;

    public void attachFilesBtnClicked(View view) {
        if (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, GALLERY_REQUEST_CODE);
        } else {
            String[] permissions = new String[1];
            permissions[0] = Manifest.permission.READ_EXTERNAL_STORAGE;
            ActivityCompat.requestPermissions(this, permissions, READ_EXTERNAL_STORAGE_REQUEST_CODE);
        }
    }

    public void uploadFileClicked(View view) {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        intent.setType("*/*");

        startActivityForResult(intent, FILE_REQUEST_CODE);
    }

    private final int FILE_REQUEST_CODE = 444;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == READ_EXTERNAL_STORAGE_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // pick image after request permission success
                attachFilesBtnClicked(null);
            }
        }
    }

    public void takePhotoBtnClicked(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, TAKE_PHOTO_REQUEST_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void addPhotoToGallery() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private Bitmap getTakenPhotoFromGallery() {
        // Get the dimensions of the View
        int targetW = 100;
        int targetH = 150;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
    }

    private Bitmap getBitmapFromData(Uri selectedImage) {
        String[] filePathColumn = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        return BitmapFactory.decodeFile(picturePath);
    }

    public void postComment(View view) {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
        String email = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_user_email), "");
        String body = bodyText.getText().toString().trim();

        if (body.isEmpty()) {
            Toast.makeText(this, "Please enter the comment body", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = getIntent();
        int messageId = intent.getIntExtra(MessageDetailActivity.MESSAGE_DETAIL_TO_COMPOSE_COMMENT_MESSAGE_ID, 0);
        CommentsService.getInstance().postComment(this, messageId, email, body, this);
    }

    @Override
    public void onSuccess(String response) {
        setResult(MainActivity.RESULT_OK, getIntent());
        finish();
    }

    private final int CREATE_LINK_REQUEST_CODE = 33;

    public void createLinkClicked(View view) {
        Intent intent = new Intent(this, CreateLinkActivity.class);
        startActivityForResult(intent, CREATE_LINK_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch(requestCode) {
                case GALLERY_REQUEST_CODE:
                {
                    Bitmap bitmap = getBitmapFromData(data.getData());
                    AttachedFile attachedImage = new AttachedFile(bitmap);
                    adapter.appendItem(attachedImage);
                    break;
                }

                case TAKE_PHOTO_REQUEST_CODE:
                {
                    addPhotoToGallery();

                    Bitmap imageBitmap = getTakenPhotoFromGallery();

                    AttachedFile attachedImage = new AttachedFile(imageBitmap);
                    adapter.appendItem(attachedImage);
                    break;
                }

                case CREATE_LINK_REQUEST_CODE:
                {
                    String link = data.getStringExtra(CreateLinkActivity.CREATE_LINK_ACTIVITY_LINK);
                    String placeholder = data.getStringExtra(CreateLinkActivity.CREATE_LINK_ACTIVITY_PLACEHOLDER);
                    AttachedFile linkFile = new AttachedFile(link, placeholder);
                    adapter.appendItem(linkFile);
                    break;
                }

                case FILE_REQUEST_CODE:
                {

                    Uri uri = data.getData();
                    File file = new File(uri.getPath());
                    AttachedFile attachedFile = new AttachedFile(file, uri);
                    adapter.appendItem(attachedFile);
                    break;
                }

                default:
                    break;
            }
        }
    }
}
