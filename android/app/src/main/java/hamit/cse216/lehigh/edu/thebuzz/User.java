package hamit.cse216.lehigh.edu.thebuzz;
/**
 * Travis Barnes
 * Last edited 9/24/18
 * Skeleton of a potential user object used to keep track of the current user
 * that is using app. Plans can be fleshed out in future phases.
 */
public class User {
    private String salt;
    private String signature;
    private String email;
    private String password;
    //upvoted message ids???
    //same for downvote??

    public User(String email, String password, String salt, String signature){
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.signature = signature;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
