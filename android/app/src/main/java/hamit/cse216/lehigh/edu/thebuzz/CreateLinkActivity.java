package hamit.cse216.lehigh.edu.thebuzz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CreateLinkActivity extends AppCompatActivity {

    private EditText linkText;
    private EditText placeholderText;

    public static final String CREATE_LINK_ACTIVITY_LINK = "create_link_activity_link";
    public static final String CREATE_LINK_ACTIVITY_PLACEHOLDER = "create_link_activity_placeholder";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_link);

        placeholderText = findViewById(R.id.editText2);
        linkText = findViewById(R.id.editText3);
    }

    public void createLinkClicked(View view) {
        String link = linkText.getText().toString().trim();
        String placeholder = placeholderText.getText().toString().trim();

        if (link.isEmpty() || placeholder.isEmpty()) {
            Toast.makeText(this, "Make sure to enter a link and a placeholder", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(CreateLinkActivity.CREATE_LINK_ACTIVITY_LINK, link);
        intent.putExtra(CreateLinkActivity.CREATE_LINK_ACTIVITY_PLACEHOLDER, placeholder);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
