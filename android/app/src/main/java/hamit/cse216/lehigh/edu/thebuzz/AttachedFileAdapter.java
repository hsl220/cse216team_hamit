package hamit.cse216.lehigh.edu.thebuzz;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class AttachedFileAdapter extends RecyclerView.Adapter<AttachedFileAdapter.ViewHolder> {

    private ArrayList<AttachedFile> attachedFiles;
    private AppCompatActivity activity;

    public AttachedFileAdapter(ArrayList<AttachedFile> attachedFiles, AppCompatActivity activity) {
        this.attachedFiles = attachedFiles;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layout = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.attached_file_item, viewGroup, false);
        return new ViewHolder(layout, this, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.configureViewHolder(attachedFiles.get(i));
    }

    @Override
    public int getItemCount() {
        return attachedFiles.size();
    }

    public void removeItem(AttachedFile file) {
        int index = attachedFiles.indexOf(file);
        attachedFiles.remove(file);
        notifyItemRemoved(index);
    }

    public void appendItem(AttachedFile file) {
        attachedFiles.add(file);
        notifyItemInserted(attachedFiles.size() - 1);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private View layout;
        private ImageView imageView;
        private Button removeBtn;
        private Button linkOrFileBtn;

        private AttachedFileAdapter adapter;

        private AppCompatActivity activity;

        private AttachedFile attachedFile;

        public ViewHolder(@NonNull View itemView, AttachedFileAdapter adapter, AppCompatActivity activity) {
            super(itemView);

            this.activity = activity;
            layout = itemView;
            imageView = layout.findViewById(R.id.imageView);
            removeBtn = layout.findViewById(R.id.button2);
            linkOrFileBtn = layout.findViewById(R.id.button6);
            this.adapter = adapter;
        }

        public void configureViewHolder(final AttachedFile file) {
            attachedFile = file;

            if (file.getType().contains("images")) {
                imageView.setVisibility(View.VISIBLE);
                linkOrFileBtn.setVisibility(View.INVISIBLE);

                imageView.setImageBitmap(file.getBitmap());

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // create image view and set bitmap
                        final ImageView imageView = new ImageView(activity);
                        imageView.setImageBitmap(attachedFile.getBitmap());

                        // get the size of the device's display
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int height = displayMetrics.heightPixels;
                        int width = displayMetrics.widthPixels;

                        // add the imageView to the activity with layout params
                        activity.addContentView(imageView, new ViewGroup.LayoutParams(width, height - 100));

                        // style the image and set default alpha
                        imageView.setBackgroundColor(Color.BLACK);
                        imageView.setAlpha(0f);

                        // animate the alpha of the image to 1 (make visible)
                        // and set listener for when animation ends
                        imageView.animate().alpha(1f).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                imageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // animate the alpha of the image to 0 (not visible)
                                        // set listener for end of animation
                                        v.animate().alpha(0f).setListener(new AnimatorListenerAdapter() {
                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                super.onAnimationEnd(animation);

                                                // remove the imageView from the activity
                                                ViewGroup parentView = (ViewGroup) imageView.getParent();
                                                parentView.removeView(imageView);
                                            }
                                        });
                                    }
                                });
                            }
                        });

                    }
                });
            }

            if (file.getType().equals("link")) {
                imageView.setVisibility(View.INVISIBLE);
                linkOrFileBtn.setVisibility(View.VISIBLE);

                linkOrFileBtn.setText(file.getUrlPlaceholder());
                linkOrFileBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(attachedFile.getUrl()));
                        activity.startActivity(browserIntent);
                    }
                });
            }

            if (!file.getType().equals("images") && !file.getType().equals("link")) {
                imageView.setVisibility(View.INVISIBLE);
                linkOrFileBtn.setVisibility(View.VISIBLE);

                linkOrFileBtn.setText(file.getFilename());
                linkOrFileBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(attachedFile.getFileUri());
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        if (intent.resolveActivity(activity.getPackageManager()) != null) {
                            activity.startActivity(intent);
                        }
                    }
                });
            }

            removeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("removeImage", "removing image");
                    adapter.removeItem(file);
                }
            });
        }
    }
}
