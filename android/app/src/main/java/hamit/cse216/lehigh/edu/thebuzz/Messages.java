package hamit.cse216.lehigh.edu.thebuzz;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Travis Barnes
 * Last edited 9/24/18
 * Basic message object used to hold messages from database made by users
 * Generally used to represent messages on Recycle View in the
 * main activity
 */
public class Messages{
    private String title;
    private String body;
    private String senderEmail;
    private int upvotes;
    private int downvotes;
    private String date;
    private int mID;

    private ArrayList<AttachedFile> attachedFiles = null;

    /**
     *Constructor for Message objects takes in all data fields
     * @param title
     *      title text of the message
     * @param body
     *      body text of the message
     * @param mID
     *      message id created when submitted to database
     * @param upvotes
     *      total upvotes for message
     * @param downvotes
     *      total downvotes for message
     * @param date
     *      date of when message was submitted to database
     */
    public Messages(String title, String body, int mID, String mSenderEmail, int upvotes, int downvotes, String date){
        this.title = title;
        this.body = body;
        this.mID = mID;
        this.senderEmail = mSenderEmail;
        this.upvotes = upvotes;
        this.downvotes = downvotes;
        this.date = date;
    }
    /**
     * Mutators and Accessors for all data fields
     */
    public String getTitle() { return title; }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String email) {
        this.senderEmail = email;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }

    public int getDownvotes() {
        return downvotes;
    }

    public void setDownvotes(int downvotes) {
        this.downvotes = downvotes;
    }

    public int getmID() {
        return mID;
    }

    public void setmID(int mID) {
        this.mID = mID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setAttachedImages(ArrayList<AttachedFile> attachedFiles) {
        this.attachedFiles = attachedFiles;
    }

    public static Messages parseMessage(JSONObject json) {
        String title = "", body = "", date = "", senderEmail = "";
        int upVotes = 0, downVotes = 0, mID = 0;
        try {
            title = json.getString("mTitle");
            body = json.getString("mBody");
            date = json.getString("mDateCreated");
            upVotes = json.getInt("mUpvoteCount");
            downVotes = json.getInt("mDownvoteCount");
            mID = json.getInt("mId");
            senderEmail = json.getString("mSenderEmail");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new Messages(title, body, mID, senderEmail, upVotes, downVotes, date);
    }
}
