package hamit.cse216.lehigh.edu.thebuzz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity implements VolleyCallback, ShowsActivity {

    private RecyclerView messagesRecyclerView;
    private ItemListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private String userEmail;

    private TextView userNameText;
    private TextView userEmailText;
    private TextView messagesCountText;

    public static final String PROFILE_FOR_USER_EMAIL = "profile_for_user_email";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        messagesRecyclerView = findViewById(R.id.profile_messages_recycler_view);
        adapter = new ItemListAdapter(new ArrayList<Messages>(), this);

        layoutManager = new LinearLayoutManager(this);
        messagesRecyclerView.setLayoutManager(layoutManager);

        messagesRecyclerView.setAdapter(adapter);

        Intent intent = getIntent();
        userEmail = intent.getStringExtra(ProfileActivity.PROFILE_FOR_USER_EMAIL);

        userNameText = findViewById(R.id.textView10);
        userEmailText = findViewById(R.id.textView11);
        messagesCountText = findViewById(R.id.textView12);

        getMessagesForUserProfile();
    }

    private void getMessagesForUserProfile() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
        String flaggerEmail = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_user_email), "");
        MessageService.getInstance().getMessages(this, this, flaggerEmail);
    }

    @Override
    public void onSuccess(String response) {
        ArrayList<Messages> messages = new ArrayList<>();
        try {
            JSONObject mData = new JSONObject(response);
            JSONArray json = mData.getJSONArray("mData");
            for (int i = 0; i < json.length(); ++i) {

//                ArrayList<AttachedImage> attachedImages = null;
//                JSONArray images = json.getJSONObject(i).getJSONArray("images");
//                for (int j = 0; j < images.length(); j++) {
//                    if (attachedImages == null) attachedImages = new ArrayList<>();
//
//                    String base64StringForImage = images.getString(j);
//                    AttachedImage attachedImage = new AttachedImage(base64StringForImage);
//                    attachedImages.add(attachedImage);
//                }

                Messages message = Messages.parseMessage(json.getJSONObject(i));
//                message.setAttachedImages(attachedImages);
                messages.add(message);
            }

            // filter the messages for the messages sent by this user
            ArrayList<Messages> filteredList = new ArrayList<>();
            for (Messages message : messages) {
                if (message.getSenderEmail().equals(userEmail)) {
                    filteredList.add(message);
                }
            }

            userEmailText.setText(userEmail);
            messagesCountText.setText(String.format("Messages (%d)", filteredList.size()));

            adapter.refresh(filteredList);
        } catch (final JSONException e) {
            Log.d("tjb220", "Error parsing JSON file: " + e.getMessage());
            return;
        }
    }

    @Override
    public void showActivity(Intent intent) {
        startActivity(intent);
    }

    @Override
    public Context getActivityIntent() {
        return this;
    }


    public void block(View v){
        BlocksService blocksService = new BlocksService();
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
        String blockerEmail = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_user_email), "");
        //String sessionKey = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_session_key), "");
        blocksService.postBlocks(this, blockerEmail, userEmail, this);

    }
}
