package hamit.cse216.lehigh.edu.thebuzz;

import android.content.Context;
import android.content.Intent;

public interface ShowsActivity {
    void showActivity(Intent intent);

    Context getActivityIntent();
}
