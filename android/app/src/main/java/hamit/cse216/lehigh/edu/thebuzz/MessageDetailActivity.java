package hamit.cse216.lehigh.edu.thebuzz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class MessageDetailActivity extends AppCompatActivity implements VolleyCallback, ShowsActivity {

    private TextView titleText;
    private TextView dateText;
    private TextView authorText;
    private TextView bodyText;
    private TextView attachmentsText;
    private TextView commentsText;

    private RecyclerView commentsRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CommentListAdapter commentsAdapter;

    private ArrayList<String> attachments;
    private RecyclerView attachmentsRecyclerView;

    private Button editBtn;

    private boolean authorIsCurrentUser = false;
    private boolean isFlagged = false;

    private int messageId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_detail);

        titleText = findViewById(R.id.textView);
        dateText = findViewById(R.id.textView2);
        authorText = findViewById(R.id.textView3);
        bodyText = findViewById(R.id.textView4);
        attachmentsText = findViewById(R.id.textView5);
        commentsText = findViewById(R.id.textView6);

        editBtn = findViewById(R.id.button7);

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
        String flaggerEmail = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_user_email), "");
        getFlagged(this, messageId, flaggerEmail, this);
        if (isFlagged) {
            Button flagButton = findViewById(R.id.flag_message);
            flagButton.setText("Flagged");
        }

        setEditBtnVisibility(authorIsCurrentUser);

        commentsRecyclerView = findViewById(R.id.commentsRecyclerView);
        layoutManager = new LinearLayoutManager(this);
        commentsAdapter = new CommentListAdapter(new ArrayList<Comments>(), this);
        commentsRecyclerView.setHasFixedSize(true);
        commentsRecyclerView.setLayoutManager(layoutManager);
        commentsRecyclerView.setAdapter(commentsAdapter);

        getAndSetInitialValues();
        getCommentsForMessage();
        getAttachmentsForMessage();
    }

    private void getFlagged(final Context context, int messageId, String flaggerEmail, final VolleyCallback callback) {
            //The following is the basic format of performing a GET request
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(context);
            String url = context.getString(R.string.api_url) + "flags/" + messageId + "/" + flaggerEmail;
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("tjb220", "Response: " + response);
                            callback.onSuccess(response);
                            Button flagButton = findViewById(R.id.flag_message);
                            flagButton.setText("Flagged");
                            isFlagged = true;
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("tjb220", "Get function didn't work!");
                    //Toast.makeText(context, "Could not retrieve flags", Toast.LENGTH_SHORT).show();
                }
            });
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
        }

    private void getAndSetInitialValues() {
        Intent intent = getIntent();
        String title = intent.getStringExtra(MainActivity.MAIN_ACTIVITY_TO_MESSAGE_DETAIL_TITLE);
        String author = intent.getStringExtra(MainActivity.MAIN_ACTIVITY_TO_MESSAGE_DETAIL_AUTHOR);
        String date = intent.getStringExtra(MainActivity.MAIN_ACTIVITY_TO_MESSAGE_DETAIL_DATE);
        String body = intent.getStringExtra(MainActivity.MAIN_ACTIVITY_TO_MESSAGE_DETAIL_BODY);
        int id = intent.getIntExtra(MainActivity.MAIN_ACTIVITY_TO_MESSAGE_DETAIL_ID, 0);

        messageId = id;
        titleText.setText(title);
        authorText.setText(author);
        dateText.setText(date);
        bodyText.setText(body);
    }

    public void flag(View v) {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
        String flaggerEmail = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_user_email), "");

        getFlagged(this, messageId, flaggerEmail, this);

        if (!isFlagged) {
            FlagsService flagsService = new FlagsService();
            if (flagsService.postFlags(this, messageId, flaggerEmail, this)) {
                Button flagButton = findViewById(R.id.flag_message);
                flagButton.setText("Flagged");
                isFlagged = true; }
        }

    }

    private void setEditBtnVisibility(boolean visible) {
        if (visible) editBtn.setVisibility(View.VISIBLE);
        else editBtn.setVisibility(View.INVISIBLE);
    }

    private void getAttachmentsForMessage() {
        attachments = new ArrayList<>();

        attachmentsText.setText(String.format("Attachments (%d)", attachments.size()));
    }

    private void getCommentsForMessage() {
        CommentsService.getInstance().getComments(this, this, messageId);
    }

    @Override
    public void onSuccess(String response) {
        ArrayList<Comments> comments = new ArrayList<>();
        try {
            JSONObject mData = new JSONObject(response);
            JSONArray json = mData.getJSONArray("mData");
            for (int i = 0; i < json.length(); ++i) {
                Comments comment = Comments.parseJson(json.getJSONObject(i));
                comments.add(comment);
            }
            commentsAdapter.refresh(comments);
            commentsText.setText(String.format("Comments (%d)", comments.size()));
        } catch (final JSONException e) {
            Log.d("tjb220", "Error parsing JSON file: " + e.getMessage());
            return;
        }
    }

    public final int COMPOSE_COMMENT_REQUEST_CODE = 1;
    public static final String MESSAGE_DETAIL_TO_COMPOSE_COMMENT_MESSAGE_ID = "message_detail_to_compose_comment_message_id";

    public void composeComment(View view) {
        Intent intent = new Intent(this, ComposeCommentActivity.class);
        intent.putExtra(MessageDetailActivity.MESSAGE_DETAIL_TO_COMPOSE_COMMENT_MESSAGE_ID, messageId);
        startActivityForResult(intent, COMPOSE_COMMENT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == android.app.Activity.RESULT_OK) {
            if (requestCode == COMPOSE_COMMENT_REQUEST_CODE) {
                getCommentsForMessage();
            }
        }
    }

    @Override
    public void showActivity(Intent intent) {
        startActivity(intent);
    }

    @Override
    public Context getActivityIntent() {
        return this;
    }
}
