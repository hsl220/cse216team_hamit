package hamit.cse216.lehigh.edu.thebuzz;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Comments {

    private String email;
    private int mId;
    private String body;

    public Comments(String email, int mId, String body){
        this.email = email;
        this.mId = mId;
        this.body = body;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public static Comments parseJson(JSONObject jsonObject) {
        Comments comment = null;
        String email;
        int id;
        String body;

        try {

            email = jsonObject.getString("mAuthorEmail");
            body = jsonObject.getString("mBody");
            id = Integer.parseInt(jsonObject.getString("mId"));

            comment = new Comments(email, id, body);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return comment;
    }
}
