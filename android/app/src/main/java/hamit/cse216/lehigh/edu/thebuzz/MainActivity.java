package hamit.cse216.lehigh.edu.thebuzz;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.drive.Drive;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
/**
 * Travis Barnes
 * Last Edited 9/24/18
 * Main activity page of "The Buzz" application
 * Main page shows messages from database using a Recycle view and users can
 * post messages by clicking the create message button leading into the
 * Second activity.
 */
public class MainActivity extends AppCompatActivity implements VolleyCallback, ShowsActivity {
    /**
     * ArrayList of messages used to hold messages from database
     * and placed into the Recycle view
     */
    private ItemListAdapter adapter;
    private RecyclerView recyclerView;
    private Button createMessageBtn;

    public static final String MAIN_ACTIVITY_TO_MESSAGE_DETAIL_TITLE = "main_activity_to_message_detail_title";
    public static final String MAIN_ACTIVITY_TO_MESSAGE_DETAIL_AUTHOR = "main_activity_to_message_detail_author";
    public static final String MAIN_ACTIVITY_TO_MESSAGE_DETAIL_DATE = "main_activity_to_message_detail_date";
    public static final String MAIN_ACTIVITY_TO_MESSAGE_DETAIL_BODY = "main_activity_to_message_detail_body";
    public static final String MAIN_ACTIVITY_TO_MESSAGE_DETAIL_ID = "main_activity_to_message_detail_id";

    public final int CREATE_MESSAGE_REQUEST_CODE = 555;

    private GoogleSignInClient mGoogleSignInClient;

    /**
     * When app is initially loaded the page performs a GET
     * request which pulls all messages from the data base
     * and places into the Recycle view using
     * populateListFromVolley method.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.message_list_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ItemListAdapter(new ArrayList<Messages>(), this);
        recyclerView.setAdapter(adapter);

        createMessageBtn = findViewById(R.id.create);

        // add scroll listener to recycler view to hide the create message btn on scroll
        // allows the user to see the last message in the list without any obstruction
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    createMessageBtn.setVisibility(View.INVISIBLE);
                    return;
                }
                if (dy < 0) createMessageBtn.setVisibility(View.VISIBLE);
            }
        });

        checkIfUserLoggedIn();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    private void checkIfUserLoggedIn() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .requestProfile()
                .requestScopes(Drive.SCOPE_FILE, Drive.SCOPE_APPFOLDER)
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        if (account == null) {
            // user is not logged in
            // show login in screen
            Intent intent = new Intent(this, LoginActivity.class);
            startActivityForResult(intent, SIGN_IN_REQUEST_CODE);
            return;
        }
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
        String flaggerEmail = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_user_email), "");

        // user is logged in
        MessageService.getInstance().getMessages(this, this, flaggerEmail);
    }

    private void signOut() {
        mGoogleSignInClient.signOut();

        // remove the user credentials from the sharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.com_hamit_thebuzz_user_email), null);
        editor.commit();

        Toast.makeText(this, "Signed out", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, SIGN_IN_REQUEST_CODE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_signout:
                signOut();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Uses the response to acquire the messages from the database and turns them into a JSONArray
     * Moves through array to create Message objects with given information
     * Adding them into the message Array list. Afterwards, three additional messages are
     * created and added to make list of messages larger. The Recycle view is then
     * formatted with the custom ItemList Adapter using array list of messages
     * @param response
     *      Takes in the response from the GET request
     */
    @Override
    public void onSuccess(String response){
        ArrayList<Messages> messages = new ArrayList<>();
        try {
            JSONObject mData = new JSONObject(response);
            JSONArray json = mData.getJSONArray("mData");
            for (int i = 0; i < json.length(); ++i) {

//                ArrayList<AttachedImage> attachedImages = null;
//                JSONArray images = json.getJSONObject(i).getJSONArray("images");
//                for (int j = 0; j < images.length(); j++) {
//                    if (attachedImages == null) attachedImages = new ArrayList<>();
//
//                    String base64StringForImage = images.getString(j);
//                    AttachedImage attachedImage = new AttachedImage(base64StringForImage);
//                    attachedImages.add(attachedImage);
//                }

                Messages message = Messages.parseMessage(json.getJSONObject(i));
//                message.setAttachedImages(attachedImages);
                messages.add(message);
            }
            adapter.refresh(messages);
        } catch (final JSONException e) {
            Log.d("tjb220", "Error parsing JSON file: " + e.getMessage());
            return;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == MainActivity.RESULT_OK) {
            if (requestCode == CREATE_MESSAGE_REQUEST_CODE || requestCode == SIGN_IN_REQUEST_CODE) {
                autoRefresh();
            }
        }
    }

    public void clickCreate(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivityForResult(intent, CREATE_MESSAGE_REQUEST_CODE);
    }

    /**
     * Takes in Tag information from specific upvote button pressed.
     * The tag contains the message Id which is added into the url to
     * create the specific request to add an upvote to the specified message.
     * Will not work for messages outside database(test messages)
     * @param view
     *      view item used is the upvote button in messages
     */
    public void clickUpvote(View view){
        int mId = (int) view.getTag();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject json = new JSONObject();
        try {
            json.put("mId", mId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String putURL = "https://glacial-fjord-88850.herokuapp.com/messages/" + mId + "/upvoteCount";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, putURL, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("tjb220","String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("tjb220","Error getting response");
            }
        });
        requestQueue.add(jsonObjectRequest);

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
        String flaggerEmail = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_user_email), "");

        MessageService.getInstance().getMessages(this, this, flaggerEmail);
    }

    /**
     * Takes in Tag information from specific Downvote button pressed.
     * The tag contains the message Id which is added into the url to
     * create the specific request to add an Downvote to the specified message.
     * @param view
     *      view item used is the Downvote button in messages
     */
    public void clickDownvote(View view){
        int mId = (int) view.getTag();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject json = new JSONObject();
        try {
            json.put("mId", mId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String putURL = getString(R.string.api_url) + "messages/" + mId + "/downvoteCount";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, putURL, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("tjb220","String Response : "+ response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("tjb220","Error getting response");
            }
        });
        requestQueue.add(jsonObjectRequest);

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
        String flaggerEmail = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_user_email), "");

        MessageService.getInstance().getMessages(this, this, flaggerEmail);
    }

    public void autoRefresh() {
        final Handler handler = new Handler();
        TimerTask doTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.com_hamit_thebuzz_preference_key), Context.MODE_PRIVATE);
                            String flaggerEmail = sharedPreferences.getString(getString(R.string.com_hamit_thebuzz_user_email), "");
                            MessageService.getInstance().getMessages(MainActivity.this, MainActivity.this, flaggerEmail);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        new Timer().scheduleAtFixedRate(doTask, 0, 10000);
    }

    @Override
    public void showActivity(Intent intent) {
        startActivity(intent);
    }

    @Override
    public Context getActivityIntent() {
        return this;
    }

    private final int SIGN_IN_REQUEST_CODE = 333;
}
