package hamit.cse216.lehigh.edu.thebuzz;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.webkit.MimeTypeMap;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class AttachedFile {
    private Bitmap bitmap = null;
    private File file = null;
    private String url = null;
    private String urlPlaceholder = null;
    private String type;
    private Uri fileUri = null;

    public AttachedFile(Bitmap bitmap) {
        this.bitmap = bitmap;
        type = "images";
    }

    public AttachedFile(File file, Uri fileUri) {
        this.file = file;
        type = MimeTypeMap.getFileExtensionFromUrl(file.getPath());
        this.fileUri = fileUri;
    }

    public AttachedFile(String url, String urlPlaceholder) {
        this.url = url;
        this.urlPlaceholder = urlPlaceholder;
        type = "link";
    }

    public Uri getFileUri() {
        return fileUri;
    }

    public String getType() {
        return type;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getFilename() {
        return file.getName();
    }

    public File getFile() {
        return file;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlPlaceholder() {
        return urlPlaceholder;
    }

    public String bitmapAsBase64String() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private Bitmap base64StringToBitmap(String input) {
        byte[] bytes = Base64.decode(input, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}