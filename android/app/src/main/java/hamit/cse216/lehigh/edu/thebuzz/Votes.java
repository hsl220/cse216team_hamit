package hamit.cse216.lehigh.edu.thebuzz;

public class Votes {
    private String email;
    private int mId;
    private int type;

    public Votes(String email, int mId, int type){
        this.email = email;
        this.mId = mId;
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
