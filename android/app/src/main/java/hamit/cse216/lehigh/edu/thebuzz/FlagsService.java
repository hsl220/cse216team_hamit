package hamit.cse216.lehigh.edu.thebuzz;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class FlagsService {


        public static FlagsService _instance;

        public static FlagsService getInstance() {
            if (_instance == null) {
                _instance = new FlagsService();
            }
            return _instance;
        }

        public FlagsService() {

        }
        public boolean postFlags(Context context, int messageId, String flaggerEmail, final VolleyCallback callback) {
            RequestQueue queue = Volley.newRequestQueue(context);
            String url = "https://glacial-fjord-88850.herokuapp.com" + "flags";

            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put("messageId", messageId);
                jsonData.put("flaggerEmail", flaggerEmail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String jsonBody = jsonData.toString();

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                callback.onSuccess(response);
            }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("postBlocks", "Failed to post block");
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() {
                    try {
                        return jsonBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            return true;
        }

        }

