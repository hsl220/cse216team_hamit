package hamit.cse216.lehigh.edu.thebuzz;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.ViewHolder> {

    private ArrayList<Comments> comments;
    private ShowsActivity activity;

    public CommentListAdapter(ArrayList<Comments> comments, ShowsActivity activity) {
        this.comments = comments;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layout = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_item, viewGroup, false);
        return new ViewHolder(layout, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.configureViewHolder(comments.get(i));
    }

    public void refresh(ArrayList<Comments> newComments) {
        comments = newComments;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private View layout;
        private TextView authorEmailText;
        private TextView bodyText;
        private View bottomBorderView;

        private ShowsActivity activity;

        public ViewHolder(@NonNull View itemView, ShowsActivity activity) {
            super(itemView);

            layout = itemView;
            authorEmailText = layout.findViewById(R.id.textView7);
            bodyText = layout.findViewById(R.id.textView9);
            bottomBorderView = layout.findViewById(R.id.view);

            bottomBorderView.setBackgroundColor(Color.LTGRAY);

            this.activity = activity;
        }

        public void configureViewHolder(Comments comment) {
            authorEmailText.setText(comment.getEmail());
            bodyText.setText(comment.getBody());

            authorEmailText.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity.getActivityIntent(), ProfileActivity.class);
            intent.putExtra(ProfileActivity.PROFILE_FOR_USER_EMAIL, authorEmailText.getText().toString());
            activity.showActivity(intent);
        }
    }
}
