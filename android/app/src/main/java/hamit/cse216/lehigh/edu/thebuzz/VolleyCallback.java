package hamit.cse216.lehigh.edu.thebuzz;

public interface VolleyCallback {
    void onSuccess(String response);
}
