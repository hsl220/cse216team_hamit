package hamit.cse216.lehigh.edu.thebuzz;

import java.io.File;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
//import com.google.api.services.drive.model.File;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.drive.CreateFileActivityOptions;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.OpenFileActivityOptions;
import com.google.android.gms.drive.TransferPreferences;
import com.google.android.gms.tasks.Task;
import com.google.api.client.http.FileContent;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

/**
 * Travis Barnes
 * Last Edited 9/24/18
 * This activity allows users to create their own messages
 * which will then use a POST request to add said message to
 * the database. Once completed activity will disappear to
 * show main activity with updated message list
 */
public class SecondActivity extends AppCompatActivity implements VolleyCallback {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AttachedFileAdapter adapter;

    private ArrayList<AttachedFile> attachedFiles;

    private String mCurrentPhotoPath;

    /**
     * Currently nothing special happens when
     * this activity loads
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        attachedFiles = new ArrayList<>();
        adapter = new AttachedFileAdapter(attachedFiles, this);

        recyclerView.setAdapter(adapter);
    }

    /**
     * When clicking on the Okay button it will check
     * if user had filled in both the Title text view
     * and the Body text view. If either are incomplete
     * a toast message will appear stating which needs
     * to be completed. Once both are complete the newly
     * created message will be transformed into a JSON
     * Object and POSTed to the database.
     * @param view
     *      view item used is "Okay" button
     */
    public void okayClick(View view){
        TextView titleText = findViewById(R.id.messageTitle);
        TextView bodyText = findViewById(R.id.messageBody);
        if(titleText.getText().length() <= 0){
            Toast.makeText(this, "Incomplete Title Section",Toast.LENGTH_LONG).show();
        }
        else if(bodyText.getText().length() <= 0){
            Toast.makeText(this, "Incomplete Body Section",Toast.LENGTH_LONG).show();
        }
        else{
            String title = titleText.getText().toString().trim();
            String body = bodyText.getText().toString().trim();
            String email = "adg219@lehigh.edu";

            // adding attached images to json
            ArrayList<String> base64ImageStrings = new ArrayList<>();
            for (AttachedFile images : attachedFiles) {
                base64ImageStrings.add(images.bitmapAsBase64String()); // converts bitmap to base64 string
            }

            MessageService.getInstance().postMessage(this, title, body, email, attachedFiles, this);
        }
    }

    @Override
    public void onSuccess(String response) {
        try {
            JSONObject json = new JSONObject(response);

            String status = json.getString("mStatus");

            if (status.equals("ok")) {

            } else {
                Log.e("postMessageRequest", json.getString("mMessage"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        setResult(Activity.RESULT_OK, getIntent());
        finish();
    }

    /**
     * If activated app will switch back over to
     * main activity page
     * @param view
     *      view item is "Cancel" button
     */
    public void cancelClick(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivityForResult(intent,55);
    }

    private final int GALLERY_REQUEST_CODE = 1;
    private final int READ_EXTERNAL_STORAGE_REQUEST_CODE = 2;
    private final int TAKE_PHOTO_REQUEST_CODE = 3;

    public void attachImages(View view) {
        if (ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, GALLERY_REQUEST_CODE);
        } else {
            String[] permissions = new String[1];
            permissions[0] = Manifest.permission.READ_EXTERNAL_STORAGE;
            ActivityCompat.requestPermissions(this, permissions, READ_EXTERNAL_STORAGE_REQUEST_CODE);
        }
    }

    public void uploadFileClicked(View view) {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        intent.setType("*/*");

        startActivityForResult(intent, FILE_REQUEST_CODE);
    }

    private final int FILE_REQUEST_CODE = 444;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == READ_EXTERNAL_STORAGE_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // pick image after request permission success
                attachImages(null);
            }
        }
    }

    public void takePhotoBtnClicked(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, TAKE_PHOTO_REQUEST_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void addPhotoToGallery() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private Bitmap getTakenPhotoFromGallery() {
        // Get the dimensions of the View
        int targetW = 100;
        int targetH = 150;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
    }

    private Bitmap getBitmapFromData(Uri selectedImage) {
        String[] filePathColumn = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        return BitmapFactory.decodeFile(picturePath);
    }

    private final int CREATE_LINK_REQUEST_CODE = 33;

    public void createLinkClicked(View view) {
        Intent intent = new Intent(this, CreateLinkActivity.class);
        startActivityForResult(intent, CREATE_LINK_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch(requestCode) {
                case GALLERY_REQUEST_CODE:
                {
                    Bitmap bitmap = getBitmapFromData(data.getData());
                    AttachedFile attachedImage = new AttachedFile(bitmap);
                    adapter.appendItem(attachedImage);
                    break;
                }

                case TAKE_PHOTO_REQUEST_CODE:
                {
                    addPhotoToGallery();

                    Bitmap imageBitmap = getTakenPhotoFromGallery();

                    AttachedFile attachedImage = new AttachedFile(imageBitmap);
                    adapter.appendItem(attachedImage);
                    break;
                }

                case CREATE_LINK_REQUEST_CODE:
                {
                    String link = data.getStringExtra(CreateLinkActivity.CREATE_LINK_ACTIVITY_LINK);
                    String placeholder = data.getStringExtra(CreateLinkActivity.CREATE_LINK_ACTIVITY_PLACEHOLDER);
                    AttachedFile linkFile = new AttachedFile(link, placeholder);
                    adapter.appendItem(linkFile);
                    break;
                }

                case FILE_REQUEST_CODE:
                {

                    Uri uri = data.getData();
                    File file = new File(uri.getPath());
                    AttachedFile attachedFile = new AttachedFile(file, uri);
                    adapter.appendItem(attachedFile);
                    break;
                }

                default:
                    break;
            }
        }
    }
}
