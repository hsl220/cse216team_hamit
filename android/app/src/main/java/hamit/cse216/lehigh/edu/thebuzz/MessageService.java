package hamit.cse216.lehigh.edu.thebuzz;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class MessageService {

    private static MessageService _instance;

    public static MessageService getInstance() {
        if (_instance == null) {
            _instance = new MessageService();
        }
        return _instance;
    }

    private MessageService() {}

//    public void getMessagesForUser(Context context, String email, VolleyCallback callback) {
//        //The following is the basic format of performing a GET request
//        // Instantiate the RequestQueue.
//        RequestQueue queue = Volley.newRequestQueue(context);
//        String url = context.getString(R.string.api_url) + "messages/" + email;
//        // Request a string response from the provided URL.
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d("tjb220", "Response: " + response);
//                        callback.onSuccess(response);
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("tjb220", "Get function didn't work!");
//                Toast.makeText(context, "Could not retrieve messages from database", Toast.LENGTH_SHORT).show();
//            }
//        });
//        // Add the request to the RequestQueue.
//        queue.add(stringRequest);
//    }

    public void getMessages(final Context context, final VolleyCallback callback, String email) {
        //The following is the basic format of performing a GET request
        // Instantiate the RequestQueue.


        RequestQueue queue = Volley.newRequestQueue(context);
        String url = context.getString(R.string.api_url) + "messages-with-blocker-email/" + email;
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("tjb220", "Response: " + response);
                        callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("tjb220", "Get function didn't work!");
                Toast.makeText(context, "Could not retrieve messages from database", Toast.LENGTH_SHORT).show();
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void postMessage(Context context, String title, String body, String email, ArrayList<AttachedFile> files, final VolleyCallback callback) {
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put("title", title);
            jsonData.put("body", body);
            jsonData.put("email", email);
            jsonData.put("files", files);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        final String jsonBody = jsonData.toString();

        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(Request.Method.POST, context.getString(R.string.api_url) + "messages", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("postMessageRequest", error.getLocalizedMessage());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return jsonBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
        };
        queue.add(request);
    }
}
