package edu.lehigh.cse216.cse216team_hamit.backend.votes;

public class VoteRequest
{
    public String email;
    public int messageId;
    public int type;
}