package edu.lehigh.cse216.cse216team_hamit.backend.users;

import edu.lehigh.cse216.cse216team_hamit.backend.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDatabase
{

    private static UserDatabase mInstance;

    public static UserDatabase getInstance()
    {
        if (mInstance == null) {
            mInstance = new UserDatabase();
        }
        return mInstance;
    }

    private PreparedStatement mSelectAll;
    private PreparedStatement mSelectOne;
    private PreparedStatement mGetSaltFromUser;
    private PreparedStatement mResetPassword;
    private PreparedStatement mSelectSessionKey;
    private PreparedStatement mUpdateSessionKey;

    private UserDatabase()
    {
        try
        {
            mSelectAll = Constants.CONNECTION.prepareStatement(Constants.SELECT_ALL_USERS);
            mSelectOne = Constants.CONNECTION.prepareStatement(Constants.SELECT_USER_WITH_EMAIL);
            mGetSaltFromUser = Constants.CONNECTION.prepareStatement(Constants.SELECT_SALT_FROM_USER);
            mResetPassword = Constants.CONNECTION.prepareStatement(Constants.RESET_PASSWORD);
            mSelectSessionKey = Constants.CONNECTION.prepareStatement(Constants.SELECT_SESSION_KEY);
            mUpdateSessionKey = Constants.CONNECTION.prepareStatement(Constants.UPDATE_SESSION_KEY);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the salt stored for the user with the corresponding email address
     * @param email
     * @return null if the user is not found, otherwise returns the salt value
     
     public String getSaltForUserWithEmail(String email)
    {
        String salt = null;

        try
        {
            mGetSaltFromUser.setString(1, email);
            ResultSet result = mGetSaltFromUser.executeQuery();

            if (result.next())
            {
                salt = result.getString("salt");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return salt;
    } */

    /**
     * Query the UserDatabase for a list of all subjects and their IDs
     *
     * @return All rows, as an ArrayList
     */
    ArrayList<UserRow> selectAll()
    {
        ArrayList<UserRow> res = new ArrayList<UserRow>();
        try {
            ResultSet rs = mSelectAll.executeQuery();
            while (rs.next()) {
                res.add(new UserRow(rs.getString("name"), rs.getString("email")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get all data for a specific row, by ID
     *
     * @param id The id of the row being requested
     *
     * @return The data for the requested row, or null if the ID was invalid
     */
    public UserRow selectOne(String email)
    {
        UserRow res = null;
        try {
            mSelectOne.setString(1, email);
            ResultSet rs = mSelectOne.executeQuery();
            if (rs.next()) {
                res = new UserRow(rs.getString("name"), rs.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Updates the user with the corresponding email. Password is updated along with the salt and the new hash (signature)
     * @param email
     * @param password
     * @param salt
     * @param hash
     * @return true if the reset was successful, else false
     */
    public boolean resetPassword(String email, String password, String salt, String hash)
    {
        boolean success = false;
        try
        {
            mResetPassword.setString(1, password);
            mResetPassword.setString(2, salt);
            mResetPassword.setString(3, hash);
            mResetPassword.setString(4, email);

            mResetPassword.execute();
            success = true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return success;
    }

    /**
     * Attempts to get the session key stored for a user
     * @param email
     * @return the session key for the user. if no session key is found, then null
     */
    public String selectSessionKey(String email)
    {
        String sessionKey = null;
        try
        {
            mSelectSessionKey.setString(1, email);
            ResultSet result = mSelectSessionKey.executeQuery();

            if (result.next())
            {
                sessionKey = result.getString("sessionKey");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return sessionKey;
    }

    public boolean updateSessionKeyForUserWithEmail(String email, String newSessionKey)
    {
        boolean success = false;

        try
        {
            mUpdateSessionKey.setString(1, newSessionKey);
            mUpdateSessionKey.setString(2, email);
            mUpdateSessionKey.execute();
            success = true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return success;
    }

}
