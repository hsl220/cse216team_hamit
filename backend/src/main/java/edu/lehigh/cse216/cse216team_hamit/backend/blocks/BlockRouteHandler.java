package edu.lehigh.cse216.cse216team_hamit.backend.blocks;

import com.google.gson.Gson;
import spark.Route;
import java.util.ArrayList;
import edu.lehigh.cse216.cse216team_hamit.backend.*;

public class BlockRouteHandler
{
    private static final Gson gson = new Gson();

    public static Route createBlock = (request, response) -> {
        BlockRequest blockRequest = gson.fromJson(request.body(), BlockRequest.class);

        String blockerEmail = blockRequest.blocker;
        String blockeeEmail = blockRequest.blockee;

        boolean success = BlockDatabase.getInstance().insertBlockedUser(blockerEmail,blockeeEmail);

        if (success)
        {
            return new StructuredResponse("ok", null, null);
        }

        return new StructuredResponse("error", "could not create block", null);
    };
}
