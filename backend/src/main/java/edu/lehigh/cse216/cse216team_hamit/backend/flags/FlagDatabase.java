package edu.lehigh.cse216.cse216team_hamit.backend.flags;
import edu.lehigh.cse216.cse216team_hamit.backend.*;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class FlagDatabase
{
    private static FlagDatabase mInstance = null;

    public static FlagDatabase getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new FlagDatabase();
        }
        return mInstance;
    }


   //Make mInsertFlag statement
   private PreparedStatement mInsertFlag;
   private PreparedStatement mSelectFlag;

    private FlagDatabase() {
        try
        {
            mInsertFlag = Constants.CONNECTION.prepareStatement(Constants.INSERT_FLAG);
            mSelectFlag = Constants.CONNECTION.prepareStatement(Constants.SELECT_FLAG);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public FlagRow getFlag(int id, String email)
    {
        FlagRow flag = null;
        try
        {
            mSelectFlag.setInt(1, id);
            mSelectFlag.setString(2, email);
            ResultSet result = mSelectFlag.executeQuery();
            if (result.next())
            {
                flag = new FlagRow(result.getInt("id"),result.getString("email"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return flag;
    }


    public boolean insertFlaggedPost(int id, String email)
    {
        boolean success = false;

        try
        {
            mInsertFlag.setInt(1, id);
            mInsertFlag.setString(2, email);
            mInsertFlag.execute();
            success = true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return success;
    }

}