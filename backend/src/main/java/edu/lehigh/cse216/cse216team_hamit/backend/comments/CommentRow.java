package edu.lehigh.cse216.cse216team_hamit.backend.comments;

import java.sql.Date;
import java.util.ArrayList;

public class CommentRow
{
    int mId;
    String mAuthorEmail;
    String mBody;
    Date mCreatedDate;
    ArrayList<String> urls;

    /** Message id */
    public int getId() {
        return this.mId;
    }

    public String getAuthorEmail() {
        return this.mAuthorEmail;
    }


    public String getBody() {
        return this.mBody;
    }

    public Date getCreatedDate() {
        return this.mCreatedDate;
    }

    public ArrayList<String> getUrls(){
        return this.urls;
    }

    public CommentRow(int id, String authorEmail, String body, Date createdDate, ArrayList<String> urls)
    {
        mId = id;
        mAuthorEmail = authorEmail;
        mBody = body;
        mCreatedDate = createdDate;
        this.urls = urls;
    }
}