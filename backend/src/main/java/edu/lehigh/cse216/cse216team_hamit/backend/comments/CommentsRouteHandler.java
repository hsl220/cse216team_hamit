package edu.lehigh.cse216.cse216team_hamit.backend.comments;

import com.google.gson.Gson;
import spark.Route;
import java.util.ArrayList;
import edu.lehigh.cse216.cse216team_hamit.backend.*;

public class CommentsRouteHandler
{
    private static final Gson gson = new Gson();

    public static Route getCommentsForMessageWithId = (request, response) -> {

        int id = Integer.parseInt(request.params(":id"));

        ArrayList<CommentRow> comments = CommentsDatabase.getInstance().getCommentsWithMessageId(id);

        if (comments == null)
        {
            return new StructuredResponse("ok", "no comments for messages", null);
        }

        return new StructuredResponse("ok", null, comments);
    };

    public static Route getCommentsWithEmail = (request, response) -> {

        String email = request.params(":email");

        ArrayList<CommentRow> comments = CommentsDatabase.getInstance().getCommentsWithEmail(email);

        if (comments == null)
        {
            return new StructuredResponse("ok", "no comments from this user", null);
        }

        return new StructuredResponse("ok", null, comments);
    };

    public static Route insertComment = (request, response) -> {
        CommentRequest commentRequest = gson.fromJson(request.body(), CommentRequest.class);

        String authorEmail = commentRequest.email;
        int messageId = Integer.parseInt(request.params(":id"));
        String body = commentRequest.body;
        ArrayList<String> files = commentRequest.fileLinks;

        boolean success = CommentsDatabase.getInstance().insertCommentForMessage(authorEmail, messageId, body, files);

        if (success)
        {
            return new StructuredResponse("ok", null, null);
        }

        return new StructuredResponse("error", "could not insert comment", null);
    };
}