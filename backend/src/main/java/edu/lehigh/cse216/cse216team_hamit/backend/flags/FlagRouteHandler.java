package edu.lehigh.cse216.cse216team_hamit.backend.flags;

import edu.lehigh.cse216.cse216team_hamit.backend.flags.FlagRequest;
import com.google.gson.Gson;
import spark.Route;
import edu.lehigh.cse216.cse216team_hamit.backend.*;

public class FlagRouteHandler
{
    private static final Gson gson = new Gson();

    public static Route findFlag = (request, response) -> {
        int messageId = Integer.parseInt(request.params(":id"));
        String flagger = request.params(":userEmail");

        FlagRow flag = FlagDatabase.getInstance().getFlag(messageId, flagger);

        if (flag != null)
        {
            return new StructuredResponse("ok", null, flag);
        }

        return new StructuredResponse("error", "could not find flag", null);
    };

    public static Route createFlag = (request, response) -> {
        FlagRequest FlagRequest = gson.fromJson(request.body(), FlagRequest.class);

        int messageId = FlagRequest.Id;
        String flagger = FlagRequest.flaggerEmail;

        boolean success = FlagDatabase.getInstance().insertFlaggedPost(messageId, flagger);

        if (success)
        {
            return new StructuredResponse("ok", null, null);
        }

        return new StructuredResponse("error", "could not flag post", null);
    };
}