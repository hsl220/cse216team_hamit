package edu.lehigh.cse216.cse216team_hamit.backend.auth;

import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.spec.PBEKeySpec;

import edu.lehigh.cse216.cse216team_hamit.backend.users.UserDatabase;

import javax.crypto.SecretKeyFactory;
import java.math.BigInteger;

public class AuthHelper
{

    /**
     * Takes a password and generates a secure hashed signature
     * @param password
     * @return the secure hashed password (signature)
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static String generateHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        int iterations = 1000;
        char[] chars = password.toCharArray();
        byte[] salt = generateSalt();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return toHex(hash);
    }

    /**
     * Generates the random part added to the end of the password to enhance security
     * @return a random string of characters
     * @throws NoSuchAlgorithmException
     */
    public static byte[] generateSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    /**
     * Converts an array of bytes into their hexidecimal representations
     * @param bytes
     * @return a string of characters
     * @throws NoSuchAlgorithmException
     */
    public static String toHex(byte[] bytes) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, bytes);
        String hex = bi.toString(16);
        int paddingLength = (bytes.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0"  +paddingLength + "d", 0) + hex;
        }else{
            return hex;
        }
    }

}