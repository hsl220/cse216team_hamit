package edu.lehigh.cse216.cse216team_hamit.backend.auth;

public class AuthRequest
{
    public String email;
    public String hash;
    public String password;
    public String salt;
}