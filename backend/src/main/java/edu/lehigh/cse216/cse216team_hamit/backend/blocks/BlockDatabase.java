package edu.lehigh.cse216.cse216team_hamit.backend.blocks;

import edu.lehigh.cse216.cse216team_hamit.backend.*;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class BlockDatabase
{
    private static BlockDatabase mInstance = null;

    public static BlockDatabase getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new BlockDatabase();
        }
        return mInstance;
    }

    private PreparedStatement mInsertBlock;

    private BlockDatabase() {
        try
        {
            mInsertBlock = Constants.CONNECTION.prepareStatement(Constants.INSERT_BLOCK);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }



    public boolean insertBlockedUser(String blocker, String blockee)
    {
        boolean success = false;

        try
        {
            mInsertBlock.setString(1, blocker);
            mInsertBlock.setString(2, blockee);

            mInsertBlock.execute();
            success = true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return success;
    }

}
