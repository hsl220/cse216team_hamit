package edu.lehigh.cse216.cse216team_hamit.backend.votes;

import com.google.gson.Gson;

import edu.lehigh.cse216.cse216team_hamit.backend.StructuredResponse;
import edu.lehigh.cse216.cse216team_hamit.backend.messages.MessageDatabase;
import spark.Route;

public class VoteRouteHandlers
{
    private static final Gson gson = new Gson();

    public static Route getVoteType = (request, response) -> {
        StructuredResponse res = null;
        VoteRequest voteRequest = gson.fromJson(request.body(), VoteRequest.class);
        response.status(200);
        response.type("application/json");

        String email = voteRequest.email;
        int messageId = voteRequest.messageId;

        int type = VotesDatabase.getInstance().selectVoteTypeForVoteWithEmailAndMessageId(email, messageId);

        switch (type)
        {
            case 0:
                res = new StructuredResponse("ok", "no vote found", null);
                break;
            case 1:
                res = new StructuredResponse("ok", null, type);
                break;
            case -1:
                res = new StructuredResponse("ok", null, type);
                break;
            default:
                res = new StructuredResponse("error", "something went wrong when trying to get vote type", null);
                break;
        }

        return res;
    };

    public static Route deleteVote = (request, response) -> {
        StructuredResponse res = null;
        VoteRequest voteRequest = gson.fromJson(request.body(), VoteRequest.class);
        response.status(200);
        response.type("application/json");

        String email = voteRequest.email;
        int messageId = voteRequest.messageId;

        int voteType = VotesDatabase.getInstance().deleteVoteWithEmailAndMessageId(email, messageId);

        switch (voteType) {
            case 0:
                // something went wrong
                return new StructuredResponse("error", "could not delete vote", null);
            case 1:
                // upvote
                MessageDatabase.getInstance().downvote(messageId);
                return new StructuredResponse("ok", null, null);
            case -1:
                // downvote
                MessageDatabase.getInstance().upvote(messageId);
                return new StructuredResponse("ok", null, null);
            default:
                break;
        }

        return res;
    };

    public static Route updateVoteType = (request, response) -> {
        VoteRequest voteRequest = gson.fromJson(request.body(), VoteRequest.class);
        response.status(200);
        response.type("application/json");

        String email = voteRequest.email;
        int messageId = voteRequest.messageId;
        int type = voteRequest.type;

        boolean success = VotesDatabase.getInstance().updateVoteType(email, messageId, type);

        if (success)
        {
            switch(type)
            {
                case 1:
                    // upvote
                    MessageDatabase.getInstance().upvote(messageId);

                    // TODO: decrement downvotes for message
                    break;
                case -1:
                    // downvote
                    MessageDatabase.getInstance().downvote(messageId);

                    // TODO: decrement upvotes for message
                    break;
                default:
                    break;
            }

            return new StructuredResponse("ok", null, null);
        }
        else
        {
            return new StructuredResponse("error", "could not change vote type", null);
        }
    };

    public static Route insertVote = (request, response) -> {
        VoteRequest voteRequest = gson.fromJson(request.body(), VoteRequest.class);
        response.status(200);
        response.type("application/json");

        String email = voteRequest.email;
        int messageId = voteRequest.messageId;
        int type = voteRequest.type;

        boolean success = VotesDatabase.getInstance().insertVote(email, messageId, type);

        if (success)
        {
            switch (type) {
                case 1:
                    MessageDatabase.getInstance().upvote(messageId);
                    break;
                case -1:
                    MessageDatabase.getInstance().downvote(messageId);
                    break;
                default:
                    break;
            }
            return new StructuredResponse("ok", null, null);
        }
        else
        {
            return new StructuredResponse("error", "vote was unable to be added", null);
        }
    };
}