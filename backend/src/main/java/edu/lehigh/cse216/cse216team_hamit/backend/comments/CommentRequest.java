package edu.lehigh.cse216.cse216team_hamit.backend.comments;

import java.util.ArrayList;

public class CommentRequest
{
    public String email;
    public String body;
    public ArrayList<String> fileLinks;
}