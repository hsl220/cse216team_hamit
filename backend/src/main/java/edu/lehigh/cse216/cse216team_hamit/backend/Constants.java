package edu.lehigh.cse216.cse216team_hamit.backend;

import java.sql.Connection;
import java.util.Map;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Constants
{
    private static Connection getConnection()
    {
        Connection conn = null;

        try {
            Map<String, String> env = System.getenv();
            // URI uri = new URI(url);
            URI uri = new URI(env.get("DATABASE_URL"));

            String username = uri.getUserInfo().split(":")[0];
            String password = uri.getUserInfo().split(":")[1];
            String dbUrl = "jdbc:postgresql://" + uri.getHost() + ':' + uri.getPort() + uri.getPath()
                    + "?sslmode=require";

            conn = DriverManager.getConnection(dbUrl, username, password);

        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return conn;
    }

    // SQL Connection
    public static final Connection CONNECTION = getConnection();

    // Message Table Query Strings
    public static final String SELECT_ALL_MESSAGES = "select * from messages where authorEmail not in (select blockee from blocks where blocker = ?) order by createdDate desc";
    public static final String SELECT_MESSAGE_WITH_ID = "select * from messages where id = ?";
    public static final String DELETE_MESSAGE_WITH_ID = "delete from messages where id = ?";
    public static final String INSERT_MESSAGE = "insert into messages (authorEmail, title, body) values (?, ?, ?)";
    public static final String UPVOTE_MESSAGE = "update messages set upvoteCount = upvoteCount + 1 where id = ?";
    public static final String DOWNVOTE_MESSAGE = "update messages set downvoteCount = downvoteCount + 1 where id = ?";
    public static final String SELECT_MESSAGE_WITH_EMAIL = "select * from messages where authorEmail = ?";
    public static final String SELECT_LINKS_FOR_MESSAGE_WITH_ID = "select * from links where messageId = ? and where type = Message";

    // User Table Query Strings
    public static final String SELECT_ALL_USERS = "select * from users";
    public static final String SELECT_USER_WITH_EMAIL = "select * from users where email = ?";
    public static final String SELECT_SALT_FROM_USER = "select salt from users where email = ?";
    public static final String RESET_PASSWORD = "update users set password = ? and salt = ? and signature = ? where email = ?";
    public static final String SELECT_SESSION_KEY = "select sessionKey from users where email = ?";
    public static final String UPDATE_SESSION_KEY = "update users set sessionKey = ? where email = ?";

    // Comments Table Query Strings
    public static final String SELECT_COMMENTS_WITH_MESSAGE_ID = "select * from comments where id = ?";
    public static final String INSERT_COMMENT = "insert into comments (id, body, authorEmail) values (?,?,?)";
    public static final String SELECT_COMMENTS_WITH_EMAIL = "select * from comments where authoremail = ?";

    // Votes Table Query Strings
    public static final String SELECT_VOTE_TYPE_FOR_MESSAGE_WITH_EMAIL_AND_MESSAGE_ID = "select type from votes where email = ? and id = ?";
    public static final String DELETE_VOTE_EMAIL_AND_MESSAGE_ID = "delete from votes where email = ? and id = ?";
    public static final String UPDATE_VOTE_TYPE_WITH_EMAIL_AND_MESSAGE_ID = "update votes set type = ? where email = ? and id = ?";
    public static final String INSERT_VOTE = "insert into votes (email, id, type) values (?,?,?)";

    // Blocks Table Query Strings
    public static final String INSERT_BLOCK = "insert into blocks (blocker, blockee) values (?,?)";

    // Flags Table Query Strings
    public static final String INSERT_FLAG = "insert into flags (id, flaggerEmail) values (?,?)";
    public static final String SELECT_FLAG = "select * from flags where id = ? and flaggerEmail = ?";

    // Links Table Query Strings
    public static final String INSERT_LINK = "insert into links (messageId, link, type) values (?,?,?)";

}

// boolean disconnect() {
//     if (mConnection == null) {
//         System.err.println("Unable to close connection: Connection was null");
//         return false;
//     }
//     try {
//         mConnection.close();
//     } catch (SQLException e) {
//         System.err.println("Error: Connection.close() threw a SQLException");
//         e.printStackTrace();
//         mConnection = null;
//         return false;
//     }
//     mConnection = null;
//     return true;
// }