package edu.lehigh.cse216.cse216team_hamit.backend.links;
import edu.lehigh.cse216.cse216team_hamit.backend.*;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class LinkDatabase
{
    private static LinkDatabase mInstance = null;

    public static LinkDatabase getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new LinkDatabase();
        }
        return mInstance;
    }

    private PreparedStatement mInsertLink;

    private LinkDatabase() {
        try
        {
            mInsertLink = Constants.CONNECTION.prepareStatement(Constants.INSERT_LINK);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public boolean insertLink(int messageId, String link, String type)
    {
        boolean success = false;

        try
        {
            mInsertLink.setInt(1, messageId);
            mInsertLink.setString(2, link);
            mInsertLink.setString(3, type);

            mInsertLink.execute();
            success = true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return success;
    }

}
