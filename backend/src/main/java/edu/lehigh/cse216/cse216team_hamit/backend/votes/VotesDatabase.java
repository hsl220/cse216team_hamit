package edu.lehigh.cse216.cse216team_hamit.backend.votes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import edu.lehigh.cse216.cse216team_hamit.backend.Constants;

public class VotesDatabase
{
    private static VotesDatabase mInstance = null;

    public static VotesDatabase getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new VotesDatabase();
        }
        return mInstance;
    }

    private PreparedStatement mSelectTypeWithEmailAndMessageId;
    private PreparedStatement mRemoveVoteWithEmailAndMessageId;
    private PreparedStatement mUpdateVoteType;
    private PreparedStatement mInsertVote;

    /**
     * Creates the instance of the VotesDatabase class and sets up all the SQL statements.
     */
    private VotesDatabase() {
        try
        {
            mSelectTypeWithEmailAndMessageId = Constants.CONNECTION.prepareStatement(Constants.SELECT_VOTE_TYPE_FOR_MESSAGE_WITH_EMAIL_AND_MESSAGE_ID);
            mRemoveVoteWithEmailAndMessageId = Constants.CONNECTION.prepareStatement(Constants.DELETE_VOTE_EMAIL_AND_MESSAGE_ID);
            mUpdateVoteType = Constants.CONNECTION.prepareStatement(Constants.UPDATE_VOTE_TYPE_WITH_EMAIL_AND_MESSAGE_ID);
            mInsertVote = Constants.CONNECTION.prepareStatement(Constants.INSERT_VOTE);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Gets the type of vote, downvote or upvote, for the vote with the corresponding user email and message id.
     * @param email
     * @param messageId
     * @return 0 if there is no vote found, -1 if it's a downvote, and 1 if it's an upvote.
     */
    public int selectVoteTypeForVoteWithEmailAndMessageId(String email, int messageId)
    {
        int type = 0;

        try
        {
            mSelectTypeWithEmailAndMessageId.setString(1, email);
            mSelectTypeWithEmailAndMessageId.setInt(2, messageId);

            ResultSet result = mSelectTypeWithEmailAndMessageId.executeQuery();

            if (result.next())
            {
                type = result.getInt("type");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return type;
    }

    /**
     * Deletes a vote with the corresponding user email and message id.
     * @param email
     * @param messageId
     * @return the type of the vote that was deleted (1 or -1), or 0 if something went wrong
     */
    public int deleteVoteWithEmailAndMessageId(String email, int messageId)
    {
        int type = 0;
        try
        {
            int tempType = selectVoteTypeForVoteWithEmailAndMessageId(email, messageId);
            mRemoveVoteWithEmailAndMessageId.setString(1, email);
            mRemoveVoteWithEmailAndMessageId.setInt(2, messageId);

            mRemoveVoteWithEmailAndMessageId.execute();
            type = tempType;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return type;
    }

    /**
     * Update the type of the vote with the corresponding email and messageId.
     * @param email
     * @param messageId
     * @param voteType
     * @return true if the update was successful, else false.
     */
    public boolean updateVoteType(String email, int messageId, int voteType)
    {
        boolean success = false;

        try
        {
            mUpdateVoteType.setInt(1, voteType);
            mUpdateVoteType.setString(2, email);
            mUpdateVoteType.setInt(3, messageId);

            mUpdateVoteType.execute();
            success = true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return success;
    }

    /**
     * Inserts a vote row with the corresponding email, messageId, and type into the votes table.
     * @param email
     * @param messageId
     * @param voteType
     * @return true if the vote was inserted successfully, else false.
     */
    public boolean insertVote(String email, int messageId, int voteType)
    {
        boolean success = false;

        try
        {
            mInsertVote.setString(1, email);
            mInsertVote.setInt(2, messageId);
            mInsertVote.setInt(3, voteType);

            mInsertVote.execute();
            success = true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return success;
    }
}