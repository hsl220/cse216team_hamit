package edu.lehigh.cse216.cse216team_hamit.backend.links;

import com.google.gson.Gson;
import spark.Route;
import java.util.ArrayList;
import edu.lehigh.cse216.cse216team_hamit.backend.*;
import edu.lehigh.cse216.cse216team_hamit.backend.comments.CommentRow;
import edu.lehigh.cse216.cse216team_hamit.backend.comments.CommentsDatabase;

public class LinkRouteHandler
{
    private static final Gson gson = new Gson();

    public static Route getCommentsForMessageWithId = (request, response) -> {

        int id = Integer.parseInt(request.params(":id"));

        ArrayList<CommentRow> comments = CommentsDatabase.getInstance().getCommentsWithMessageId(id);

        if (comments == null)
        {
            return new StructuredResponse("ok", "no comments for messages", null);
        }

        return new StructuredResponse("ok", null, comments);
    };

    public static Route getCommentsWithEmail = (request, response) -> {

        String email = request.params(":email");

        ArrayList<CommentRow> comments = CommentsDatabase.getInstance().getCommentsWithEmail(email);

        if (comments == null)
        {
            return new StructuredResponse("ok", "no comments from this user", null);
        }

        return new StructuredResponse("ok", null, comments);
    };

    // public static Route insertLink = (request, response) -> {
    //     LinkRequest linkRequest = gson.fromJson(request.body(), LinkRequest.class);

    //     int messageId = Integer.parseInt(request.params(":id"));
    //     String body = commentRequest.body;

    //     boolean success = CommentsDatabase.getInstance().insertCommentForMessage(authorEmail, messageId, body);

    //     if (success)
    //     {
    //         return new StructuredResponse("ok", null, null);
    //     }

    //     return new StructuredResponse("error", "could not insert comment", null);
    // };
}
