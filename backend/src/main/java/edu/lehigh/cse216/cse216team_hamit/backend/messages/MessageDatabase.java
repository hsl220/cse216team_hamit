package edu.lehigh.cse216.cse216team_hamit.backend.messages;

import edu.lehigh.cse216.cse216team_hamit.backend.*;
import edu.lehigh.cse216.cse216team_hamit.backend.links.LinkDatabase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MessageDatabase {

    private static MessageDatabase mInstance;

    public static MessageDatabase getInstance() {
        if (mInstance == null) {
            mInstance = new MessageDatabase();
        }
        return mInstance;
    }

    private PreparedStatement mSelectAll;
    private PreparedStatement mSelectLinks;
    private PreparedStatement mSelectOne;
    private PreparedStatement mDeleteOne;
    private PreparedStatement mInsertOne;
    private PreparedStatement mUpvote;
    private PreparedStatement mDownvote;
    private PreparedStatement mSelectWithEmail;

    private MessageDatabase() {
        try
        {
            mSelectLinks = Constants.CONNECTION.prepareStatement(Constants.SELECT_LINKS_FOR_MESSAGE_WITH_ID);
            mSelectAll = Constants.CONNECTION.prepareStatement(Constants.SELECT_ALL_MESSAGES);
            mSelectOne = Constants.CONNECTION.prepareStatement(Constants.SELECT_MESSAGE_WITH_ID);
            mDeleteOne = Constants.CONNECTION.prepareStatement(Constants.DELETE_MESSAGE_WITH_ID);
            mInsertOne = Constants.CONNECTION.prepareStatement(Constants.INSERT_MESSAGE);
            mUpvote = Constants.CONNECTION.prepareStatement(Constants.UPVOTE_MESSAGE);
            mDownvote = Constants.CONNECTION.prepareStatement(Constants.DOWNVOTE_MESSAGE);
            mSelectWithEmail = Constants.CONNECTION.prepareStatement(Constants.SELECT_MESSAGE_WITH_EMAIL);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public int insertMessageRow(String email, String title, String body, ArrayList<String> fileLinks) {
        int count = 0;
        String type = "Message";
        // try {
        //     mInsertOne.setString(1, email);
        //     mInsertOne.setString(2, title);
        //     mInsertOne.setString(3, body);
        //     ///for each after splitting link
        //     count += mInsertOne.executeUpdate();
        // } catch (SQLException e) {
        //     e.printStackTrace();
        // }

        try {

            String statementString = String.format("insert into messages (authorEmail, title, body) values (%s,%s,%s)", email, title, body);
            Statement statement = Constants.CONNECTION.createStatement();
            int newlyCreatedMessageId = statement.executeUpdate(statementString, Statement.RETURN_GENERATED_KEYS);
            for (String link : fileLinks) {
                // LinkDatabase.instance.insert(messageId, link);
                LinkDatabase.getInstance().insertLink(newlyCreatedMessageId, link, type);
            }

        } catch(SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public int upvote(int id) {
        int count = 0;
        try {
            mUpvote.setInt(1, id);
            count += mUpvote.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public int downvote(int id) {
        int count = 0;
        try {
            mDownvote.setInt(1, id);
            count += mDownvote.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public ArrayList<MessageRow> selectAll(String blockerEmail) {
        ArrayList<MessageRow> res = new ArrayList<MessageRow>();
        try {
            mSelectAll.setString(1, blockerEmail);
            ResultSet rs = mSelectAll.executeQuery();
            while (rs.next()) {
                res.add(new MessageRow(rs.getInt("id"), rs.getString("authorEmail"), rs.getString("title"), rs.getString("body"),
                        rs.getDate("createdDate"), rs.getDate("updatedDate"), rs.getInt("upvoteCount"),
                        rs.getInt("downvoteCount"), new ArrayList<>()));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public MessageRow selectOne(int id) {
        MessageRow res = null;
        try {
            mSelectOne.setInt(1, id);
            ResultSet rs = mSelectOne.executeQuery();
            if (rs.next()) {
                int messageId = rs.getInt("id");
                mSelectLinks.setInt(1, messageId);
                ResultSet links = mSelectLinks.executeQuery();
                ArrayList<String> urls = new ArrayList<String>();
                while(links.next()){
                    String url = links.getString("url");
                    urls.add(url);
                }
                res = new MessageRow(rs.getInt("id"),  rs.getString("authorEmail"), rs.getString("title"), rs.getString("body"), rs.getDate("createdDate"), rs.getDate("updatedDate"), rs.getInt("upvoteCount"), rs.getInt("downvoteCount"),urls);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public ArrayList<MessageRow> selectWithEmail(String email) {
        ArrayList<MessageRow> messages = null;
        try
        {
            mSelectWithEmail.setString(1, email);
            ResultSet rs = mSelectWithEmail.executeQuery();

            if (rs.next())
            {
                messages = new ArrayList<MessageRow>();
            }

            do
            {
                messages.add(new MessageRow(rs.getInt("id"), rs.getString("authorEmail"), rs.getString("title"), rs.getString("body"),
                rs.getDate("createdDate"), rs.getDate("updatedDate"), rs.getInt("upvoteCount"),
                rs.getInt("downvoteCount"), new ArrayList<>()));
            }
            while (rs.next());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return messages;
    }

    public int deleteRow(int id) {
        int res = -1;
        try {
            mDeleteOne.setInt(1, id);
            res = mDeleteOne.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
}
