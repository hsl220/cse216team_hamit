package edu.lehigh.cse216.cse216team_hamit.backend.flags;

public class FlagRow
{
    int mId;
    String mFlaggerEmail;

    public int getId() {
        return this.mId;
    }

    public String getFlaggerEmail() {
        return this.mFlaggerEmail;
    }
    public FlagRow(int id, String email)
    {
        mId = id;
        mFlaggerEmail = email;
    }
}