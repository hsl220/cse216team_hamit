package edu.lehigh.cse216.cse216team_hamit.backend.messages;

import java.util.ArrayList;
import java.sql.Date;

public class MessageRow {
	private int mId;
	private String mSenderEmail;
	private String mTitle;
	private String mBody;
	private Date mDateCreated;
	private Date mDateUpdated;
	private int mUpvoteCount;
	private int mDownvoteCount;
	private ArrayList<String> urls;

	public MessageRow(int id, String senderEmail, String title, String body, Date dateCreated, Date dateUpdated, int upvoteCount,
			int downvoteCount, ArrayList<String> urls) {
		mId = id;
		mSenderEmail = senderEmail;
		mTitle = title;
		mBody = body;
		mDateCreated = dateCreated;
		mDateUpdated = dateUpdated;
		mUpvoteCount = upvoteCount;
		mDownvoteCount = downvoteCount;
		this.urls = urls;
	}

	public int getMid() {
		return this.mId;
	}

	public String getSenderEmail() {
		return mSenderEmail;
	}

	public String getMtitle() {
		return this.mTitle;
	}

	public String getMbody() {
		return this.mBody;
	}

	public Date getMdatecreated() {
		return this.mDateCreated;
	}

	public Date getMdateupdated() {
		return this.mDateUpdated;
	}

	public int getMupvotecount() {
		return this.mUpvoteCount;
	}

	public int getMdownvotecount() {
		return this.mDownvoteCount;
	}

	public ArrayList<String> getUrls(){
		return this.urls;
	}
}