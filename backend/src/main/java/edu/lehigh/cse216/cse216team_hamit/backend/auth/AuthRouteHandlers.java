 package edu.lehigh.cse216.cse216team_hamit.backend.auth;

import spark.Route;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

import com.google.gson.Gson;

import edu.lehigh.cse216.cse216team_hamit.backend.*;
import edu.lehigh.cse216.cse216team_hamit.backend.users.*;
import java.util.Collections;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.*;
//import com.google.api.client.drive.Drive;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import java.io.File;
import java.io.FileReader;


public class AuthRouteHandlers
{
    private static Gson gson = new Gson();

    public static Route useoAuth = (request, response) -> {;
        // (Receive authCode via HTTPS POST)
        AuthRequest authRequest = gson.fromJson(request.body(), AuthRequest.class);
        String authCode = request.params(":authCode");
        response.status(200);
        response.type("application/json");
    NetHttpTransport transport = new NetHttpTransport();
    JacksonFactory jacksonFactory = JacksonFactory.getDefaultInstance();
    // Exchange auth code for access token
    GoogleTokenResponse tokenResponse =
            new GoogleAuthorizationCodeTokenRequest(
                transport,
                jacksonFactory,
                "https://www.googleapis.com/oauth2/v4/token",
                "760530349142-p66fpijr1lrohcrbsonrio5ef717c4d2.apps.googleusercontent.com",
                "Bx9WhR41cpcxaE19B-vyHoqU",
                authCode,
                "https://glacial-fjord-88850.herokuapp.com")  // Specify the same redirect URI that you use with your web
                               // app. If you don't have a web version of your app, you can
                               // specify an empty string.
                .execute();
  
    String accessToken = tokenResponse.getAccessToken();
  
    // Use access token to call API
    GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
  
    GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, tokenResponse.getFactory())
    // Specify the CLIENT_ID of the app that accesses the backend:
    .setAudience(Collections.singletonList("760530349142-p66fpijr1lrohcrbsonrio5ef717c4d2.apps.googleusercontent.com"))
    .build();
    // Get profile info from ID token
    GoogleIdToken idToken = tokenResponse.parseIdToken();
    idToken = verifier.verify(idToken.toString());
    GoogleIdToken.Payload payload = idToken.getPayload();
    String domain = payload.getHostedDomain(); //check to see if lehigh domain

    //idtoken.verify to verify the token
    
    if (idToken != null && domain.equals("lehigh.edu")) {
        String newSessionKey = UUID.randomUUID().toString();
        boolean success = UserDatabase.getInstance().updateSessionKeyForUserWithEmail(authRequest.email, newSessionKey);
             return new StructuredResponse("ok", null, newSessionKey);
        }
        else  {
            return new StructuredResponse("error", "null token", null);
        }
    };

/*
    public static Route login = (request, response) -> {
        AuthRequest authRequest = gson.fromJson(request.body(), AuthRequest.class);
        response.status(200);
        response.type("application/json");

        // use email find the user and get their salt
        String email = authRequest.email;
        if (email == null)
        {
            return new StructuredResponse("error", "email address is needed", null);
        }

        UserRow user = UserDatabase.getInstance().selectOne(email);
        String userSig = user.getMSignature();

        String hash = authRequest.hash;

        // if (hash.equals(validHash))
        // {
        //     // the password the user entered is valid
        //     // create a new session key and send that key back to the user
        //     return new StructuredResponse("ok", null, null);
        // }
        // else
        // {
        //     // incorrect password
        //     return new StructuredResponse("error", "incorrect password", null);
        // }

        if (hash.equals(userSig))
        {
            // generate new session key and send that back to the user
            String newSessionKey = UUID.randomUUID().toString();
            boolean success = UserDatabase.getInstance().updateSessionKeyForUserWithEmail(email, newSessionKey);
            if (success)
            {
                return new StructuredResponse("ok", null, newSessionKey);
            }
            else
            {
                return new StructuredResponse("error", "could not login user. new session key could not be updated", null);
            }
        }
        else
        {
            return new StructuredResponse("error", "incorrect password", null);
        }

    };

    public static Route getSalt = (request, response) -> {
        String email = request.params(":email");
        response.status(200);
        response.type("application/json");

        if (email == null)
        {
            return new StructuredResponse("error", "email needed", null);
        }
        // gets the salt for user with that email
        String salt = UserDatabase.getInstance().getSaltForUserWithEmail(email);
        // checks to make sure that the user exists and there was a salt value returned
        if (salt == null)
        {
            return new StructuredResponse("error", "no user found for email: " + email, null);
        }
        return new StructuredResponse("ok", null, salt);
    };

    public static Route validateCurrentPassword = (request, response) -> {
        // in order to reset a password you need the original password
        AuthRequest authRequest = gson.fromJson(request.body(), AuthRequest.class);
        response.status(200);
        response.type("application/json");

        String email = authRequest.email;
        String salt = UserDatabase.getInstance().getSaltForUserWithEmail(email);
        String password = authRequest.password;
        String hash = authRequest.hash;

        if (validateHash(email, password, salt, hash))
        {
            // the hash is valid
            return new StructuredResponse("ok", null, null);
        }
        else
        {
            // hash is not valid
            return new StructuredResponse("error", "password does not match the current password for user with email: " + email, null);
        }
    }; 

    public static Route resetPassword = (request, response) -> {
        AuthRequest authRequest = gson.fromJson(request.body(), AuthRequest.class);
        response.status(200);
        response.type("application/json");

        String email = authRequest.email;
        String password = authRequest.password;
        String salt = authRequest.salt;
        String hash = authRequest.hash;

        if (validateHash(email, password, salt, hash))
        {
            boolean success = UserDatabase.getInstance().resetPassword(email, password, salt, hash);
            if (success)
            {
                return new StructuredResponse("ok", "successfully reset password", null);
            }
            else
            {
                return new StructuredResponse("error", "password could not be reset", null);
            }
        }
        else
        {
            return new StructuredResponse("error", "password could not be reset", null);
        }
    }; */

    public static Route getSessionKeyForUser = (request, response) -> {
        String email = request.params(":email");
        response.status(200);
        response.type("application/json");

        if (email == null) {
            System.out.println("EMAIL IS NULL");
            return null;
        }

        String sessionKey = UserDatabase.getInstance().selectSessionKey(email);

        if (sessionKey.equals(" ") || sessionKey == null)
        {
            return new StructuredResponse("error", "no session key found. user has never logged in before", null);
        }
        else
        {
            return new StructuredResponse("ok", null, sessionKey);
        }
    };

     /**
     * This method will generate a hashed password using the given password and salt.
     * It will then check to see if the generated hash matches the given hash.
     * @param password
     * @param salt
     * @param hash
     * @return false if the passed in hash is not equal to the generated hash, else true.
     
    public static boolean validateHash(String email, String password, String salt, String hash)
    {
        boolean isValid = false;
        try
        {
            String selectedSalt = UserDatabase.getInstance().getSaltForUserWithEmail(email);
            String validHash = AuthHelper.generateHash(password + selectedSalt);
            if (hash.equals(validHash) && salt.equals(selectedSalt))
            {
                isValid = true;
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return isValid;
    } */
    
}