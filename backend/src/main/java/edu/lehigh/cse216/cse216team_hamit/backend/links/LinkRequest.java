package edu.lehigh.cse216.cse216team_hamit.backend.links;

public class LinkRequest
{
    public int messageId;
    public String link;
    public String type;
}
