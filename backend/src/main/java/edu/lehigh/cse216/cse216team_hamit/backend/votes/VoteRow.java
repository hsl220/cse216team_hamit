package edu.lehigh.cse216.cse216team_hamit.backend.votes;

public class VoteRow
{
    String mEmail;
    int mMessageId;
    int mVoteType;

    public String getEmail() {
        return this.mEmail;
    }

    public int getMessageId() {
        return this.mMessageId;
    }

    public int getVoteType() {
        return this.mVoteType;
    }

    public VoteRow(String email, int messageId, int voteType)
    {
        mEmail = email;
        mMessageId = messageId;
        mVoteType = voteType;
    }
}