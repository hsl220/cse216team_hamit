package edu.lehigh.cse216.cse216team_hamit.backend.comments;

import edu.lehigh.cse216.cse216team_hamit.backend.*;
import edu.lehigh.cse216.cse216team_hamit.backend.links.LinkDatabase;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class CommentsDatabase
{
    private static CommentsDatabase mInstance = null;

    public static CommentsDatabase getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new CommentsDatabase();
        }
        return mInstance;
    }

    private PreparedStatement mSelectCommentsForMessage;
    private PreparedStatement mInsertComment;
    private PreparedStatement mSelectCommentsWithEmail;

    private CommentsDatabase() {
        try
        {
            mSelectCommentsForMessage = Constants.CONNECTION.prepareStatement(Constants.SELECT_COMMENTS_WITH_MESSAGE_ID);
            mInsertComment = Constants.CONNECTION.prepareStatement(Constants.INSERT_COMMENT);
            mSelectCommentsWithEmail = Constants.CONNECTION.prepareStatement(Constants.SELECT_COMMENTS_WITH_EMAIL);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public ArrayList<CommentRow> getCommentsWithEmail(String email)
    {
        ArrayList<CommentRow> comments = null;
        try
        {
            mSelectCommentsWithEmail.setString(1, email);
            ResultSet result = mSelectCommentsWithEmail.executeQuery();

            if (result.next())
            {
                comments = new ArrayList<CommentRow>();
            }

            do
            {
                int id = result.getInt("id");
                String body = result.getString("body");
                String authorEmail = result.getString("authorEmail");
                Date createdDate = result.getDate("createdDate");
                CommentRow comment = new CommentRow(id, authorEmail, body, createdDate, new ArrayList<>());
                comments.add(comment);
            }
            while (result.next());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return comments;
    }

    public ArrayList<CommentRow> getCommentsWithMessageId(int idx)
    {
        ArrayList<CommentRow> comments = null;
        try
        {
            mSelectCommentsForMessage.setInt(1, idx);
            ResultSet result = mSelectCommentsForMessage.executeQuery();

            if (result.next())
            {
                comments = new ArrayList<CommentRow>();
            }

            do
            {
                int id = result.getInt("id");
                String body = result.getString("body");
                String authorEmail = result.getString("authorEmail");
                Date createdDate = result.getDate("createdDate");
                CommentRow comment = new CommentRow(id, authorEmail, body, createdDate, new ArrayList<>());
                comments.add(comment);
            }
            while (result.next());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return comments;
    }

    public boolean insertCommentForMessage(String email, int messageId, String body, ArrayList<String> urls)
    {
        boolean success = false;
        String type = "Comment";
        try
        {
            mInsertComment.setInt(1, messageId);
            mInsertComment.setString(2, body);
            mInsertComment.setString(3, email);

            mInsertComment.execute();
            for (String url : urls) {
                LinkDatabase.getInstance().insertLink(messageId, url, type);
            }
            success = true;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return success;
    }

}