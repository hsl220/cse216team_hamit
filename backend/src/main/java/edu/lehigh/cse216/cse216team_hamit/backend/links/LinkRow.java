package edu.lehigh.cse216.cse216team_hamit.backend.links;

public class LinkRow
{
    int mId;
    String mLink;


    public int getId() {
        return this.mId;
    }

    public String getLink() {
        return this.mLink;
    }

    public LinkRow(int id, String link)
    {
        mId = id;
        mLink = link;
    }
}
