package edu.lehigh.cse216.cse216team_hamit.backend;

import edu.lehigh.cse216.cse216team_hamit.backend.users.UserDatabase;

public class AuthenticatedRequest
{
    public String email;
    public String sessionKey;

    public boolean isAuthenticated()
    {
        // make sure that the request contains these values
        if (email == null || sessionKey == null)
        {
            return false;
        }

        // try to select the session key for the user with the email from database
        String selectedSessionKey = UserDatabase.getInstance().selectSessionKey(email);

        // check if the session key was found
        if (selectedSessionKey == null)
        {
            return false;
        }

        // check if session key received matches the one selected from the database
        if (sessionKey.equals(selectedSessionKey))
        {
            return true;
        }

        return false;
    }
}