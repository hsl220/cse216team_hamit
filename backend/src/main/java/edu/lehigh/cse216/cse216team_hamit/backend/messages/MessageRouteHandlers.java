package edu.lehigh.cse216.cse216team_hamit.backend.messages;

import edu.lehigh.cse216.cse216team_hamit.backend.*;
import edu.lehigh.cse216.cse216team_hamit.backend.messages.MessageRow;
import com.google.gson.Gson;
import spark.Route;
import java.util.ArrayList;

public class MessageRouteHandlers
{
    private static final Gson gson = new Gson();

    public static Route getMessageWithId = (request, response) -> {

        int idx = Integer.parseInt(request.params(":id"));
        response.status(200);
        response.type("application/json");
        MessageRow data = MessageDatabase.getInstance().selectOne(idx);

        if (data == null) {
            return new StructuredResponse("error", idx + " not found", null);
        } else {
            return new StructuredResponse("ok", null, data);
        }
    };

    public static Route getMessagesWithEmail = (request, response) -> {

        String email = request.params(":email");
        response.status(200);
        response.type("application/json");
        ArrayList<MessageRow> data = MessageDatabase.getInstance().selectWithEmail(email);

        if (data == null) {
            return new StructuredResponse("error", email + " has no messages", null);
        } else {
            return new StructuredResponse("ok", null, data);
        }
    };

    public static Route getMessages = (request, response) -> {
        String blockerEmail = request.params(":email");
        response.status(200);
        response.type("application/json");
        ArrayList<MessageRow> data = MessageDatabase.getInstance().selectAll(blockerEmail);
        if (data == null) {
            return new StructuredResponse("error", "messages table not found", null);
        } else {
            return new StructuredResponse("ok", null, data);
        }
    };

    public static Route postMessage = (request, response) -> {
        MessageRequest req = gson.fromJson(request.body(), MessageRequest.class);
        response.status(200);
        response.type("application/json");
        int newId = MessageDatabase.getInstance().insertMessageRow(req.email, req.title, req.body, req.fileLinks);
        if (newId == -1) {
            return new StructuredResponse("error", "error performing insertion", null);
        } else {
            return new StructuredResponse("ok", "" + newId, null);
        }
    };

    public static Route upVoteMessage = (request, response) -> {
        int mId = Integer.parseInt(request.params("id"));
        response.status(200);
        response.type("application/json");
        int newId = MessageDatabase.getInstance().upvote(mId);
        if (newId == -1) {
            return new StructuredResponse("error", "error performing upvote", null);
        } else {
            return new StructuredResponse("ok", "" + newId, null);
        }
    };

    public static Route downVoteMessage = (request, response) -> {
        int mId = Integer.parseInt(request.params("id"));
        response.status(200);
        response.type("application/json");
        int newId = MessageDatabase.getInstance().downvote(mId);
        if (newId == -1) {
            return new StructuredResponse("error", "error performing downvote", null);
        } else {
            return new StructuredResponse("ok", "" + newId, null);
        }
    };
}