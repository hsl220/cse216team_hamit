package edu.lehigh.cse216.cse216team_hamit.backend.users;

import com.google.gson.Gson;

import edu.lehigh.cse216.cse216team_hamit.backend.StructuredResponse;
import spark.Route;

public class UserRouteHandlers
{
    private static final Gson gson = new Gson();

    public static Route getUserWithEmail = (request, response) -> {
        response.status(200);
        response.type("application/json");

        String email = request.params("email");

        UserRow user = UserDatabase.getInstance().selectOne(email);

        if (user == null)
        {
            return new StructuredResponse("ok", "no user with email: " + email + " found", null);
        }
        else
        {
            return new StructuredResponse("ok", null, user);
        }
    };
}