package edu.lehigh.cse216.cse216team_hamit.backend;

import edu.lehigh.cse216.cse216team_hamit.backend.auth.*;
import edu.lehigh.cse216.cse216team_hamit.backend.messages.MessageRouteHandlers;
import edu.lehigh.cse216.cse216team_hamit.backend.users.UserRouteHandlers;
import edu.lehigh.cse216.cse216team_hamit.backend.votes.VoteRouteHandlers;
import edu.lehigh.cse216.cse216team_hamit.backend.flags.FlagRouteHandler;
import edu.lehigh.cse216.cse216team_hamit.backend.blocks.BlockRouteHandler;
import edu.lehigh.cse216.cse216team_hamit.backend.comments.*;

import java.util.Map;
import static spark.Spark.*; //Import Spark Framework
import com.google.gson.*; // Import Google's JSON library

public class App {
    public static void main(String[] args) {

        // Set the port for our
        port(getIntFromEnv("PORT", 4567));

        final Gson gson = new Gson();

        Map<String, String> env = System.getenv();

        String cors_enabled = env.get("CORS_ENABLED");
        if (cors_enabled == null) {
            cors_enabled = "false";
        }
        if (cors_enabled.equals("True")) {
            final String acceptCrossOriginRequestsFrom = "*";
            final String acceptedCrossOriginRoutes = "GET,PUT,POST,DELETE,OPTIONS";
            final String supportedRequestHeaders = "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin";
            enableCORS(acceptCrossOriginRequestsFrom, acceptedCrossOriginRoutes, supportedRequestHeaders);
        }

        // Set up the location for serving static files. If the STATIC_LOCATION
        // environment variable is set, we will serve from it. Otherwise, serve
        // from "/web"
        String static_location_override = System.getenv("STATIC_LOCATION");
        if (static_location_override == null)
        {
            staticFileLocation("/web");
        }
        else
        {
            staticFiles.externalLocation(static_location_override);
        }

        // MARK: Auth routes

        //put("/login", AuthRouteHandlers.login, gson::toJson);

        //get("/salt/:email", AuthRouteHandlers.getSalt, gson::toJson);

        get("/sessionKey/:email", AuthRouteHandlers.getSessionKeyForUser, gson::toJson);

        //put("/resetPassword", AuthRouteHandlers.resetPassword, gson::toJson);

        put("/auth", AuthRouteHandlers.useoAuth, gson::toJson); //??

/*
        // checks that each request contains the information (email, sessionKey) to make sure the request is authenticated
        before((request, response) -> {

            String path = request.pathInfo();
            String[] firstWordInPath = path.split("/");

            switch (firstWordInPath[0]) {
                case "login":
                case "salt":
                case "sessionKey":
                case "resetPassword":
                    break;
                default:
                {
                    AuthenticatedRequest authenticatedRequest = gson.fromJson(request.body(), AuthenticatedRequest.class);

                    if (!authenticatedRequest.isAuthenticated())
                    {
                        halt(401, "user is not authenticated");
                    }
                }
            }
        }); */


        // MARK: Message routes

        get("/messages/:id", MessageRouteHandlers.getMessageWithId, gson::toJson);

        get("/messages-with-blocker-email/:email", MessageRouteHandlers.getMessages, gson::toJson);

        post("/messages", MessageRouteHandlers.postMessage, gson::toJson);

        get("/messages-with-email/:email", MessageRouteHandlers.getMessagesWithEmail, gson::toJson);

        // MARK: Comment routes

        post("/comments/:id", CommentsRouteHandler.insertComment, gson::toJson);

        get("/comments/:id", CommentsRouteHandler.getCommentsForMessageWithId, gson::toJson);

        get("/comment-with-email/:email", CommentsRouteHandler.getCommentsWithEmail, gson::toJson);


        // MARK: Vote routes

        post("/votes", VoteRouteHandlers.insertVote, gson::toJson);

        delete("/votes", VoteRouteHandlers.deleteVote, gson::toJson);

        get("/votes/type", VoteRouteHandlers.getVoteType, gson::toJson);

        put("/votes/type", VoteRouteHandlers.updateVoteType, gson::toJson);

        //MARK: User routes

        get("/users/:email", UserRouteHandlers.getUserWithEmail, gson::toJson);


        //MARK: Block routes
        post("/block", BlockRouteHandler.createBlock, gson::toJson);

        //MARK: Flag routes
        post("/flag", FlagRouteHandler.createFlag, gson::toJson);

        get("/flag/:id/:userEmail", FlagRouteHandler.findFlag, gson::toJson);


    }

    /**
     * Get an integer environment varible if it exists, and otherwise return the
     * default value.
     *
     * @envar The name of the environment variable to get.
     * @defaultVal The integer value to use as the default if envar isn't found
     *
     * @returns The best answer we could come up with for a value for envar
     */
    static int getIntFromEnv(String envar, int defaultVal) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get(envar) != null) {
            return Integer.parseInt(processBuilder.environment().get(envar));
        }
        return defaultVal;
    }

    /**
     * Set up CORS headers for the OPTIONS verb, and for every response that the
     * server sends. This only needs to be called once.
     *
     * @param origin  The server that is allowed to send requests to this server
     * @param methods The allowed HTTP verbs from the above origin
     * @param headers The headers that can be sent with a request from the above
     *                origin
     */
    public static void enableCORS(String origin, String methods, String headers) {
        // Create an OPTIONS route that reports the allowed CORS headers and methods
        options("/*", (request, response) -> {
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }
            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            return "OK";
        });

        // 'before' is a decorator, which will run before any
        // get/post/put/delete. In our case, it will put three extra CORS
        // headers into the response
        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
        });
    }
}