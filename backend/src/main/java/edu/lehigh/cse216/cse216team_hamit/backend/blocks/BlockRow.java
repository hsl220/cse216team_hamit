package edu.lehigh.cse216.cse216team_hamit.backend.blocks;

public class BlockRow
{
    String mBlocker;
    String mBlockee;

    public String getBlocker() {
        return this.mBlocker;
    }

    public String getBlockee() {
        return this.mBlockee;
    }

    public BlockRow(String blocker, String blockee)
    {
        mBlocker = blocker;
        mBlockee = blockee;
    }
}
