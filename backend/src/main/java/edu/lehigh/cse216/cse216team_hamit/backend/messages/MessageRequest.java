package edu.lehigh.cse216.cse216team_hamit.backend.messages;

import java.util.ArrayList;

public class MessageRequest
{
    public String email;
    public String title;
    public String body;
    public ArrayList<String> fileLinks;
}