"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class User {
    constructor(email, name) {
        this.email = email;
        this.name = name;
    }
    static parseUser(data) {
        const { mEmail, mName } = data;
        return new User(mEmail, mName);
    }
}
class UserService {
    constructor() { }
    getUserInfoWithEmail(email) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                url: Constants.API_URL + `/users/${email}`,
                method: 'GET',
                dataType: 'json'
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                resolve(User.parseUser(mData));
            }
            else {
                reject(mMessage);
            }
        }));
    }
}
UserService.instance = new UserService();
class Message {
    constructor(id, senderEmail, title, body, dateCreated, upvoteCount, downvoteCount) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.senderEmail = senderEmail;
        this.dateCreated = dateCreated;
        this.upvoteCount = upvoteCount;
        this.downvoteCount = downvoteCount;
    }
    static parseMessage(data) {
        const { mId, mTitle, mBody, mSenderEmail, mDateCreated, mUpvoteCount, mDownvoteCount } = data;
        return new Message(mId, mSenderEmail, mTitle, mBody, mDateCreated, mUpvoteCount, mDownvoteCount);
    }
    update(title, body) {
        this.title = title;
        this.body = body;
    }
}
class Constants {
}
Constants.API_URL = 'https://glacial-fjord-88850.herokuapp.com';
class MessagesService {
    MessagesService() { }
    upvote(email, messageId) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                type: 'POST',
                url: Constants.API_URL + `/votes`,
                data: JSON.stringify({ email, messageId, type: 1 }),
                contentType: 'application/json',
                dataType: 'json'
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                resolve(true);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    downvote(email, messageId) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                type: 'POST',
                url: Constants.API_URL + `/votes`,
                data: JSON.stringify({ email, messageId, type: -1 }),
                contentType: 'application/json',
                dataType: 'json'
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === "ok") {
                resolve(true);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    postMessage(email, title, body) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                type: 'POST',
                url: Constants.API_URL + "/messages",
                contentType: 'application/json',
                data: JSON.stringify({ email, title, body }),
                dataType: 'json'
            })
                .catch(error => console.log(error));
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                resolve(true);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    getMessages(email) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                type: "GET",
                url: Constants.API_URL + `/messages-with-blocker-email/${email}`,
                dataType: 'json'
            });
            const { mStatus, mData, mMessage } = result;
            let messages = [];
            if (mStatus === "ok") {
                let objs = mData;
                objs.forEach(obj => {
                    messages.push(Message.parseMessage(obj));
                });
                resolve(messages);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    getMessagesWithAuthorEmail(email) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                url: Constants.API_URL + `/messages-with-email/${email}`,
                method: 'GET',
                dataType: 'json'
            });
            const { mStatus, mData, mMessage } = result;
            let messages = [];
            if (mStatus === 'ok') {
                let objects = mData;
                objects.forEach(obj => {
                    let message = Message.parseMessage(obj);
                    messages.push(message);
                });
                resolve(messages);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    updateMessage(id, title, body) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                url: Constants.API_URL + `messages/${id}`,
                method: 'PUT',
                data: JSON.stringify({ title, body }),
                dataType: 'json'
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                resolve(true);
            }
            else {
                reject(mMessage);
            }
        }));
    }
}
MessagesService.instance = new MessagesService();
class ProfileScreen {
    static setSelectedUserWithEmail(email, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let user = yield UserService.instance.getUserInfoWithEmail(email);
                ProfileScreen.selectedUser = user;
                ProfileScreen.show();
                ProfileScreen.setCloseBtnHandler(callback);
            }
            catch (error) {
                window.alert(error);
            }
        });
    }
    static setCloseBtnHandler(afterCloseHandler) {
        $(`#${ProfileScreen.NAME}-btn-close`).click(() => {
            ProfileScreen.hide();
            afterCloseHandler();
        });
    }
    static init() {
        if (!ProfileScreen.isInit) {
            var user = new User('adg219@lehigh.edu', 'alexander greene');
            let html = `<div id="${ProfileScreen.NAME}">` +
                `<header id="${ProfileScreen.NAME}-header">` +
                `<button id="${ProfileScreen.NAME}-btn-close" class="btn"><i class="fa fa-times"></i></button>` +
                `<h1>Profile</h1>` +
                `</header>` +
                `</div>`;
            $('#app').append(html);
            ProfileScreen.isInit = true;
        }
    }
    static refresh() {
        return __awaiter(this, void 0, void 0, function* () {
            ProfileScreen.init();
            $('#profile-card').remove();
            $(`#${ProfileScreen.NAME}-messages-table`).remove();
            $(`#${ProfileScreen.NAME}-messages-header`).remove();
            $(`#${ProfileScreen.NAME}`).append(HTMLStringCreator.createProfileHTML(ProfileScreen.selectedUser));
            try {
                let messages = yield MessagesService.instance.getMessagesWithAuthorEmail(ProfileScreen.selectedUser.email);
                let table = $(`<table id="${ProfileScreen.NAME}-messages-table"></table>`);
                messages.forEach(message => {
                    table.append(HTMLStringCreator.createMessageCardWithMessage(ProfileScreen.NAME, message));
                });
                let messagesHeader = $(`<h4 id="${ProfileScreen.NAME}-messages-header">Messages (${messages.length})</h4>`);
                $(`#${ProfileScreen.NAME}`).append(messagesHeader);
                $(`#${ProfileScreen.NAME}`).append(table);
            }
            catch (error) {
                window.alert(error);
            }
        });
    }
    static show() {
        ProfileScreen.init();
        $(`#${ProfileScreen.NAME}`).show();
        ProfileScreen.refresh();
    }
    static hide() {
        ProfileScreen.init();
        $(`#${ProfileScreen.NAME}`).hide();
        $(`#profile-card`).remove();
    }
}
ProfileScreen.NAME = "profile-screen";
ProfileScreen.isInit = false;
class MyNavigation {
    static showingNewPage() {
        MyNavigation.prevScrollPos = window.scrollY;
        window.scrollTo(0, 0);
    }
    static closingPage() {
        window.scrollTo(0, MyNavigation.prevScrollPos);
    }
}
MyNavigation.prevScrollPos = 0;
class HTMLStringCreator {
    static createMessageCard(componentName) {
        return `<div id="${componentName}-message-card" class="card">` +
            `<div class="card-body">` +
            `<h5 id="${componentName}-message-title" class="card-title"></h5>` +
            `<h6 id="${componentName}-message-subtitle" class="card-subtitle mb-2 text-muted"></h6>` +
            `<p id="${componentName}-message-text" class="card-text"></p>` +
            `</div>` +
            `</div>`;
    }
    static createMessageCardWithMessage(componentName, message) {
        const { title, body, senderEmail, dateCreated } = message;
        return `<div id="${componentName}-message-card" class="card">` +
            `<div class="card-body">` +
            `<h5 class="${componentName}-message-title" class="card-title">${title}</h5>` +
            `<h6 class="${componentName}-message-subtitle" class="card-subtitle mb-2 text-muted">${senderEmail} - ${dateCreated}</h6>` +
            `<p class="${componentName}-message-text" class="card-text">${body}</p>` +
            `</div>` +
            `</div>`;
    }
    static createCommentCard(componentName, comment) {
        return `<div class="card ${componentName}-comment-card">` +
            `<div class="card-body">` +
            `<h6 class="${componentName}-message-author-email" class="card-subtitle mb-2 text-muted">${comment.authorEmail}</h6>` +
            `<p class="${componentName}-message-text" class="card-text">${comment.body}</p>` +
            `</div>` +
            `</div>`;
    }
    static createFormGroupWithTextInput(className, label, placeholder, s) {
        return `<div class="form-group">` +
            `<label for="${s}">${label}</label>` +
            `<input type="text" class="form-control" id="${className}-field" aria-describedby="${s}Help" placeholder="${placeholder}">` +
            `</div>`;
    }
    static createFormGroupWithTextArea(className, label, s) {
        return `<div class="form-group">` +
            `<label for="${s}">${label}</label>` +
            `<textarea class="form-control" id="${className}-textarea" rows="3"></textarea>` +
            `</div>`;
    }
    static createProfileHTML(user) {
        const { email, name } = user;
        return `<div class="card" id="profile-card">` +
            `<div class="card-body">` +
            `<p id="profile-name" class="card-text">${name}</p>` +
            `<p id="profile-email" class="card-text">${email}</p>` +
            `</div>` +
            `</div>`;
    }
}
class AddMessageScreen {
    static init() {
        if (!AddMessageScreen.isInit) {
            let div = $(`<div id="${AddMessageScreen.NAME}"><div id="${AddMessageScreen.NAME}-header"><h1 class="text-center">Add Message</h1></div></div>`);
            let form = $(`<form></form>`);
            form.submit((e) => {
                e.preventDefault();
            });
            form.append(HTMLStringCreator.createFormGroupWithTextInput(AddMessageScreen.NAME, 'Title', 'Enter title', 'title'));
            form.append(HTMLStringCreator.createFormGroupWithTextArea(AddMessageScreen.NAME, 'Body', 'body'));
            let submitBtn = $(`<button class="btn btn-warning" id="${AddMessageScreen.NAME}-btn-submit">Add Message</button>`);
            submitBtn.click(AddMessageScreen.submit);
            let cancelBtn = $(`<button id="${AddMessageScreen.NAME}-btn-cancel" class="btn btn-light" style="margin-left: 10px;">Cancel</button>`);
            let chooseFileBtn = $(`<button id="${AddMessageScreen.NAME}-btn" class="btn btn-light" style="margin-left: 10px;">Choose File</button>`);
            form.append(submitBtn);
            form.append(cancelBtn);
            form.append(chooseFileBtn);
            div.append(form);
            $('#app').append(div);
            AddMessageScreen.isInit = true;
        }
    }
    static submit() {
        return __awaiter(this, void 0, void 0, function* () {
            AddMessageScreen.init();
            console.log('trying to add message');
            let title = $(`#${AddMessageScreen.NAME}-field`).val().toString();
            let body = $(`#${AddMessageScreen.NAME}-textarea`).val().toString();
            let email = "adg219@lehigh.edu";
            let success = yield MessagesService.instance.postMessage(email, title, body)
                .catch(errorMessage => window.alert(errorMessage));
            if (success) {
                console.log('message added successfully');
                AddMessageScreen.hide();
                ChatScreen.refresh();
                ChatScreen.show();
            }
            else {
                console.log('message could not be added');
            }
        });
    }
    static resetForm() {
        $(`#${AddMessageScreen.NAME}-field`).val('');
        $(`#${AddMessageScreen.NAME}-textarea`).val('');
    }
    static hide() {
        AddMessageScreen.init();
        AddMessageScreen.resetForm();
        $(`#${AddMessageScreen.NAME}`).hide();
    }
    static show() {
        AddMessageScreen.init();
        $(`#${AddMessageScreen.NAME}`).show();
    }
    static setCloseBtnListener(handler) {
        $(`#${AddMessageScreen.NAME}-btn-cancel`).click(() => {
            AddMessageScreen.hide();
            handler();
        });
    }
}
AddMessageScreen.NAME = 'add-message-form';
AddMessageScreen.isInit = false;
class MyComment {
    constructor(id, authorEmail, body, createdDate) {
        this.id = id;
        this.authorEmail = authorEmail;
        this.body = body;
        this.createdDate = createdDate;
    }
    static parseComment(data) {
        const { mId, mAuthorEmail, mBody, mCreatedDate } = data;
        return new MyComment(mId, mAuthorEmail, mBody, mCreatedDate);
    }
}
class CommentService {
    constructor() { }
    getCommentsWithMessageId(id) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                url: Constants.API_URL + `/comments/${id}`,
                method: 'GET',
                dataType: 'json'
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                let comments = [];
                if (mData !== undefined) {
                    mData.forEach((cmtData) => {
                        let comment = MyComment.parseComment(cmtData);
                        comments.push(comment);
                    });
                }
                resolve(comments);
            }
            else {
                reject(mMessage);
            }
        }));
    }
    addComment(id, email, body) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let result = yield $.ajax({
                url: Constants.API_URL + `/comments/${id}`,
                method: 'POST',
                data: JSON.stringify({ email, body }),
                contentType: 'application/json',
                dataType: 'json'
            });
            const { mStatus, mData, mMessage } = result;
            if (mStatus === 'ok') {
                resolve(true);
            }
            else {
                reject(mMessage);
            }
        }));
    }
}
CommentService.instance = new CommentService();
class MessageDetailScreen {
    static setMessage(message) {
        MessageDetailScreen.selectedMessage = message;
    }
    static init() {
        if (!MessageDetailScreen.isInit) {
            let div = $(`<div id="${MessageDetailScreen.NAME}"><div id="${MessageDetailScreen.NAME}-header"><button id="${MessageDetailScreen.NAME}-btn-back" class="btn"><i class="fa fa-times"></i></button><h1 class="text-center">Message Detail</h1></div></div>`);
            let card = HTMLStringCreator.createMessageCard(MessageDetailScreen.NAME);
            let header = `<h5 id="${MessageDetailScreen.NAME}-comments-count">Comments</h5>`;
            let commentForm = $(`<form id="${MessageDetailScreen.NAME}-comment-form">${MessageDetailScreen.createFormGroupWithTextArea('Add comment', 'add-comment', 2, `${MessageDetailScreen.NAME}-add-comment-form`)}</form>`);
            commentForm.submit(e => e.preventDefault());
            let addCommentBtn = $(`<button id="${MessageDetailScreen.NAME}-add-comment-btn" class="btn btn-warning">Submit Comment</button>`);
            addCommentBtn.click(MessageDetailScreen.submitComment);
            div.append(card);
            commentForm.append(addCommentBtn);
            div.append(commentForm);
            div.append(header);
            $('#app').append(div);
            MessageDetailScreen.isInit = true;
        }
    }
    static submitComment() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('submitting comment');
            let email = 'adg219@lehigh.edu';
            let body = $(`#${MessageDetailScreen.NAME}-add-comment-textarea`).val().toString();
            console.log(body);
            try {
                let success = yield CommentService.instance.addComment(MessageDetailScreen.selectedMessage.id, email, body);
                if (success) {
                    console.log('added comment successfully');
                    MessageDetailScreen.refreshComments();
                    $(`#${MessageDetailScreen.NAME}-add-comment-textarea`).val('');
                }
            }
            catch (error) {
                window.alert(error);
            }
        });
    }
    static addEditBtnClickListener(handler) {
        MessageDetailScreen.init();
        $(`#${MessageDetailScreen.NAME}-btn-edit`).click(handler);
    }
    static addBackBtnClickListener(handler) {
        MessageDetailScreen.init();
        $(`#${MessageDetailScreen.NAME}-btn-back`).click(MessageDetailScreen.hide);
    }
    static submit() {
        MessageDetailScreen.init();
        let title = $(`#${MessageDetailScreen.NAME}-title-field`).val();
        let body = $(`#${MessageDetailScreen.NAME}-body-textarea`).val();
        MessageDetailScreen.hide();
    }
    static resetForm() {
        $(`#${MessageDetailScreen.NAME}-title-field`).val('');
        $(`#${MessageDetailScreen.NAME}-body-textarea`).val('');
        $(`#${MessageDetailScreen.NAME}-add-comment-textarea`).val('');
    }
    static createFormGroupWithTextArea(label, s, rows, className) {
        if (rows === undefined) {
            rows = 3;
        }
        return `
			<div class="form-group ${className}">
				<label>${label}</label>
				<textarea class="form-control" id="${MessageDetailScreen.NAME}-${s}-textarea" rows="${rows}"></textarea>
			</div>
		`;
    }
    static refreshComments() {
        return __awaiter(this, void 0, void 0, function* () {
            $(`#${MessageDetailScreen.NAME}-comments`).remove();
            try {
                let comments = yield CommentService.instance.getCommentsWithMessageId(MessageDetailScreen.selectedMessage.id);
                $(`#${MessageDetailScreen.NAME}-comments-count`).text(`Comments (${comments.length})`);
                let table = $(`<table id="${MessageDetailScreen.NAME}-comments"></table>`);
                comments.forEach(comment => {
                    let row = HTMLStringCreator.createCommentCard(MessageDetailScreen.NAME, comment);
                    table.append(row);
                });
                $(`#${MessageDetailScreen.NAME}`).append(table);
                $(`.${MessageDetailScreen.NAME}-message-author-email`).each((i, obj) => {
                    let p = $(obj);
                    p.click(e => {
                        let email = $(e.currentTarget).text().split(' ')[0];
                        ProfileScreen.setSelectedUserWithEmail(email, MessageDetailScreen.show);
                        MessageDetailScreen.hide();
                    });
                });
            }
            catch (error) {
                window.alert(error);
            }
        });
    }
    static hide() {
        MessageDetailScreen.init();
        MessageDetailScreen.resetForm();
        $(`#${MessageDetailScreen.NAME}-edit-link-container`).remove();
        $(`#${MessageDetailScreen.NAME}`).hide();
    }
    static setCloseBtnListener(handler) {
        MessageDetailScreen.init();
        MessageDetailScreen.hide();
        $(`#${MessageDetailScreen.NAME}-btn-back`).click(() => {
            MessageDetailScreen.hide();
            handler();
        });
    }
    static createEditForm() {
        return `<form>` +
            `<div class="form-group">` +
            `<label for="title">Title</label>` +
            `<input type="text" class="form-control" id="${MessageDetailScreen.NAME}-edit-field" aria-describedby="titleHelp">` +
            `</div>` +
            `<div class="form-group">` +
            `<label for="body">Body</label>` +
            `<textarea class="form-control" id="${MessageDetailScreen.NAME}-edit-textarea" rows="3"></textarea>` +
            `</div>` +
            `<button id="${MessageDetailScreen.NAME}-edit-save-btn" class="btn btn-warning">Save</button>` +
            `<button id="${MessageDetailScreen.NAME}-edit-cancel-btn" class="btn">Cancel</button>` +
            `</form>`;
    }
    static refreshMessage() {
        const { title, senderEmail, body } = MessageDetailScreen.selectedMessage;
        $(`#${MessageDetailScreen.NAME}-message-title`).text(title);
        $(`#${MessageDetailScreen.NAME}-message-subtitle`).text(senderEmail);
        $(`#${MessageDetailScreen.NAME}-message-text`).text(body);
    }
    static show() {
        MessageDetailScreen.init();
        MessageDetailScreen.refreshMessage();
        $(`#${MessageDetailScreen.NAME}-edit-link-container`).remove();
        if (MessageDetailScreen.selectedMessage.senderEmail === 'adg219@lehigh.edu') {
            let ul = $(`<ul id="${MessageDetailScreen.NAME}-edit-link-container" class="list-group list-group-flush"></ul>`);
            let li = $(`<li class="list-group-item"></li>`);
            let editLink = $(`<a href="#" class="card-link float-right">Edit</a>`);
            editLink.click(e => {
                let editForm = $(MessageDetailScreen.createEditForm());
                editForm.submit(e => {
                    e.preventDefault();
                });
                let div = $(`<div id="${MessageDetailScreen.NAME}-edit-form"></div>`);
                div.append(editForm);
                $(`body`).append(div);
                $(`#${MessageDetailScreen.NAME}-edit-field`).val(MessageDetailScreen.selectedMessage.title);
                $(`#${MessageDetailScreen.NAME}-edit-textarea`).val(MessageDetailScreen.selectedMessage.body);
                $(`#${MessageDetailScreen.NAME}-edit-cancel-btn`).click(e => $(`#${MessageDetailScreen.NAME}-edit-form`).remove());
                $(`#${MessageDetailScreen.NAME}-edit-save-btn`).click((e) => __awaiter(this, void 0, void 0, function* () {
                    let title = $(`#${MessageDetailScreen.NAME}-edit-field`).val().toString();
                    let body = $(`#${MessageDetailScreen.NAME}-edit-textarea`).val().toString();
                    try {
                        let success = yield MessagesService.instance.updateMessage(MessageDetailScreen.selectedMessage.id, title, body);
                        if (success) {
                            $(`#${MessageDetailScreen.NAME}-edit-form`).remove();
                            MessageDetailScreen.selectedMessage.update(title, body);
                            MessageDetailScreen.refreshMessage();
                        }
                    }
                    catch (error) {
                        window.alert(error);
                    }
                }));
            });
            li.append(editLink);
            ul.append(li);
            $(`#${MessageDetailScreen.NAME}-message-card`).append(ul);
        }
        MessageDetailScreen.refreshComments();
        $(`#${MessageDetailScreen.NAME}`).show();
    }
}
MessageDetailScreen.NAME = 'message-detail-screen';
MessageDetailScreen.isInit = false;
class ChatScreen {
    static init() {
        if (!ChatScreen.isInit) {
            let html = `<div id="${ChatScreen.NAME}">` +
                `<div id="${ChatScreen.NAME}-header">` +
                `<button class="btn btn-light" id="${ChatScreen.NAME}-btn-profile">Profile</button>` +
                `<h1 class="text-center">Messages</h1>` +
                `<button class="btn" id="${ChatScreen.NAME}-btn-add"><i class="fa fa-pencil-square-o"></i></button>` +
                `</div>` +
                `</div>`;
            $('#app').append(html);
            $(`#${ChatScreen.NAME}-btn-add`).click(ChatScreen.showAddMessageScreen);
            $(`#${ChatScreen.NAME}-btn-profile`).click(e => {
                ChatScreen.showProfileScreen('adg219@lehigh.edu');
            });
            ChatScreen.isInit = true;
        }
    }
    static showMessageDetail(message) {
        MessageDetailScreen.setMessage(message);
        MessageDetailScreen.setCloseBtnListener(ChatScreen.show);
        MessageDetailScreen.show();
        ChatScreen.hide();
    }
    static showProfileScreen(email) {
        ProfileScreen.setSelectedUserWithEmail(email, ChatScreen.show);
        ChatScreen.hide();
    }
    static showAddMessageScreen() {
        AddMessageScreen.show();
        AddMessageScreen.setCloseBtnListener(ChatScreen.show);
        ChatScreen.hide();
    }
    static hide() {
        ChatScreen.init();
        $(`#${ChatScreen.NAME}`).hide();
    }
    static show() {
        ChatScreen.init();
        $(`#${ChatScreen.NAME}`).show();
    }
    static refresh() {
        return __awaiter(this, void 0, void 0, function* () {
            ChatScreen.init();
            $(`#${ChatScreen.NAME}-table`).remove();
            $(`#${ChatScreen.NAME}`).append(`<table id="${ChatScreen.NAME}-table"></table>`);
            let messages = yield MessagesService.instance.getMessages('adg219@lehigh.edu');
            messages.forEach(message => {
                let row = $(`<tr class="${ChatScreen.NAME}-row"></tr>`);
                let rowTds = ChatScreen.createMessageRow(message);
                rowTds.forEach((td, i) => {
                    let element = $(td);
                    if (i === 0) {
                        element.click(ChatScreen.messageRowClickListener);
                    }
                    row.append(element);
                });
                $(`#${ChatScreen.NAME}-table`).append(row);
            });
            ChatScreen.setupVoteClickListeners();
            ChatScreen.setupEmailClickListener();
        });
    }
    static setupEmailClickListener() {
        $(`.${ChatScreen.NAME}-user-email`).each((i, obj) => {
            let p = $(obj);
            p.click(e => {
                e.stopPropagation();
                e.preventDefault();
                let p = $(e.currentTarget);
                let email = p.text().split(' ')[0];
                console.log(email);
                ChatScreen.showProfileScreen(email);
            });
        });
    }
    static updateMessageRow() {
    }
    static setupVoteClickListeners() {
        $(`.btn-upvote`).click((e) => __awaiter(this, void 0, void 0, function* () {
            let btn = $(e.target);
            let id = parseInt(btn.data('id'));
            let email = 'adg219@lehigh.edu';
            try {
                let success = yield MessagesService.instance.upvote(email, id);
                if (success) {
                    ChatScreen.updateVoteSum(id, 1);
                }
                else {
                    console.log('upvote was unsuccessful');
                }
            }
            catch (error) {
                window.alert(error);
            }
        }));
        $(`.btn-downvote`).click((e) => __awaiter(this, void 0, void 0, function* () {
            let btn = $(e.target);
            let id = parseInt(btn.data('id'));
            let email = 'adg219@lehigh.edu';
            try {
                let success = yield MessagesService.instance.downvote(email, id);
                if (success) {
                    ChatScreen.updateVoteSum(id, -1);
                }
                else {
                    console.log('downvote was unsuccessful');
                }
            }
            catch (error) {
                window.alert(error);
            }
        }));
    }
    static updateVoteSum(id, value) {
        $(`.${ChatScreen.NAME}-message-vote-sum`).each((i, obj) => {
            let p = $(obj);
            let messageId = parseInt(p.data('id'));
            if (messageId === id) {
                let sum = parseInt(p.text());
                let newSum = sum + value;
                if (newSum > 0) {
                    p.addClass('positive');
                    p.removeClass('negative');
                }
                else if (newSum < 0) {
                    p.addClass('negative');
                    p.removeClass('positive');
                }
                else {
                    p.removeClass('negative');
                    p.removeClass('positive');
                }
                p.text(newSum);
            }
        });
    }
    static messageRowClickListener(e) {
        let textContainer = $(e.currentTarget);
        let title = textContainer.data('title');
        let id = parseInt(textContainer.data('id'));
        let body = textContainer.data('body');
        let senderEmail = textContainer.data('senderEmail');
        let dateCreated = textContainer.data('dateCreated');
        let upvoteCount = textContainer.data('upvoteCount');
        let downvoteCount = textContainer.data('downvoteCount');
        let message = new Message(id, senderEmail, title, body, dateCreated, upvoteCount, downvoteCount);
        ChatScreen.showMessageDetail(message);
    }
    static createMessageRow(message) {
        let { id, senderEmail, title, body, dateCreated, upvoteCount, downvoteCount } = message;
        let voteSum = upvoteCount - downvoteCount;
        let className = '';
        if (voteSum > 0) {
            className = 'positive';
        }
        else if (voteSum < 0) {
            className = 'negative';
        }
        return [
            `<td data-id="${id}"
			data-date-created="${dateCreated}"
			data-sender-email="${senderEmail}"
			data-title="${title}"
			data-body="${body}"
			data-upvote-count="${upvoteCount}"
			data-downvote-count="${downvoteCount}">` +
                `<h5>${title}</h5>` +
                `<p class="${ChatScreen.NAME}-user-email">${senderEmail} - ${dateCreated}</p>` +
                `<h6>${body}</h6>` +
                `</td>`,
            `<td>` +
                `<button data-id="${id}" class="btn btn-upvote"><i class="fa fa-chevron-up" data-id="${id}"></i></button>` +
                `<p class="${ChatScreen.NAME}-message-vote-sum ${className}" data-id="${id}">${voteSum}</p>` +
                `<button data-id="${id}" class="btn btn-downvote"><i class="fa fa-chevron-down" data-id="${id}"></i></button>` +
                `</td>`
        ];
    }
    static logout() {
        window.localStorage.setItem('the-buzz-authorization-token', '');
        window.localStorage.setItem('the-buzz-current-user-email', '');
        ChatScreen.hide();
    }
}
ChatScreen.NAME = "chat-screen";
ChatScreen.isInit = false;
const API_URL = 'https://glacial-fjord-88850.herokuapp.com';
$(() => {
    ChatScreen.refresh();
});
function readText() {
    document.getElementById("img1").src = "data:image/png;base64," + document.getBase64(file);
}
function uploadImage() {
    function getBase64(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            document.getElementById("img2").src = reader.result;
        };
    }
}
function checkIfLoggedIn() {
    let token = window.localStorage.getItem('the-buzz-authorization-token');
    return (token === undefined || token === null || token.length < 1) ? false : true;
}
function signInCallback(authResult) {
    if (authResult['code']) {
        $('#signinButton').attr('style', 'display: none');
        $.ajax({
            type: 'POST',
            url: 'https://glacial-fjord-88850.herokuapp.com/auth',
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            contentType: 'application/octet-stream; charset=utf-8',
            success: function (result) {
                console.log('result from google: ', result);
            },
            processData: false,
            data: authResult['code']
        });
    }
    else {
    }
}
class AuthScreen {
    static init() {
        if (!AuthScreen.isInit) {
            AuthScreen.isInit = true;
        }
    }
}
AuthScreen.NAME = 'auth-screen';
AuthScreen.isInit = false;
class Vote {
    constructor(email, id, type) {
        this.email = email;
        this.id = id;
        this.type = type;
    }
    static parseMessage(obj) {
        const { email, id, type } = obj;
        return new Vote(email, id, type);
    }
}
